/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

pub extern crate winit;
//Since we are on windows and that is the only way to get us the surface, include it.
#[cfg(windows)]
extern crate raw_window_handle;
#[cfg(windows)]
extern crate winapi;

use marp::ash;
use marp::ash::extensions::khr::*;
use marp::device::PhysicalDevice;
use marp::instance::Instance;
use marp::swapchain::surface::{Surface, WindowPlatform};

use std::sync::Arc;

///Defines a generic surfaces taken from a window and build for an instance.
/// # Panics
/// Using this Surface might panic if no surface extension (that works on the current System)
/// is activated. However, if you created the Instance the usual way and you are on
/// either windows or a Linux based system (with Xorg) you should be save.
pub struct WinitSurface {
    //The vulkan surface derived from surface
    surface: ash::vk::SurfaceKHR,
    //The actual surface we use to draw to
    surface_loader: ash::extensions::khr::Surface,
    //The instance we got created on.
    instance: Arc<Instance>,

    platform: WindowPlatform,
}

impl WinitSurface {
    ///Creates a new surface for a supplied window. TODO make it work for wayland.
    #[cfg(all(unix, not(target_os = "android")))]
    pub fn new<E>(
        instance: Arc<Instance>,
        window: &winit::window::Window,
        events_loop: &winit::event_loop::EventLoop<E>,
    ) -> Result<Arc<Self>, ash::vk::Result> {
        use winit::platform::unix::WindowExtUnix;

        use crate::winit::platform::unix::EventLoopWindowTargetExtUnix;

        //Since we are on Linux we have to check if we use X11 or Wayland.
        if events_loop.is_wayland() {
            //on wayland
            let way_display = window
                .wayland_display()
                .expect("failed to get wayland display!");
            let way_surface = window
                .wayland_surface()
                .expect("fauked to get wayland surface");

            let way_create_info = ash::vk::WaylandSurfaceCreateInfoKHR::builder()
                .display(way_display)
                .surface(way_surface);
            //Create the surface loader
            let way_surface_loader = WaylandSurface::new(instance.get_entry(), instance.vko());
            //Finally create the surface.
            #[cfg(feature = "logging")]
            log::info!("Marp: WARNING: creating wayland surface. You can't capture RenderDoc debug-frames when the wayland instance-extension is activated!");
            unsafe {
                match way_surface_loader.create_wayland_surface(&way_create_info, None) {
                    Ok(final_surf) => Ok(Arc::new(WinitSurface {
                        surface: final_surf,
                        surface_loader: ash::extensions::khr::Surface::new(
                            instance.get_entry(),
                            instance.vko(),
                        ),
                        instance: instance.clone(),
                        platform: WindowPlatform::Wayland,
                    })),
                    Err(er) => Err(er),
                }
            }
        } else {
            //in x11
            //Get us the X11 Display and the X11 window TODO use wayland as welll.
            let x11_display = window.xlib_display().expect("failed to find x11 display");
            let x11_window = window.xlib_window().expect("failed to find X11 Window");
            //Create surface info for x11 surface
            let x11_create_info = ash::vk::XlibSurfaceCreateInfoKHR::builder()
                .dpy(x11_display as *mut ash::vk::Display)
                .window(x11_window as ash::vk::Window);
            //Load the correct loader
            let xlib_surface_loader = XlibSurface::new(instance.get_entry(), instance.vko());
            //Finally create the surface and store it
            unsafe {
                match xlib_surface_loader.create_xlib_surface(&x11_create_info, None) {
                    Ok(surface) => Ok(Arc::new(WinitSurface {
                        surface,
                        surface_loader: ash::extensions::khr::Surface::new(
                            instance.get_entry(),
                            instance.vko(),
                        ),
                        instance: instance.clone(),
                        platform: WindowPlatform::X11,
                    })),
                    Err(er) => Err(er),
                }
            }
        }
    }

    #[cfg(windows)]
    pub fn new<E>(
        instance: Arc<Instance>,
        window: &winit::window::Window,
        events_loop: &winit::event_loop::EventLoop<E>,
    ) -> Result<Arc<Self>, ash::vk::Result> {
        //This time we can only load the Windows native surface.
        use raw_window_handle::HasRawWindowHandle;
        use winapi::shared::windef::HWND;
        use winapi::um::libloaderapi::GetModuleHandleW;

        //FIXME Pretty sure the windows path is broken
        //Get the address
        let hwnd = match window.raw_window_handle() {
            raw_window_handle::RawWindowHandle::Windows(windows_handle) => {
                windows_handle.hwnd as HWND
            }

            // If you'd like to handle other operating system's window handles, you can match on other `RawWindowHandle` variants.
            _ => panic!("unsupported os"),
        };
        //the instance of that window
        unsafe {
            let hinstance = GetModuleHandleW(std::ptr::null()) as *const std::ffi::c_void;
            let win32_create_info = ash::vk::Win32SurfaceCreateInfoKHR::builder()
                .hinstance(hinstance)
                .hwnd(hwnd as *const std::ffi::c_void);

            //Finally create the loader and the surface
            let win32_surface_loader = Win32Surface::new(instance.get_entry(), instance.vko());
            match win32_surface_loader.create_win32_surface(&win32_create_info, None) {
                Ok(surface) => Ok(Arc::new(WinitSurface {
                    surface,
                    surface_loader: ash::extensions::khr::Surface::new(
                        instance.get_entry(),
                        instance.vko(),
                    ),
                    instance: instance.clone(),
                    platform: WindowPlatform::Windows,
                })),
                Err(er) => Err(er),
            }
        }
    }
}

impl Drop for WinitSurface {
    fn drop(&mut self) {
        //delocate the surfaceKHR
        unsafe {
            self.surface_loader.destroy_surface(self.surface, None);
        }
    }
}

impl Surface for WinitSurface {
    ///Returns the Surface loader, use this one to query for surface capabilites etc.
    fn get_surface_loader(&self) -> &ash::extensions::khr::Surface {
        &self.surface_loader
    }

    ///Returns true if this surface is supported by this `physical_device` on this `queue_index`
    fn is_supported(&self, physical_device: &ash::vk::PhysicalDevice, queue_index: usize) -> bool {
        //Not sure if I can do this. The function is marked as unsafe but what could go wrong?
        unsafe {
            if let Ok(res) = self.surface_loader.get_physical_device_surface_support(
                *physical_device,
                queue_index as u32,
                self.surface,
            ) {
                res
            } else {
                false
            }
        }
    }

    ///Returns the capabilites this surface has in respect to this physical device.
    /// Use this to find out how many swapchain images you can create, the extend of the current surfaceKHR
    /// ans similar information.
    fn get_surface_capabilities(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> Result<ash::vk::SurfaceCapabilitiesKHR, ash::vk::Result> {
        //assert!(self.surface != ash::extensions::Surface::null(), "Surface must not be null!");
        //Since we can be sure that the device and the surface are not null this is safe
        unsafe {
            match self
                .surface_loader
                .get_physical_device_surface_capabilities(*physical_device.vko(), self.surface)
            {
                Ok(k) => Ok(k),
                Err(er) => {
                    #[cfg(feature = "logging")]
                    log::warn!("Warning could not get surface capabilities: {}", er);
                    Err(er)
                }
            }
        }
    }

    ///Returns the list of present modes this surface supports. Fifo is supported everywhere.
    fn get_present_modes(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> ash::prelude::VkResult<Vec<ash::vk::PresentModeKHR>> {
        unsafe {
            self.surface_loader
                .get_physical_device_surface_present_modes(*physical_device.vko(), self.surface)
        }
    }

    ///Returns the formats this surface supports.
    fn get_supported_formats(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> ash::prelude::VkResult<Vec<ash::vk::SurfaceFormatKHR>> {
        unsafe {
            self.surface_loader
                .get_physical_device_surface_formats(*physical_device.vko(), self.surface)
        }
    }

    ///Returns the SurfaceKHR
    fn vko(&self) -> &ash::vk::SurfaceKHR {
        &self.surface
    }

    ///Returns the instance this surface was created for.
    fn get_instance(&self) -> Arc<Instance> {
        self.instance.clone()
    }

    ///Returns on which platform this surface was created
    fn get_platform(&self) -> &WindowPlatform {
        &self.platform
    }
}
