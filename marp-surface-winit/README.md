# Marp-Surface-Winit
Is the standart implementation of the `Surface` trait for a winit window. It will, depending on operating system and desktop environment, find the windows surface and create a vulkan SurfaceKHR object for it.
This implementation also takes care of destroying said object.

There area currently no other surface implementations, if you want to use GLFW or SDL2, you'll have to write your own. However, that shouldn't be too hard.
