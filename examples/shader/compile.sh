#!/bin/sh
#TODO do with a while loop over .vert and .frag files

#Triangle example
echo "Compile Triangle shader ..."
glslangValidator -V triangle/triangle.frag
glslangValidator -V triangle/triangle.vert 
mv frag.spv triangle/
mv vert.spv triangle/

#Cube example
echo "Compile Cube shader ..."
glslangValidator -V cube/cube.frag
glslangValidator -V cube/cube.vert 
mv frag.spv cube/
mv vert.spv cube/


#Cube with textures example
echo "Compile Cube with textures shader ..."
glslangValidator -V cube_textures/cube_textures.frag
glslangValidator -V cube_textures/cube_textures.vert 
mv frag.spv cube_textures/
mv vert.spv cube_textures/


#Cube with textures example
echo "Compile PushConstants shader ..."
glslangValidator -V push_constants/push.frag 
glslangValidator -V push_constants/push.vert 
mv frag.spv push_constants/
mv vert.spv push_constants/
