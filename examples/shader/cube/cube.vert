/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450

layout (location = 0) in vec4 pos;
layout (location = 1) in vec4 color;

layout(set = 0, binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;


layout (location = 0) out vec4 o_color;
void main() {
    o_color = color;
    gl_Position = ubo.proj * ubo.view * ubo.model * pos;
}
