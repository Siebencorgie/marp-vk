/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate cgmath;
extern crate marp;

use cgmath::*;
use marp::ash::vk;
use marp::buffer::*;
use marp::descriptor::*;
use marp::memory::*;
use marp::pipeline::*;
use marp::*;

use marp_surface_winit::winit;
use marp_utils::*;

use std::mem;
use std::time::Instant;
use std::u64;

//Our Vertex format
#[derive(Clone, Debug, Copy)]
#[repr(C)]
struct Vertex {
    pos: [f32; 4],
}

#[repr(align(16))]
#[derive(Clone, Copy, Debug)]
struct Push {
    color: [f32; 4],
    x: f32,
    y: f32,
}

pub fn main() {
    let events_loop = winit::event_loop::EventLoop::new();

    let window = winit::window::Window::new(&events_loop).expect("Failed to create window!");

    //Create a device and queue that support the surface and the graphics operations
    let (device, queue, surface) = create_device_and_gfx_queue(&window, &events_loop)
        .expect("Failed to create device and queue");

    //This one descibes how vertices are bound to the shader.
    let vertex_state = pipeline::VertexInputState {
        vertex_binding_descriptions: vec![vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()],
        vertex_attribute_descriptions: vec![
            //Description of the Pos attribute
            vk::VertexInputAttributeDescription::builder()
                .location(0)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, pos) as u32)
                .build(),
        ],
    };

    //TODO Create Vertex/Index buffer
    let indices: Vec<u32> = vec![0, 1, 2]; //, 0, 3, 2, 0, 1, 4];
    let (index_upload, index_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        indices.clone(), //The buffer size we need
        BufferUsage {
            index_buffer: true,
            storage_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly,
    )
    .expect("Failed to create index buffer");
    index_upload
        .wait(u64::MAX)
        .expect("Failed to upload inidice buffer!");

    let vertices = vec![
        Vertex {
            pos: [-0.7, 0.7, 0.0, 0.7],
        },
        Vertex {
            pos: [0.7, 0.7, 0.0, 0.7],
        },
        Vertex {
            pos: [0.0, -0.7, 0.0, 0.7],
        },
        Vertex {
            pos: [-0.7, -0.7, 0.0, 0.7],
        },
    ];

    let (vertex_upload, vertex_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        vertices, //The buffer size we need
        BufferUsage {
            vertex_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly,
    )
    .expect("Failed to create index buffer");
    vertex_upload
        .wait(u64::MAX)
        .expect("Failed to upload vertex buffer!");

    //Create our push constant
    let push_constant = PushConstant::new(
        Push {
            color: [1.0, 0.0, 1.0, 1.0],
            x: 0.0,
            y: 0.0,
        },
        vk::ShaderStageFlags::VERTEX, //we only send to the vertex shader
    );

    //Since we have all data for the pipeline ready, start the test bed.
    let base_app = RenderBed::new(
        device.clone(),
        queue.clone(),
        surface,
        window,
        events_loop,
        "examples/shader/push_constants/vert.spv",
        "examples/shader/push_constants/frag.spv",
        vec![], //The descriptor set layouts we want to take with our pipeline
        vec![*push_constant.range()], //push constants,
        vertex_state,
    );

    let start_time = Instant::now();
    println!("Created Cube base app!");

    let layout = base_app.pipeline.layout();
    base_app.start(move |(_idx, cb)| {
        let el = start_time.elapsed().as_millis() as f64 / 1000.0;

        /*
        *push_constant.get_content_mut() = Push{
            color: [el.cos() as f32 / (5.0), el.sin() as f32 / (1.0 / 3.0), el.sin() as f32, 1.0],
            x: el.sin() as f32 / 3.0,
            y: el.cos() as f32 / 4.0,
        };
        */
        cb.cmd_bind_vertex_buffers(0, vec![(vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer!");

        cb.cmd_bind_index_buffer(index_buffer.clone(), 0, vk::IndexType::UINT32)
            .expect("Failed to bind index buffer!");

        cb.cmd_push_constants(
            layout.clone(),
            &PushConstant::new(
                Push {
                    color: [
                        el.cos() as f32 / (5.0),
                        el.sin() as f32 / (1.0 / 3.0),
                        el.sin() as f32,
                        1.0,
                    ],
                    x: el.sin() as f32 / 3.0,
                    y: el.cos() as f32 / 4.0,
                },
                vk::ShaderStageFlags::VERTEX,
            ),
        )
        .expect("Failed to push push constant!");

        cb.cmd_draw_indexed(indices.len() as u32, 1, 0, 0, 1)
            .expect("Faild to draw primitives");

        cb
    });
}
