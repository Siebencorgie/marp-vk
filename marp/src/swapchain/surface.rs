/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::device::physical_device::PhysicalDevice;
use crate::instance::Instance;
use ash;
use std::sync::Arc;

///Specifies on which windowing platform this Surface was created
pub enum WindowPlatform {
    Windows,
    X11,
    Wayland,
}

///Defines a trait which each surface provider (like winit or glfw) should implement.
pub trait Surface {
    fn get_surface_loader(&self) -> &ash::extensions::khr::Surface;
    ///Returns true if this surface is supported by this `physical_device` on this `queue_index`
    fn is_supported(&self, physical_device: &ash::vk::PhysicalDevice, queue_index: usize) -> bool;
    ///Returns the capabilites this surface has in respect to this physical device.
    /// Use this to find out how many swapchain images you can create, the extend of the current surfaceKHR
    /// and similar information.
    fn get_surface_capabilities(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> Result<ash::vk::SurfaceCapabilitiesKHR, ash::vk::Result>;
    ///Returns the list of present modes this surface supports. Fifo is supported everywhere.
    fn get_present_modes(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> ash::prelude::VkResult<Vec<ash::vk::PresentModeKHR>>;
    ///Returns the formats this surface supports.
    fn get_supported_formats(
        &self,
        physical_device: Arc<PhysicalDevice>,
    ) -> ash::prelude::VkResult<Vec<ash::vk::SurfaceFormatKHR>>;
    ///Returns the raw vulkan surface.
    fn vko(&self) -> &ash::vk::SurfaceKHR;
    ///Returns the instance this surface was created for.
    fn get_instance(&self) -> Arc<Instance>;
    ///Returns on which platform this surface was created
    fn get_platform(&self) -> &WindowPlatform;
}
