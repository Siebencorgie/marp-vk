/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::command_buffer::*;
use crate::device::{queue::*, Device, Queue};
use crate::image::{ImageUsage, SwapchainImage};
use crate::sync::*;
use ash::{self, vk::Extent2D};
use std::sync::{Arc, RwLock};
use std::u64;

///Contains the description of a generic surface
pub mod surface;

#[derive(Debug)]
pub enum SwapchainCreationError {
    ///Could not find an format at the index supplied to `Swapchain::new()`
    NoSuchFormatIndex,
}

#[derive(Debug, PartialEq)]
pub enum SwapchainImageState {
    Ok,
    ///If the swapchain's surface and the swapchain images have a different extend.
    DivergingExtent,
}

impl SwapchainImageState {
    pub fn is_ok(&self) -> bool {
        match self {
            SwapchainImageState::Ok => true,
            _ => false,
        }
    }
}

///A Swapchain that serves images to a `Surface`
pub struct Swapchain {
    swapchain: RwLock<ash::vk::SwapchainKHR>,
    swapchain_loader: ash::extensions::khr::Swapchain,
    //The Format this swapchain's surface uses
    format: ash::vk::SurfaceFormatKHR,
    //The extend this swapchain was created for.
    extent: RwLock<ash::vk::Extent2D>,
    //The surface we target with our images
    surface: Arc<dyn surface::Surface + Send + Sync + 'static>,
    //reference to the used device
    device: Arc<Device>,
    //the images of this swapchain
    images: RwLock<Vec<Arc<SwapchainImage>>>,

    //The image count our surface can hold
    image_count: u32,
    //The pre transform we want to apply
    pre_transform: ash::vk::SurfaceTransformFlagsKHR,
    //The present mode we want to use
    present_mode: ash::vk::PresentModeKHR,

    //The usage flags for the swapchain images
    usage: ImageUsage,
}

impl Swapchain {
    /// Creates a swapchain from this surface.
    ///
    /// Creates a swapchain with `dimensions if supported. Else it'll create the biggest image possible
    ///
    /// Uses the `format` or the first format possible for the surface.
    ///
    /// Creates `img_count` images (if possible), or if not supplied as many as possible for this surface.
    ///
    /// Uses `present_mode` if possible on the device.
    ///
    /// Creates the Images with `usage` flags if possible. Otherwise only `COLOR_ATTACHMENT` is used since it is guaranteed.
    ///
    /// #Notice
    /// The image sharing mode will be exclusive. You will only be able to access the images from one queue.
    pub fn new(
        device: Arc<Device>,
        surface: Arc<dyn surface::Surface + Send + Sync + 'static>,
        dimensions: ash::vk::Extent2D,
        format: Option<ash::vk::SurfaceFormatKHR>,
        img_count: Option<usize>,
        present_mode: Option<ash::vk::PresentModeKHR>,
        usage: Option<ImageUsage>,
    ) -> Result<Arc<Self>, SwapchainCreationError> {
        let surface_formats = surface
            .get_supported_formats(device.get_physical_device().clone())
            .expect("failed to get supported formats of the surface.");

        let format = if let Some(format) = format {
            format
        } else {
            *surface_formats
                .iter()
                .nth(0)
                .expect("Surface has no formats")
        };

        let surf_capabilities = surface
            .get_surface_capabilities(device.get_physical_device().clone())
            .expect("failed to get surface capabilities.");
        let usage = if let Some(uf) = usage {
            if surf_capabilities
                .supported_usage_flags
                .contains(uf.to_image_usage_flags())
            {
                uf
            } else {
                ImageUsage {
                    color_attachment: true,
                    ..Default::default()
                }
            }
        } else {
            ImageUsage {
                color_attachment: true,
                ..Default::default()
            }
        };

        let image_count = if let Some(imgc) = img_count {
            //If imgc < min_count use min_count, if imgc >= 1 check if max_count is either 0 (everything is okay) or check if we are within bounds
            if (imgc as u32) < surf_capabilities.min_image_count {
                surf_capabilities.min_image_count
            } else {
                if surf_capabilities.max_image_count == 0 {
                    imgc as u32
                } else {
                    (imgc as u32).min(surf_capabilities.max_image_count)
                }
            }
        } else {
            #[cfg(feature = "logging")]
            log::warn!(
                "Falling back to default image count {:?}",
                surf_capabilities.min_image_count
            );
            surf_capabilities.min_image_count
        };

        //Check if we support the given extend. Else use the max extend possible
        let resolution = {
            let max_extend = surf_capabilities.current_extent;
            if dimensions.width <= max_extend.width && dimensions.height <= max_extend.height {
                dimensions
            } else {
                #[cfg(feature = "logging")]
                log::warn!(
                    "swapchain extend is falling back! \nfrom desired: {:?}\nto supported: {:?}",
                    dimensions,
                    max_extend
                );
                max_extend
            }
        };

        //Check if we can set the pre transform to identity, else just use the default one.
        //TODO make the whole resolution, transform etc step nicer with an surface_properties struct or something
        let pre_transform = if surf_capabilities
            .supported_transforms
            .contains(ash::vk::SurfaceTransformFlagsKHR::IDENTITY)
        {
            ash::vk::SurfaceTransformFlagsKHR::IDENTITY
        } else {
            surf_capabilities.current_transform
        };

        let available_pms = surface
            .get_present_modes(device.get_physical_device().clone())
            .expect("Could not read present modes of the surface");

        let present_mode = if let Some(pm) = present_mode {
            //Check if we have this one, else use fifo which is always supported
            available_pms
                .iter()
                .cloned()
                .find(|&mode| mode == pm)
                .unwrap_or(ash::vk::PresentModeKHR::MAILBOX)
        } else {
            #[cfg(feature = "logging")]
            log::warn!("Could not find set present mode checking for FIFO");
            available_pms
                .iter()
                .cloned()
                .find(|&mode| mode == ash::vk::PresentModeKHR::MAILBOX)
                .unwrap_or(ash::vk::PresentModeKHR::FIFO)
        };

        //Now create the loader, and stuff it with the prepared info.

        let swapchain_loader =
            ash::extensions::khr::Swapchain::new(device.get_instance().vko(), device.vko());

        let swapchain_create_info = ash::vk::SwapchainCreateInfoKHR::builder()
            .surface(*surface.vko())
            .min_image_count(image_count)
            .image_format(format.format)
            .image_color_space(format.color_space)
            .image_extent(resolution)
            .image_usage(usage.to_image_usage_flags())
            .image_array_layers(1) //normal image
            .image_sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true); //TODO set queue indices to present queue?
                            //Since all resources we use are either initialized or left default, we can use unsafe here. However, I'm not sure if we queried the image format etc enough.
        let swapchain = unsafe {
            swapchain_loader
                .create_swapchain(&swapchain_create_info, None)
                .expect("failed to create swapchain")
        };

        //Now create the actual image representations for this swapchain
        let sw_images = unsafe {
            swapchain_loader
                .get_swapchain_images(swapchain)
                .expect("Failed to find all swapchain images!")
        };
        let images = RwLock::new(
            sw_images
                .into_iter()
                .map(|img| {
                    SwapchainImage::from_swapchain_image(
                        device.clone(),
                        format.format,
                        resolution,
                        img,
                        usage,
                    )
                    .expect("Failed to build swapchain image!")
                })
                .collect(),
        );

        Ok(Arc::new(Swapchain {
            swapchain: RwLock::new(swapchain),
            swapchain_loader,
            format,
            extent: RwLock::new(resolution),

            surface,
            device,
            images,

            image_count,
            pre_transform,
            present_mode,

            usage,
        }))
    }
    ///Returns the image format this swapchain uses.
    pub fn get_format(&self) -> ash::vk::Format {
        self.format.format
    }
    ///Returns the current extent this swapchain was created for.
    pub fn get_extent(&self) -> ash::vk::Extent2D {
        *self.extent.read().expect("Failed to read current extent")
    }

    ///Returns the current image. Keep in mind that the images can become invalid if the swapchain's surface size changes. Therefor try to retrieve the images only when needed.
    pub fn get_images(&self) -> Vec<Arc<SwapchainImage>> {
        self.images
            .read()
            .expect("Failed to read swapchain images")
            .clone()
    }

    ///Returns the number of images currently in use.
    pub fn image_count(&self) -> u32 {
        self.image_count
    }

    ///Returns the current surface capabilities. Is the same as calling `vkGetPhysiclDeviceSurfaceCapapbilitiesKHR`
    pub fn get_suface_capabilities(
        &self,
    ) -> ash::prelude::VkResult<ash::vk::SurfaceCapabilitiesKHR> {
        self.surface
            .get_surface_capabilities(self.device.get_physical_device().clone())
    }

    ///Returns the current extent of the surface, or None, if the surface extent is determined by the swapchains size. In that case
    /// you can use whatever you want.
    pub fn get_current_surface_extend(&self) -> Option<ash::vk::Extent2D> {
        //Get surface to find its current extend
        let surf_capabilities = self
            .get_suface_capabilities()
            .expect("Could not get surface capabilities!");
        //Use the currents swapchain resolution, or fallback to 720p
        let ext = surf_capabilities.current_extent;

        if ext
            == Extent2D::builder()
                .width(0xFFFFFFFF)
                .height(0xFFFFFFFF)
                .build()
        {
            None
        } else {
            Some(ext)
        }
    }

    pub fn check_image_state(&self) -> SwapchainImageState {
        //Use the currents swapchain resolution, or fallback to 720p
        if let Some(resolution) = self.get_current_surface_extend() {
            //Check if the current extend is the one we created this swapchain for.
            let current_extent = self.extent.read().expect("Failed to read extend");
            if current_extent.width != resolution.width
                || current_extent.height != resolution.height
            {
                SwapchainImageState::DivergingExtent
            } else {
                SwapchainImageState::Ok
            }
        } else {
            SwapchainImageState::Ok
        }
    }

    ///Recreates the swapchain based on the current surface and a new extent. Falls back to the max supported extent if the given one is too big.
    pub fn recreate(&self, extent: ash::vk::Extent2D) {
        //Lock the swapchain until we are finished
        let mut inner_swapchain = self
            .swapchain
            .write()
            .expect("Failed to lock swapchain while recreating");

        //Use the currents swapchain resolution, or fallback to 720p
        let resolution = {
            if let Some(desired_res) = self.get_current_surface_extend() {
                if extent.width <= desired_res.width && extent.height <= desired_res.height {
                    extent
                } else {
                    #[cfg(feature = "logging")]
                    log::warn!("swapchain extend is falling back! \nfrom desired: {:?}\nto supported: {:?}", extent, desired_res);
                    desired_res
                }
            } else {
                //Can be what ever
                extent
            }
        };
        //Now create a new swapchain but provide out old one.
        let swapchain_create_info = ash::vk::SwapchainCreateInfoKHR::builder()
            .surface(*self.surface.vko())
            .min_image_count(self.image_count)
            .image_format(self.format.format)
            .image_color_space(self.format.color_space)
            .image_extent(resolution) //new resolution
            .image_usage(self.usage.to_image_usage_flags())
            .image_array_layers(1) //normal image
            .image_sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
            .pre_transform(self.pre_transform)
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(self.present_mode)
            .clipped(true)
            .old_swapchain(*inner_swapchain); //TODO set queue indices to present queue?

        //Since all resources we use are either initialized or left default, we can use unsafe here. However, I'm not sure if we queried the image format etc enough.
        let swapchain = unsafe {
            self.swapchain_loader
                .create_swapchain(&swapchain_create_info, None)
                .expect("failed to create swapchain")
        };

        //Now create the actual image representations for this swapchain
        let sw_images = unsafe {
            self.swapchain_loader
                .get_swapchain_images(swapchain)
                .expect("Failed to find all swapchain images!")
        };
        let images = sw_images
            .into_iter()
            .map(|img| {
                SwapchainImage::from_swapchain_image(
                    self.device.clone(),
                    self.format.format,
                    resolution,
                    img,
                    self.usage,
                )
                .expect("Failed to build swapchain image!")
            })
            .collect();

        //Override our images
        let mut swapchain_images = self
            .images
            .write()
            .expect("Failed to acquire write lock for swapchain images");
        *swapchain_images = images;

        //Now delete the old one
        unsafe {
            self.swapchain_loader
                .destroy_swapchain(*inner_swapchain, None);
        }

        //Override the swapchain itself
        *inner_swapchain = swapchain;

        //Now override the extend we used for the swapchain creation with the inner extend
        *self
            .extent
            .write()
            .expect("Failed to write extend while swapchain recreation") = resolution;
    }

    ///Returns the index of the available swapchain image.
    pub fn acquire_next_image(
        &self,
        timeout: u64,
        signal_semaphore: Arc<Semaphore>,
    ) -> Result<u32, ash::vk::Result> {
        unsafe {
            match self.swapchain_loader.acquire_next_image(
                *self.swapchain.read().expect("Failed to read swapchain"),
                timeout,
                *signal_semaphore.vko(),
                ash::vk::Fence::null(),
            ) {
                Ok((idx, _is_suboptimal)) => Ok(idx),
                Err(er) => Err(er),
            }
        }
    }

    ///Moves all image of the swapchain into the PRESENT_SRC_KHR layout.
    pub fn images_to_present_layout(&self, queue: Arc<Queue>) -> QueueFence {
        //Transition all images to the present layout.
        let command_pool = match CommandBufferPool::new(
            self.device.clone(),
            queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        ) {
            Ok(cbp) => cbp,
            Err(_er) => panic!("Failed to create resize command pool"),
        };

        let cb: Arc<CommandBuffer> = match command_pool.alloc(1, false) {
            Ok(ref mut cbs) => cbs
                .pop()
                .expect("Failed to get swapchain resize Command Buffer"),
            Err(_er) => panic!("Failed to get resize command buffer"),
        };

        let _ = cb
            .begin_recording(true, false, false, None)
            .expect("Could not start upload command buffer");

        let mut barrier_builder = PipelineBarrierBuilder::<8>::new(
            ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
            ash::vk::PipelineStageFlags::TRANSFER,
        );

        for image in self.get_images().iter() {
            barrier_builder = barrier_builder.with_image_barrier(ImageMemoryBarrierBuilder::new(
                image.clone(),
                ImageLayoutTransitionEvent::Initialise(ash::vk::ImageLayout::PRESENT_SRC_KHR),
            ));
        }
        //Submit barrier
        cb.pipeline_barrier(barrier_builder.build()).unwrap();

        cb.end_recording()
            .expect("Could not end copy command buffer");
        //We have to create a staging buffer, and a command buffer, then upload the data and then wait for it to be finished.
        let submit_fence = queue
            .clone()
            .queue_submit(vec![SubmitInfo::new(vec![], vec![cb], vec![])])
            .expect("Failed to Submit Command buffer for present image transition!");

        submit_fence
    }

    ///Presents an image using `queue` on this swapchain.
    pub fn queue_present(
        &self,
        queue: Arc<Queue>,
        wait_semaphores: Vec<Arc<Semaphore>>,
        indice: u32,
    ) -> Result<PresentResources, ash::vk::Result> {
        let sems: Vec<ash::vk::Semaphore> = wait_semaphores.iter().map(|s| *s.vko()).collect();

        let swap = vec![*self.swapchain.read().expect("Failed to read swapchain")];
        let ind = vec![indice];
        let info = ash::vk::PresentInfoKHR::builder()
            .wait_semaphores(&sems)
            .swapchains(&swap)
            .image_indices(&ind);

        unsafe {
            match self.swapchain_loader.queue_present(*queue.vko(), &info) {
                Ok(_) => Ok(PresentResources {
                    semaphores: wait_semaphores,
                    indices: ind,
                }),
                Err(er) => {
                    #[cfg(feature = "logging")]
                    log::error!("Error on Queue present: {}", er);
                    Err(er)
                }
            }
        }
    }
}

///Saves how some presentation was configured.
pub struct PresentResources {
    #[allow(dead_code)]
    semaphores: Vec<Arc<Semaphore>>,
    #[allow(dead_code)]
    indices: Vec<u32>,
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe {
            //First drop all our images, then the swapchain
            *self
                .images
                .write()
                .expect("Failed to write images while dropping swapchain") = Vec::new();
            self.swapchain_loader.destroy_swapchain(
                *self
                    .swapchain
                    .read()
                    .expect("Failed to get swapchain while dropping"),
                None,
            );
        }
    }
}
