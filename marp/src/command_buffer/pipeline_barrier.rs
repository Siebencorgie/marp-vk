use std::sync::Arc;

use ash::vk::{
    self, AccessFlags, BufferMemoryBarrier, DependencyFlags, ImageLayout, ImageMemoryBarrier,
    ImageSubresourceRange, MemoryBarrier,
};
use smallvec::SmallVec;

use crate::{buffer::Buffer, device::Queue, image::AbstractImage};

///Finished pipeline barrier, can be reused if wanted.
#[derive(Clone)]
pub struct PipelineBarrier<const N: usize> {
    pub(super) src_stage: vk::PipelineStageFlags,
    pub(super) dst_stage: vk::PipelineStageFlags,

    pub(super) dependecies: DependencyFlags,

    pub(super) image_barriers: SmallVec<[ImageMemoryBarrier; N]>,
    pub(super) buffer_barriers: SmallVec<[BufferMemoryBarrier; N]>,
    pub(super) memory_barriers: SmallVec<[MemoryBarrier; N]>,
}

impl<const N: usize> PipelineBarrier<N> {
    pub fn new(
        src_stage: vk::PipelineStageFlags,
        dst_stage: vk::PipelineStageFlags,
    ) -> PipelineBarrierBuilder<N> {
        PipelineBarrierBuilder::new(src_stage, dst_stage)
    }
}

pub struct PipelineBarrierBuilder<const N: usize> {
    pub(super) src_stage: vk::PipelineStageFlags,
    pub(super) dst_stage: vk::PipelineStageFlags,

    pub(super) dependecies: DependencyFlags,

    pub(super) image_barriers: SmallVec<[ImageMemoryBarrierBuilder; N]>,
    pub(super) buffer_barriers: SmallVec<[BufferMemoryBarrierBuilder; N]>,
    pub(super) memory_barriers: SmallVec<[MemoryBarrierBuilder; N]>,
}
impl<const N: usize> PipelineBarrierBuilder<N> {
    ///Creates a new barrier that waits for all commands until `src_stage` to execute, and blocks all execution for stages belonging
    /// to `dst_stage` or higher. Allocates up until `BARRIERS` for each type on the stack.
    ///
    /// At runtime create the Pipeline barrier like this:
    /// ```ignore
    /// command_buffer.pipeline_barrier(
    ///     PipelineBarrierBuilder::<1>::new(
    ///         PipelineStageFlags::BOTTOM_OF_PIPE,
    ///         PipelineStageFlags::TRANSFER
    ///     ).with_buffer_barrier(
    ///         BufferMemoryBarrierBuilder::new(self.clone())
    ///             .with_access_mask(AccessMaskEvent::Acquire{to: AccessFlags::TRANSFER_WRITE})
    ///     ).build()
    /// ).unwrap();
    /// ```
    pub fn new(src_stage: vk::PipelineStageFlags, dst_stage: vk::PipelineStageFlags) -> Self {
        PipelineBarrierBuilder {
            src_stage,
            dst_stage,
            dependecies: DependencyFlags::empty(),
            buffer_barriers: SmallVec::new(),
            image_barriers: SmallVec::new(),
            memory_barriers: SmallVec::new(),
        }
    }

    pub fn with_dependencies(mut self, dependencies: DependencyFlags) -> Self {
        self.dependecies = dependencies;
        self
    }

    pub fn with_memory_barrier(mut self, barrier: MemoryBarrierBuilder) -> Self {
        self.memory_barriers.push(barrier);
        self
    }

    pub fn with_buffer_barrier(mut self, barrier: BufferMemoryBarrierBuilder) -> Self {
        self.buffer_barriers.push(barrier);
        self
    }

    pub fn with_image_barrier(mut self, barrier: ImageMemoryBarrierBuilder) -> Self {
        self.image_barriers.push(barrier);
        self
    }

    ///Merges two pipeline barriers. The src stage is set to the earliest one of the two, the dst stage to the latest to make sure
    /// the barrier keeps compatible.
    ///
    /// If src or dst stage change is undesired, check the compatibility of two stages via [is_compatible()](PipelineBarrierBuilder::is_compatible).
    pub fn merge<const ON: usize>(&mut self, mut other: PipelineBarrierBuilder<ON>) {
        //Create biggest window of stages
        self.src_stage = if self.src_stage < other.src_stage {
            self.src_stage
        } else {
            other.src_stage
        };
        self.dst_stage = if self.dst_stage > other.dst_stage {
            self.dst_stage
        } else {
            other.dst_stage
        };

        //merge dependecies
        self.dependecies |= other.dependecies;

        //TODO merge memory barriers.
        self.image_barriers.append(&mut other.image_barriers);
        self.buffer_barriers.append(&mut other.buffer_barriers);
        self.memory_barriers.append(&mut other.memory_barriers);
    }

    pub fn is_compatible(&self, other: &Self) -> bool {
        self.src_stage == other.src_stage && self.dst_stage == other.dst_stage
    }

    pub fn build(self) -> PipelineBarrier<N> {
        PipelineBarrier {
            src_stage: self.src_stage,
            dst_stage: self.dst_stage,
            dependecies: self.dependecies,
            buffer_barriers: self
                .buffer_barriers
                .into_iter()
                .map(|builder| builder.build())
                .collect(),
            image_barriers: self
                .image_barriers
                .into_iter()
                .map(|builder| builder.build())
                .collect(),
            memory_barriers: self
                .memory_barriers
                .into_iter()
                .map(|builder| builder.build())
                .collect(),
        }
    }
}

pub enum QueueEvent {
    None,
    ///Releases the queue from `from`'s ownership to `to`'s ownership.
    SyncedRelease {
        from: Arc<Queue>,
        to: Arc<Queue>,
    },
    ///Acquires the queue from `from`'s ownership to `to`'s ownership.
    SyncedAcquire {
        from: Arc<Queue>,
        to: Arc<Queue>,
    },
}

impl QueueEvent {
    pub fn into_src_dst_queue(self) -> (Option<Arc<Queue>>, Option<Arc<Queue>>) {
        match self {
            QueueEvent::None => (None, None),
            QueueEvent::SyncedRelease { from, to } => (Some(from), Some(to)),
            QueueEvent::SyncedAcquire { from, to } => (Some(from), Some(to)),
        }
    }
}

pub enum AccessMaskEvent {
    ///Transitions from empty flags to this configuration.
    Acquire(AccessFlags),
    Transition {
        from: AccessFlags,
        to: AccessFlags,
    },
    None,
}

impl AccessMaskEvent {
    pub fn into_access_flags(self) -> (Option<AccessFlags>, Option<AccessFlags>) {
        match self {
            AccessMaskEvent::None => (None, None),
            AccessMaskEvent::Acquire(to) => (None, Some(to)),
            AccessMaskEvent::Transition { from, to } => (Some(from), Some(to)),
        }
    }
}

pub struct MemoryBarrierBuilder {
    access_mask: AccessMaskEvent,
}

impl MemoryBarrierBuilder {
    pub fn new() -> Self {
        MemoryBarrierBuilder {
            access_mask: AccessMaskEvent::None,
        }
    }

    pub fn with_access_flags(mut self, flags: AccessMaskEvent) -> Self {
        self.access_mask = flags;
        self
    }

    pub fn build(self) -> MemoryBarrier {
        let mut builder = MemoryBarrier::builder();
        match self.access_mask {
            AccessMaskEvent::None => {}
            AccessMaskEvent::Acquire(to) => {
                builder = builder.dst_access_mask(to);
            }
            AccessMaskEvent::Transition { from, to } => {
                builder = builder.src_access_mask(from);
                builder = builder.dst_access_mask(to);
            }
        }

        builder.build()
    }
}

pub struct BufferMemoryBarrierBuilder {
    buffer: Arc<Buffer>,
    access_mask: AccessMaskEvent,
    queue_event: QueueEvent,
    ///Option<(offset, size)> Of a sub range in the buffer this barrier is executed on.
    range: Option<(u64, u64)>,
}

impl BufferMemoryBarrierBuilder {
    pub fn new(buffer: Arc<Buffer>) -> Self {
        BufferMemoryBarrierBuilder {
            buffer,
            access_mask: AccessMaskEvent::None,
            queue_event: QueueEvent::None,
            range: None,
        }
    }

    pub fn with_access_mask(mut self, access_event: AccessMaskEvent) -> Self {
        self.access_mask = access_event;
        self
    }

    pub fn with_queue_event(mut self, queue_event: QueueEvent) -> Self {
        self.queue_event = queue_event;
        self
    }

    pub fn with_range(mut self, offset: u64, size: u64) -> Self {
        self.range = Some((offset, size));
        self
    }

    pub fn build(self) -> BufferMemoryBarrier {
        let (src_queue, dst_queue) = self.queue_event.into_src_dst_queue();
        let (src_access, dst_access) = self.access_mask.into_access_flags();
        let (offset, size) = if let Some((offset, size)) = self.range {
            (Some(offset), Some(size))
        } else {
            (None, None)
        };

        let mut builder = ash::vk::BufferMemoryBarrier::builder();
        match (src_access, dst_access) {
            (Some(oa), Some(na)) => {
                builder = builder.src_access_mask(oa).dst_access_mask(na);
            }
            (None, Some(na)) => {
                builder = builder.dst_access_mask(na);
            }
            (Some(oa), None) => {
                builder = builder.src_access_mask(oa);
            }
            (None, None) => {
                //Nothingo
            }
        }

        //Only add if we are not concurrent
        if self.buffer.sharing_mode.is_exclusive() {
            match (src_queue, dst_queue) {
                (Some(old), Some(new)) => {
                    builder = builder
                        .src_queue_family_index(old.get_family().get_family_index())
                        .dst_queue_family_index(new.get_family().get_family_index());
                }
                (None, Some(new)) => {
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(new.get_family().get_family_index());
                }
                (Some(old), None) => {
                    builder = builder
                        .src_queue_family_index(old.get_family().get_family_index())
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
                (None, None) => {
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
            }
        } else {
            builder = builder
                .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
        }

        //Now setup the offset buffer and size info
        if let Some(off) = offset {
            builder = builder.offset(off);
        } else {
            builder = builder.offset(0);
        }

        if let Some(sz) = size {
            builder = builder.size(sz);
        } else {
            builder = builder.size(ash::vk::WHOLE_SIZE);
        }

        //always the whole buffer
        builder = builder.buffer(*self.buffer.vko());

        builder.build()
    }
}

///High-level representation if image layout transitions
pub enum ImageLayoutTransitionEvent {
    ///Initialises an image by using `UNDEFINED` as the src layout.
    Initialise(ImageLayout),
    Transition {
        from: ImageLayout,
        to: ImageLayout,
    },
    KeepLayout(ImageLayout),
}

impl ImageLayoutTransitionEvent {
    fn into_layouts(self) -> (Option<ImageLayout>, Option<ImageLayout>) {
        match self {
            ImageLayoutTransitionEvent::Initialise(to) => (None, Some(to)),
            ImageLayoutTransitionEvent::KeepLayout(l) => (Some(l), Some(l)),
            ImageLayoutTransitionEvent::Transition { from, to } => (Some(from), Some(to)),
        }
    }
}

pub struct ImageMemoryBarrierBuilder {
    image: Arc<dyn AbstractImage + Send + Sync>,
    ///Some if a layout transition is present.
    layout_transition: ImageLayoutTransitionEvent,

    queue_event: QueueEvent,

    access: AccessMaskEvent,

    range: Option<ImageSubresourceRange>,
}

impl ImageMemoryBarrierBuilder {
    ///Creates a new barrier for `image`. A transition type always need to be specified. If no transition takes place,
    /// use `ImageLayoutTransitionEvent::KeepLayout`.
    pub fn new(
        image: Arc<dyn AbstractImage + Send + Sync>,
        layout_transition: ImageLayoutTransitionEvent,
    ) -> Self {
        ImageMemoryBarrierBuilder {
            access: AccessMaskEvent::None,
            image,
            layout_transition,
            queue_event: QueueEvent::None,
            range: None,
        }
    }

    pub fn with_access_flags_event(mut self, event: AccessMaskEvent) -> Self {
        self.access = event;
        self
    }
    pub fn with_queue_event(mut self, event: QueueEvent) -> Self {
        self.queue_event = event;
        self
    }

    ///Sets a range for this image memory barrier.
    pub fn with_range(mut self, range: ImageSubresourceRange) -> Self {
        self.range = Some(range);
        self
    }

    pub fn build(self) -> ImageMemoryBarrier {
        let (src_queue, dst_queue) = self.queue_event.into_src_dst_queue();
        let (src_layout, dst_layout) = self.layout_transition.into_layouts();
        let (src_access, dst_access) = self.access.into_access_flags();

        let mut builder = ash::vk::ImageMemoryBarrier::builder();

        //Transition queue if one is set and we are exclusive on one queue
        if self.image.sharing_mode().is_exclusive() {
            match (src_queue, dst_queue) {
                //Queue transfer
                (Some(oq), Some(nq)) => {
                    builder = builder
                        .src_queue_family_index(oq.get_family().get_family_index())
                        .dst_queue_family_index(nq.get_family().get_family_index());
                }
                //Acquire
                (None, Some(nq)) => {
                    builder = builder.dst_queue_family_index(nq.get_family().get_family_index());
                }
                //Release
                (Some(oq), None) => {
                    builder = builder.src_queue_family_index(oq.get_family().get_family_index());
                }
                //No transfer
                (None, None) => {
                    //no queue transfer
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
            }
        } else {
            builder = builder
                .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
        }

        match (src_access, dst_access) {
            (Some(oa), Some(na)) => {
                builder = builder.src_access_mask(oa).dst_access_mask(na);
            }
            (None, Some(na)) => {
                builder = builder.dst_access_mask(na);
            }
            (Some(oa), None) => {
                builder = builder.src_access_mask(oa);
            }
            (None, None) => {
                //Nothingo
            }
        }

        //transition layout
        match (src_layout, dst_layout) {
            (Some(old), Some(new)) => {
                builder = builder.old_layout(old).new_layout(new);
            }
            (None, Some(new)) => {
                builder = builder
                    .old_layout(ash::vk::ImageLayout::UNDEFINED)
                    .new_layout(new);
            }
            (Some(old), None) => {
                builder = builder
                    .old_layout(old)
                    .new_layout(ash::vk::ImageLayout::UNDEFINED);
            }
            (None, None) => {
                builder = builder
                    .old_layout(ash::vk::ImageLayout::UNDEFINED)
                    .new_layout(ash::vk::ImageLayout::UNDEFINED);
            }
        }

        if let Some(r) = self.range {
            builder = builder.subresource_range(r);
        } else {
            builder =
                builder.subresource_range(self.image.image_info().to_subresource_range().build());
        }

        //now add the image and the whole subresource range
        builder = builder.image(*self.image.as_vk_image());

        builder.build()
    }
}
