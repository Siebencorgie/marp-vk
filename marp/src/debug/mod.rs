use ash::{self, extensions::ext::DebugUtils};

use std::sync::Arc;

use crate::{command_buffer::CommandBuffer, device::Device};

pub struct Debugger {
    pub(crate) device: Arc<Device>,
}

impl Debugger {
    ///If the device was created with a debugger returns Some.
    pub fn new(device: Arc<Device>) -> Option<Self> {
        if device.get_instance().debug_report_loader.is_some() {
            Some(Debugger { device })
        } else {
            None
        }
    }

    pub fn begin_region(
        &self,
        command_buffer: Arc<CommandBuffer>,
        name: &str,
        color: Option<[f32; 4]>,
    ) {
        let label = command_buffer.add_label(name, color);
        self.on_debug_utils(|ut| unsafe {
            ut.cmd_begin_debug_utils_label(*command_buffer.vko(), &label)
        })
    }

    pub fn add_label(
        &self,
        command_buffer: Arc<CommandBuffer>,
        name: &str,
        color: Option<[f32; 4]>,
    ) {
        let label = command_buffer.add_label(name, color);
        self.on_debug_utils(|ut| unsafe {
            ut.cmd_insert_debug_utils_label(*command_buffer.vko(), &label)
        })
    }

    pub fn end_region(&self, command_buffer: Arc<CommandBuffer>) {
        self.on_debug_utils(|ut| unsafe { ut.cmd_end_debug_utils_label(*command_buffer.vko()) })
    }

    ///Executes f directly with the DebugUtil object in scope
    pub fn on_debug_utils<F>(&self, f: F)
    where
        F: Fn(&DebugUtils),
    {
        (f)(self
            .device
            .get_instance()
            .debug_report_loader
            .as_ref()
            .unwrap())
    }
}
