/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ash;
use ash::vk;
use std::sync::{PoisonError, RwLock, RwLockWriteGuard};

use crate::device::*;
use crate::instance::Instance;
use std::sync::Arc;

use gpu_allocator::{
    vulkan::{Allocation, AllocationCreateDesc, Allocator, AllocatorCreateDesc},
    MemoryLocation,
};

/**
Types of memory usage. Make sure to use GpuOnly wherever it applies to get optimal performance.
 */
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum MemoryUsage {
    Unknown,
    GpuOnly,
    CpuToGpu,
    GpuToCpu,
}

impl From<MemoryUsage> for MemoryLocation {
    fn from(m: MemoryUsage) -> Self {
        match m {
            MemoryUsage::Unknown => MemoryLocation::Unknown,
            MemoryUsage::GpuOnly => MemoryLocation::GpuOnly,
            MemoryUsage::CpuToGpu => MemoryLocation::CpuToGpu,
            MemoryUsage::GpuToCpu => MemoryLocation::GpuToCpu,
        }
    }
}

///Errors that can happen while allocating.
#[derive(Debug)]
pub enum AllocationError {
    ///If the VMA allocator had a problem,
    VMAError(gpu_allocator::AllocationError),
    ///If vulkan hat a problem
    VulkanError(ash::vk::Result),
    ///If something went wrong while binding an image to a chunk of an heap.
    ImageBindingError(ash::vk::Result),
    ///Same as `ImageBindingError` but for buffers.
    BufferBindingError(ash::vk::Result),
    /// If uploading data from host to device has failed.
    UploadFailed(ash::vk::Result),
    /// If mapping a pointer and/or copying data to this pointer has failed.
    MapFailed,
    ///If there was an error locking the allocator
    AllocatorLockError,
}

impl From<PoisonError<RwLockWriteGuard<'_, VMA>>> for AllocationError {
    fn from(_e: PoisonError<RwLockWriteGuard<'_, VMA>>) -> Self {
        AllocationError::AllocatorLockError
    }
}

pub struct VMA {
    #[allow(dead_code)]
    physical_device: Arc<PhysicalDevice>,
    #[allow(dead_code)]
    instance: Arc<Instance>,
    allocator: Allocator,
}

impl VMA {
    pub fn new(
        instance: Arc<Instance>,
        device: ash::Device,
        physical_device: Arc<PhysicalDevice>,
        has_buffer_device_address: bool,
    ) -> RwLock<Self> {
        //We use currently only the standard allocator
        let allocator = Allocator::new(&AllocatorCreateDesc {
            instance: instance.vko().clone(),
            device: device.clone(),
            physical_device: *physical_device.vko(),
            debug_settings: Default::default(),
            buffer_device_address: has_buffer_device_address,
        })
        .expect("Failed to create Gpu memory allocator!");

        RwLock::new(VMA {
            physical_device,
            instance,
            allocator,
        })
    }
    ///binds an appropriate memory range to a created buffer object.
    pub fn create_buffer(
        &mut self,
        device: &ash::Device,
        buffer_info: &vk::BufferCreateInfo,
        memory_usage: MemoryUsage,
        name: Option<&str>,
    ) -> Result<(vk::Buffer, Allocation), AllocationError> {
        let buffer = unsafe { device.create_buffer(&buffer_info, None) }
            .map_err(|e| AllocationError::VulkanError(e))?;
        let requirements = unsafe { device.get_buffer_memory_requirements(buffer) };

        let allocation = self
            .allocator
            .allocate(&AllocationCreateDesc {
                name: name.unwrap_or("unnamed allocation"),
                requirements,
                location: memory_usage.into(),
                linear: true, // Buffers are always linear
            })
            .map_err(|e| AllocationError::VMAError(e))?;

        // Bind memory to the buffer
        unsafe {
            device
                .bind_buffer_memory(buffer, allocation.memory(), allocation.offset())
                .map_err(|e| AllocationError::BufferBindingError(e))?
        };

        //Return both, the allocation and the buffer
        Ok((buffer, allocation))
    }

    ///Destroys the created buffer.
    pub fn destroy_buffer(
        &mut self,
        device: &ash::Device,
        buffer: vk::Buffer,
        allocation: &Allocation,
    ) {
        self.allocator.free(allocation.clone()).unwrap();
        unsafe { device.destroy_buffer(buffer, None) };
    }

    ///Creates a new image from the specified image_info and binds memory based on the allocation info to it
    pub fn create_image(
        &mut self,
        device: &ash::Device,
        image_info: &vk::ImageCreateInfo,
        memory_usage: MemoryUsage,
        is_linear: bool,
        name: Option<&str>,
    ) -> Result<(vk::Image, Allocation), AllocationError> {
        let image = unsafe { device.create_image(&image_info, None) }
            .map_err(|e| AllocationError::VulkanError(e))?;
        let requirements = unsafe { device.get_image_memory_requirements(image) };

        let allocation = self
            .allocator
            .allocate(&AllocationCreateDesc {
                name: name.unwrap_or("unnamed allocation"),
                requirements,
                location: memory_usage.into(),
                linear: is_linear,
            })
            .map_err(|e| AllocationError::VMAError(e))?;

        // Bind memory to the buffer
        unsafe {
            device
                .bind_image_memory(image, allocation.memory(), allocation.offset())
                .map_err(|e| AllocationError::BufferBindingError(e))?
        };

        //Return both, the allocation and the buffer
        Ok((image, allocation))
    }

    pub fn destroy_image(
        &mut self,
        device: &ash::Device,
        image: vk::Image,
        allocation: &Allocation,
    ) {
        self.allocator.free(allocation.clone()).unwrap();
        unsafe { device.destroy_image(image.clone(), None) };
    }

    pub fn allocator(&mut self) -> &mut Allocator {
        &mut self.allocator
    }
}
