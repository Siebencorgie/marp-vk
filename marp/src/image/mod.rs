/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Things you can do in Vulkan but not in marp_vk
//! I decided to not split the concept of ImageView and Image within marp. While you can create an Image in
//! Vulkan and use several Views on it, the actual use case is kind of small. To keep confusion down
//! you create only an image from some `ImageInfo` in marp and get a Image in return. There might be specialised
//! image types later on.
//!
//! Because the ImageView an Images are merged into one object there are some implications that go with it. The biggest might be
//! that the images view that results always covers the whole image.
//! Some other values like `view_type` are handled implicit depending on the supplied `ImageType`
//!
use ash;
use ash::vk::AccessFlags;
use ash::vk::ImageLayout;
use gpu_allocator::vulkan::Allocation;

use crate::buffer::*;
use crate::device::*;
use crate::framebuffer;
use crate::memory::*;

use crate::command_buffer::*;
use crate::sync::*;

use std::sync::Arc;
use std::u64;

use std::default::Default;
///Describes how mip map levels should be allocated. Log2 creates a new level for every "half" images of the original one.
/// You usually want that for textures.
#[derive(Clone, Copy)]
pub enum MipLevel {
    ///Creates (rounded down to full numberts) log2 levels based on the smallest extend of that image in one direction.
    Log2,
    Specific(u32),
}

impl MipLevel {
    pub fn to_level_count(&self, image_type: &ImageType) -> u32 {
        match self {
            MipLevel::Specific(count) => *count,
            MipLevel::Log2 => {
                //Find the biggest extend of that image and calculate the log2 then round down to the next full number.
                let biggest_extend = match image_type {
                    ImageType::Image1D { width } => width,
                    ImageType::Image2D {
                        width,
                        height,
                        samples: _,
                    } => width.min(height),
                    ImageType::Image3D {
                        width,
                        height,
                        depth,
                    } => width.min(height).min(depth),
                    ImageType::Cube {
                        width,
                        height,
                        samples: _,
                    } => width.min(height),
                    ImageType::Image1DArray {
                        width,
                        array_layers: _,
                    } => width,
                    ImageType::Image2DArray {
                        width,
                        height,
                        array_layers: _,
                        samples: _,
                    } => width.min(height),
                    ImageType::CubeArray {
                        width,
                        height,
                        samples: _,
                        array_layers: _,
                    } => width.min(height),
                };
                let log = (*biggest_extend as f32).log2().floor() as u32;
                log
            }
        }
    }
}

///Defines Image types. NOTE: if you use a samples count of 1 this means that the image is not multi sampled.
/// Multisampling is usually only used for input attchaments. Also be sure, if you use multisampling to make it
/// a 2^n value between 1 and 64, otherwise it will be set to 1.
#[derive(Eq, PartialEq, Clone, Copy)]
pub enum ImageType {
    Image1D {
        width: u32,
    },
    Image2D {
        width: u32,
        height: u32,
        samples: u32,
    },
    Image3D {
        width: u32,
        height: u32,
        depth: u32,
    },
    Cube {
        width: u32,
        height: u32,
        samples: u32,
    },
    Image1DArray {
        width: u32,
        array_layers: u32,
    },
    Image2DArray {
        width: u32,
        height: u32,
        array_layers: u32,
        samples: u32,
    },
    ///The array layers have to be a multiple of 6.
    CubeArray {
        width: u32,
        height: u32,
        samples: u32,
        array_layers: u32,
    },
}

impl ImageType {
    ///Returns true if the image is valid when used to allocate memory
    pub fn is_valid(&self) -> Result<(), String> {
        if self.width() < 1 || self.height() < 1 || self.depth() < 1 {
            return Err("width, height and depth of an image must be greater than 0".to_string());
        }

        if self.array_layer() < 1 {
            return Err("The images array_layers must be greater than ".to_string());
        }

        Ok(())
    }

    ///Returns a compatible ImageViewType
    pub fn vko_view(&self) -> ash::vk::ImageViewType {
        match &self {
            ImageType::Image1D { width: _ } => ash::vk::ImageViewType::TYPE_1D,
            ImageType::Image2D {
                width: _,
                height: _,
                samples: _,
            } => ash::vk::ImageViewType::TYPE_2D,
            ImageType::Image3D {
                width: _,
                height: _,
                depth: _,
            } => ash::vk::ImageViewType::TYPE_3D,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => ash::vk::ImageViewType::TYPE_1D_ARRAY,
            ImageType::Image2DArray {
                width: _,
                height: _,
                array_layers: _,
                samples: _,
            } => ash::vk::ImageViewType::TYPE_2D_ARRAY,
            ImageType::Cube {
                width: _,
                height: _,
                samples: _,
            } => ash::vk::ImageViewType::CUBE,
            ImageType::CubeArray {
                width: _,
                height: _,
                samples: _,
                array_layers: _,
            } => ash::vk::ImageViewType::CUBE_ARRAY,
        }
    }

    ///Returns a compatible ImageType
    pub fn vko_type(&self) -> ash::vk::ImageType {
        match &self {
            ImageType::Image1D { width: _ } => ash::vk::ImageType::TYPE_1D,
            ImageType::Image2D {
                width: _,
                height: _,
                samples: _,
            } => ash::vk::ImageType::TYPE_2D,
            ImageType::Image3D {
                width: _,
                height: _,
                depth: _,
            } => ash::vk::ImageType::TYPE_3D,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => ash::vk::ImageType::TYPE_1D,
            ImageType::Image2DArray {
                width: _,
                height: _,
                array_layers: _,
                samples: _,
            } => ash::vk::ImageType::TYPE_2D,
            ImageType::Cube {
                width: _,
                height: _,
                samples: _,
            } => ash::vk::ImageType::TYPE_2D,
            ImageType::CubeArray {
                width: _,
                height: _,
                samples: _,
                array_layers: _,
            } => ash::vk::ImageType::TYPE_2D,
        }
    }

    pub fn width(&self) -> u32 {
        match &self {
            ImageType::Image1D { width } => *width,
            ImageType::Image2D {
                width,
                height: _,
                samples: _,
            } => *width,
            ImageType::Image3D {
                width,
                height: _,
                depth: _,
            } => *width,
            ImageType::Image1DArray {
                width,
                array_layers: _,
            } => *width,
            ImageType::Image2DArray {
                width,
                height: _,
                array_layers: _,
                samples: _,
            } => *width,
            ImageType::Cube {
                width,
                height: _,
                samples: _,
            } => *width,
            ImageType::CubeArray {
                width,
                height: _,
                samples: _,
                array_layers: _,
            } => *width,
        }
    }

    pub fn height(&self) -> u32 {
        match &self {
            ImageType::Image1D { width: _ } => 1,
            ImageType::Image2D {
                width: _,
                height,
                samples: _,
            } => *height,
            ImageType::Image3D {
                width: _,
                height,
                depth: _,
            } => *height,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => 1,
            ImageType::Image2DArray {
                width: _,
                height,
                array_layers: _,
                samples: _,
            } => *height,
            ImageType::Cube {
                width: _,
                height,
                samples: _,
            } => *height,
            ImageType::CubeArray {
                width: _,
                height,
                samples: _,
                array_layers: _,
            } => *height,
        }
    }

    pub fn depth(&self) -> u32 {
        match &self {
            ImageType::Image1D { width: _ } => 1,
            ImageType::Image2D {
                width: _,
                height: _,
                samples: _,
            } => 1,
            ImageType::Image3D {
                width: _,
                height: _,
                depth,
            } => *depth,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => 1,
            ImageType::Image2DArray {
                width: _,
                height: _,
                array_layers: _,
                samples: _,
            } => 1,
            ImageType::Cube {
                width: _,
                height: _,
                samples: _,
            } => 1,
            ImageType::CubeArray {
                width: _,
                height: _,
                samples: _,
                array_layers: _,
            } => 1,
        }
    }

    pub fn array_layer(&self) -> u32 {
        match &self {
            ImageType::Image1D { width: _ } => 1,
            ImageType::Image2D {
                width: _,
                height: _,
                samples: _,
            } => 1,
            ImageType::Image3D {
                width: _,
                height: _,
                depth: _,
            } => 1,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => 1,
            ImageType::Image2DArray {
                width: _,
                height: _,
                array_layers,
                samples: _,
            } => *array_layers,
            ImageType::Cube {
                width: _,
                height: _,
                samples: _,
            } => 1,
            ImageType::CubeArray {
                width: _,
                height: _,
                samples: _,
                array_layers,
            } => *array_layers,
        }
    }

    pub fn samples(&self) -> u32 {
        match &self {
            ImageType::Image1D { width: _ } => 1,
            ImageType::Image2D {
                width: _,
                height: _,
                samples,
            } => *samples,
            ImageType::Image3D {
                width: _,
                height: _,
                depth: _,
            } => 1,
            ImageType::Image1DArray {
                width: _,
                array_layers: _,
            } => 1,
            ImageType::Image2DArray {
                width: _,
                height: _,
                array_layers: _,
                samples,
            } => *samples,
            ImageType::Cube {
                width: _,
                height: _,
                samples,
            } => *samples,
            ImageType::CubeArray {
                width: _,
                height: _,
                samples,
                array_layers: _,
            } => *samples,
        }
    }

    pub fn sample_count_flag(&self) -> ash::vk::SampleCountFlags {
        let mut count = self.samples();
        //Check that we are below 64, else use 64
        if count > 64 {
            count = 64;
        }
        count = count.next_power_of_two();
        match count {
            1 | 0 => ash::vk::SampleCountFlags::TYPE_1,
            2 => ash::vk::SampleCountFlags::TYPE_2,
            4 => ash::vk::SampleCountFlags::TYPE_4,
            8 => ash::vk::SampleCountFlags::TYPE_8,
            16 => ash::vk::SampleCountFlags::TYPE_16,
            32 => ash::vk::SampleCountFlags::TYPE_32,
            64 => ash::vk::SampleCountFlags::TYPE_64,
            _ => {
                #[cfg(feature = "logging")]
                log::warn!("Wrong sampling count, falling back to 1 sample");
                ash::vk::SampleCountFlags::TYPE_1
            }
        }
    }

    pub fn to_extent(&self) -> ash::vk::Extent3D {
        ash::vk::Extent3D {
            width: self.width(),
            height: self.height(),
            depth: self.depth(),
        }
    }
}

///Describes the usage of that image in several ways. Keep in mind that there are impossible combinations.
/// For instance you can't create a image that is depth AND color image.
#[derive(Clone, Copy)]
pub struct ImageUsage {
    ///If the image should be usable as the src of a command. For instance copying.
    pub transfer_src: bool,
    ///If the image should be able to receive data from a command.
    pub transfer_dst: bool,
    ///If it should be possible to sample from that image.
    pub sampled: bool,
    ///If this image should be able to be used as a Storage image. Needed for instance to store data in the image from a compute shader via `imageStore()`.
    pub storage: bool,
    pub color_attachment: bool,
    pub depth_attachment: bool,
    pub stencil_attachment: bool,
    pub transient_attachment: bool,
    pub input_attachment: bool,

    ///Can be used if you use an color storage image but don't want to set the color-attachment bit.
    pub color_aspect: bool,
    ///Can be used if you use an depth storage image but don't want to set the depth-attachment bit.
    pub depth_aspect: bool,
    ///Can be used if you use an stencil storage image but don't want to set the stencil-attachment bit.
    pub stencil_aspect: bool,
}

impl Default for ImageUsage {
    /// Initializes everything to false.
    fn default() -> Self {
        ImageUsage {
            transfer_src: false,
            transfer_dst: false,
            sampled: false,
            storage: false,
            color_attachment: false,
            depth_attachment: false,
            stencil_attachment: false,
            transient_attachment: false,
            input_attachment: false,
            color_aspect: false,
            depth_aspect: false,
            stencil_aspect: false,
        }
    }
}

impl ImageUsage {
    /// Returns true if the choosen combination is valid.
    pub fn is_valid(&self) -> bool {
        //Check if we have valid color/stencil or depth bit set
        if (self.depth_attachment && self.color_attachment)
            || (self.stencil_attachment && self.color_attachment)
            || (self.color_aspect && self.depth_aspect)
            || (self.stencil_aspect && self.color_aspect)
        {
            #[cfg(feature = "logging")]
            log::warn!("attachments can either be (depth and/or stencil) OR color attachments!");
            return false;
        }

        if !self.color_attachment
            && !self.depth_attachment
            && !self.stencil_attachment
            && !self.color_aspect
            && !self.depth_aspect
            && !self.stencil_aspect
        {
            #[cfg(feature = "logging")]
            log::warn!("An image must be either depth, stencil or depth attachment or must have one of the *_aspect flags set. Otherwise no aspect flags can be found.");
            return false;
        }

        //If we have a transient attachment we can't do any "post rendering" operations like sampling or transfer
        if self.transient_attachment
            && (self.transfer_dst || self.transfer_src || self.sampled || self.storage)
        {
            #[cfg(feature = "logging")]
            log::warn!(
                "If and attachment is transient, it cant be trans dst/src, sampeld or storage"
            );
            return false;
        }

        true
    }
    ///Returns true if the format can be used with that usage
    pub fn is_vaild_for_format(&self, format_feature: ash::vk::FormatFeatureFlags) -> bool {
        if self.color_attachment
            && !format_feature.contains(ash::vk::FormatFeatureFlags::COLOR_ATTACHMENT)
        {
            return false;
        }
        if (self.depth_attachment || self.stencil_attachment)
            && !format_feature.contains(ash::vk::FormatFeatureFlags::DEPTH_STENCIL_ATTACHMENT)
        {
            return false;
        }
        if self.sampled && !format_feature.contains(ash::vk::FormatFeatureFlags::SAMPLED_IMAGE) {
            return false;
        }
        if self.storage && !format_feature.contains(ash::vk::FormatFeatureFlags::STORAGE_IMAGE) {
            return false;
        }
        if self.transfer_src && !format_feature.contains(ash::vk::FormatFeatureFlags::TRANSFER_SRC)
        {
            return false;
        }
        if self.transfer_dst && !format_feature.contains(ash::vk::FormatFeatureFlags::TRANSFER_DST)
        {
            return false;
        }
        true
    }

    pub fn to_image_usage_flags(&self) -> ash::vk::ImageUsageFlags {
        let mut flag = ash::vk::ImageUsageFlags::empty();
        if self.transfer_src {
            flag = flag | ash::vk::ImageUsageFlags::TRANSFER_SRC;
        }
        if self.transfer_dst {
            flag = flag | ash::vk::ImageUsageFlags::TRANSFER_DST;
        }
        if self.sampled {
            flag = flag | ash::vk::ImageUsageFlags::SAMPLED;
        }
        if self.storage {
            flag = flag | ash::vk::ImageUsageFlags::STORAGE;
        }
        if self.color_attachment {
            flag = flag | ash::vk::ImageUsageFlags::COLOR_ATTACHMENT;
        }
        if self.depth_attachment || self.stencil_attachment {
            flag = flag | ash::vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT;
        }
        if self.transient_attachment {
            flag = flag | ash::vk::ImageUsageFlags::TRANSIENT_ATTACHMENT;
        }
        if self.input_attachment {
            flag = flag | ash::vk::ImageUsageFlags::INPUT_ATTACHMENT;
        }

        flag
    }

    pub fn to_image_aspect_flags(&self) -> ash::vk::ImageAspectFlags {
        let mut flags = ash::vk::ImageAspectFlags::empty();
        if self.color_attachment || self.color_aspect {
            flags = flags | ash::vk::ImageAspectFlags::COLOR;
        }
        if self.depth_attachment || self.depth_aspect {
            flags = flags | ash::vk::ImageAspectFlags::DEPTH;
        }
        if self.stencil_attachment || self.stencil_aspect {
            flags = flags | ash::vk::ImageAspectFlags::STENCIL;
        }

        flags
    }
}

///Describes several attributes a images has to define.
#[derive(Clone)]
pub struct ImageInfo {
    ///The general type of that image including extends and sampling information
    pub image_type: ImageType,
    ///The format at which it is saved
    pub format: ash::vk::Format,
    ///ComponentMapping between the channels of that image
    pub component_mapping: ash::vk::ComponentMapping,
    ///Mip mapping type of the image
    pub mip_mapping: MipLevel,
    ///Intended usage of that image
    pub usage: ImageUsage,
    ///How the memory will be used.
    pub memory_usage: MemoryUsage,
    //Tiling information for the image. Optimal is usually fine.
    pub tiling: ash::vk::ImageTiling,
}

impl ImageInfo {
    ///Checks several requirements before creating the ImageInfo.
    ///DefaultComponent mapping is rgba -> rgba
    ///Default MipLevel is MipLevel::Specific(1) (only the base image),
    pub fn new(
        image_type: ImageType,
        format: ash::vk::Format,
        component_mapping: Option<ash::vk::ComponentMapping>,
        mip_mapping: Option<MipLevel>,
        usage: ImageUsage,
        memory_usage: MemoryUsage,
        tiling: Option<ash::vk::ImageTiling>,
    ) -> Self {
        if let Err(er) = image_type.is_valid() {
            panic!("{}", er);
        }

        ImageInfo {
            image_type,
            format,
            component_mapping: if let Some(mapping) = component_mapping {
                mapping
            } else {
                ash::vk::ComponentMapping {
                    r: ash::vk::ComponentSwizzle::R,
                    g: ash::vk::ComponentSwizzle::G,
                    b: ash::vk::ComponentSwizzle::B,
                    a: ash::vk::ComponentSwizzle::A,
                }
            },
            mip_mapping: if let Some(mip) = mip_mapping {
                mip
            } else {
                MipLevel::Specific(1)
            },
            usage,
            memory_usage,
            tiling: if let Some(tiling_info) = tiling {
                tiling_info
            } else {
                ash::vk::ImageTiling::OPTIMAL
            },
        }
    }
    /// Generate the subresourceRange containing the whole image based on the current settings
    pub fn to_subresource_range(&self) -> ash::vk::ImageSubresourceRangeBuilder {
        let aspect_mask = self.usage.to_image_aspect_flags();

        ash::vk::ImageSubresourceRange::builder()
            .aspect_mask(aspect_mask)
            .base_mip_level(0)
            .level_count(self.mip_mapping.to_level_count(&self.image_type))
            .base_array_layer(0)
            .layer_count(self.image_type.array_layer())
    }

    ///Checks if the used format is compatible to the specified ImageUsage
    pub fn is_format_compatible(&self, device: Arc<Device>) -> bool {
        let image_format_flags = unsafe {
            let format_properties = device
                .get_instance()
                .vko()
                .get_physical_device_format_properties(
                    *device.get_physical_device().vko(),
                    self.format,
                );
            //Based on the tiling choose a feature
            if self.tiling == ash::vk::ImageTiling::OPTIMAL {
                format_properties.optimal_tiling_features
            } else {
                format_properties.linear_tiling_features
            }
        };

        self.usage.is_vaild_for_format(image_format_flags)
    }
}

/// A image primitive. It represents the vulkan image.
///
/// In general this image can be anything from a multi dimensional buffer to a framebuffer attachment to an texture.
/// Most of those properties are defined through the usage parameter in the `ImageInfo`.
///
/// If you created the image with `SharingMode::Exclusive`, be sure to transfer ownership to the queue
/// that needs it before usage.

/// *NOTE*: The image layout is always `UNDEFINED` created.
#[allow(dead_code)]
pub struct Image {
    image: ash::vk::Image,
    image_view: ash::vk::ImageView,

    image_info: ImageInfo,

    sharing_mode: SharingMode,
    memory_handle: Allocation,
    mem_size: u64,
    device: Arc<Device>,
}

impl Image {
    pub fn new(
        device: Arc<Device>,
        image_info: ImageInfo,
        sharing_mode: SharingMode,
    ) -> Result<Arc<Self>, AllocationError> {
        //Before doing anything, check if the image and the format we choose are correct.
        assert!(
            image_info.usage.is_valid(),
            "Image format has no valid combination of usage flags!"
        );
        //Also check the format
        assert!(
            image_info.is_format_compatible(device.clone()),
            "The choosen image format cannot support the image usage flags!"
        );

        //Now create the image, then upload it and finally store
        let queue_indices = sharing_mode.queue_indices();
        let image_create_info = ash::vk::ImageCreateInfo::builder()
            //.flags(image_info.) //TODO add flags ?
            .image_type(image_info.image_type.vko_type())
            .format(image_info.format)
            .extent(image_info.image_type.to_extent())
            .mip_levels(
                image_info
                    .mip_mapping
                    .to_level_count(&image_info.image_type),
            )
            .array_layers(image_info.image_type.array_layer())
            .samples(image_info.image_type.sample_count_flag())
            .tiling(image_info.tiling)
            .usage(image_info.usage.to_image_usage_flags())
            .sharing_mode(sharing_mode.vko())
            .queue_family_indices(queue_indices.as_slice())
            .initial_layout(ash::vk::ImageLayout::UNDEFINED);

        let is_linear = if let MemoryUsage::GpuToCpu = image_info.memory_usage {
            true
        } else {
            false
        }; //As far as i understood only needed for readback textures.
           //TODO make sure the stuff above is actually correct.

        let (image, memory_handle) = device.get_allocator_locked()?.create_image(
            device.vko(),
            &*image_create_info,
            image_info.memory_usage,
            is_linear,
            None,
        )?;

        //Create an image view that "sees" the whole image
        let create_info = ash::vk::ImageViewCreateInfo::builder()
            .format(image_info.format)
            .components(image_info.component_mapping)
            .subresource_range(*image_info.to_subresource_range())
            .image(image)
            .view_type(image_info.image_type.vko_view());

        //Now create the view
        let image_view = unsafe {
            match device.vko().create_image_view(&create_info, None) {
                Ok(res) => res,
                Err(er) => return Err(AllocationError::VulkanError(er)),
            }
        };

        Ok(Arc::new(Image {
            image,
            image_view,
            image_info,

            sharing_mode,
            mem_size: memory_handle.size(),
            memory_handle,
            device,
        }))
    }

    ///Creates an image and copies `data` to it. Returns a fence which is signaled when the upload has finished.
    pub fn from_data<T: Copy + Sized>(
        device: Arc<Device>,
        upload_queue: Arc<Queue>,
        mut image_info: ImageInfo,
        sharing_mode: SharingMode,
        data: Vec<T>,
    ) -> Result<(QueueFence, Arc<Self>), AllocationError> {
        //First check the usages, if transfer dst is missing, add it.
        image_info.usage.transfer_dst = true;

        //Now create the dst image, since we need to know the size etc.
        let image = Image::new(device.clone(), image_info.clone(), sharing_mode)?;
        //Always copy since we need the layout transitions.
        let fence = image.copy(upload_queue, data)?;

        Ok((fence, image))
    }

    pub fn vko(&self) -> &ash::vk::Image {
        &self.image
    }

    ///Returns the memory size of this image in bytes
    pub fn size(&self) -> u64 {
        self.mem_size
    }

    ///Creates an attachment description from this image.
    /// # Safety
    /// - This will return an `Err` if this image does not support any AttachmentUsage
    /// - Assumes that you don't care what the initial layout and therefor the initial content of this image was. It might be deleted when the image is used
    /// - Sets the final layout to ColorAttachment if the image's usage contains `color_attachment`, otherwise to DepthStencilAttachment
    /// - Will set StoreOp/LoadOp for color/depth and stencil attachments the same way (if needed).
    /// If you need finer control, use `AttachmentDescription::new(...)`.
    pub fn to_attachment_description(
        &self,
        load_op: ash::vk::AttachmentLoadOp,
        store_op: ash::vk::AttachmentStoreOp,
    ) -> framebuffer::AttachmentDescription {
        let final_layout = if self.image_info.usage.color_attachment {
            ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL
        } else {
            ash::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL
        };

        let (store, load, s_store, s_load) = if self.image_info.usage.color_attachment {
            //We are an color attachment so we only need this info.
            (store_op, load_op, None, None)
        } else {
            //We are stencil or depth, so we need to that the info for stencil as well
            (store_op, load_op, Some(store_op), Some(load_op))
        };

        framebuffer::AttachmentDescription::new(
            self.image_info.image_type.sample_count_flag(), //sample count
            self.format(),                                  //format
            store,                                          //color/depth store op
            load,                                           // color/depth load op
            s_store,                                        // stencil store
            s_load,                                         // stencil load
            None,                                           //initial layout
            final_layout,                                   //final layout
        )
    }
}

impl Drop for Image {
    fn drop(&mut self) {
        //First destroy the view into that image, then tell the allocator to destroy
        unsafe {
            //Delete the image and the image view
            self.device.vko().destroy_image_view(self.image_view, None);
        }

        self.device
            .get_allocator_locked()
            .expect("failed to destroy image!")
            .destroy_image(self.device.vko(), self.image, &self.memory_handle);
    }
}

impl UploadStrategy for Arc<Image> {
    fn map_data<T: Copy + Sized>(&self, data: Vec<T>) -> Result<(), AllocationError> {
        //Debug check if there is enough room for "data"
        debug_assert!(
            (data.len() * std::mem::size_of::<T>()) as u64 <= self.size(),
            "There is not enough room in this buffer for \"data\" supplied: {} !<= {}",
            data.len() * std::mem::size_of::<T>(),
            self.size()
        );

        if let Some(mut mapped_ptr) = self.memory_handle.mapped_ptr() {
            let mut data_slice = unsafe {
                ash::util::Align::new(
                    mapped_ptr.as_mut(),
                    std::mem::align_of::<T>() as u64,
                    self.memory_handle.size(),
                )
            };
            //Copy, then drop the ptr to unmap
            data_slice.copy_from_slice(&data);
            Ok(())
        } else {
            Err(AllocationError::MapFailed)
        }
    }

    fn copy_sync<T: Copy + Sized>(
        &self,
        command_buffer: Arc<CommandBuffer>,
        data: Vec<T>,
    ) -> Result<(), AllocationError> {
        //Debug check if there is enough room for "data"
        debug_assert!(
            (data.len() * std::mem::size_of::<T>()) as u64 <= self.size(),
            "There is not enough room in this buffer for \"data\" supplied: {} !<= {}",
            data.len() * std::mem::size_of::<T>(),
            self.size()
        );

        let (staging_fence, staging_buffer) = Buffer::from_data(
            self.device.clone(),
            command_buffer.get_queue(),
            data,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )?;
        staging_fence
            .wait(u64::MAX)
            .expect("Failed to wait for staging buffers fence!");

        command_buffer
            .pipeline_barrier(
                PipelineBarrier::<1>::new(
                    ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                    ash::vk::PipelineStageFlags::TRANSFER,
                )
                .with_image_barrier(
                    ImageMemoryBarrierBuilder::new(
                        self.clone(),
                        ImageLayoutTransitionEvent::Initialise(ImageLayout::TRANSFER_DST_OPTIMAL),
                    )
                    .with_access_flags_event(AccessMaskEvent::Acquire(AccessFlags::TRANSFER_WRITE)),
                )
                .build(),
            )
            .unwrap();

        //copy to device memory
        command_buffer
            .cmd_copy_buffer_to_image(
                staging_buffer,
                self.clone(),
                ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                vec![ash::vk::BufferImageCopy::builder()
                    .image_subresource(
                        ash::vk::ImageSubresourceLayers::builder()
                            .aspect_mask(self.image_info().usage.to_image_aspect_flags())
                            .layer_count(1)
                            .build(),
                    )
                    .image_extent(self.extent_3d())
                    .build()],
            )
            .expect("Could not add copy command for buffer!");

        Ok(())
    }

    ///Copies data to this image. `Self` will be in
    ///
    /// - SHARED_READ_ONLY_OPTIMAL layout
    /// - SHADER_READ mask
    fn copy<T: Copy + Sized>(
        &self,
        upload_queue: Arc<Queue>,
        data: Vec<T>,
    ) -> Result<QueueFence, AllocationError> {
        //Debug check if there is enough room for "data"
        debug_assert!(
            (data.len() * std::mem::size_of::<T>()) as u64 <= self.size(),
            "There is not enough room in this buffer for \"data\" supplied: {} !<= {}",
            data.len() * std::mem::size_of::<T>(),
            self.size()
        );

        //Start command buffer
        let command_pool = match CommandBufferPool::new(
            self.device.clone(),
            upload_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        ) {
            Ok(cbp) => cbp,
            Err(er) => return Err(AllocationError::UploadFailed(er)),
        };

        let cb: Arc<CommandBuffer> = match command_pool.alloc(1, false) {
            Ok(cbs) => cbs[0].clone(),
            Err(er) => return Err(AllocationError::UploadFailed(er)),
        };

        let _ = cb
            .begin_recording(true, false, false, None)
            .expect("Could not start upload command buffer");
        //Create ram memory, must be host coherent
        let (staging_fence, staging_buffer) = Buffer::from_data(
            self.device.clone(),
            upload_queue.clone(),
            data,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )?;
        staging_fence
            .wait(u64::MAX)
            .expect("Failed to wait for staging buffers fence!");

        cb.pipeline_barrier(
            PipelineBarrier::<1>::new(
                ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                ash::vk::PipelineStageFlags::TRANSFER,
            )
            .with_image_barrier(
                ImageMemoryBarrierBuilder::new(
                    self.clone(),
                    ImageLayoutTransitionEvent::Initialise(ImageLayout::TRANSFER_DST_OPTIMAL),
                )
                .with_access_flags_event(AccessMaskEvent::Acquire(AccessFlags::TRANSFER_WRITE)),
            )
            .build(),
        )
        .unwrap();

        //copy to device memory
        cb.cmd_copy_buffer_to_image(
            staging_buffer,
            self.clone(),
            ash::vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            vec![ash::vk::BufferImageCopy::builder()
                .image_subresource(
                    ash::vk::ImageSubresourceLayers::builder()
                        .aspect_mask(self.image_info().usage.to_image_aspect_flags())
                        .layer_count(1)
                        .build(),
                )
                .image_extent(self.extent_3d())
                .build()],
        )
        .expect("Could not add copy command for buffer!");

        cb.pipeline_barrier(
            PipelineBarrier::<1>::new(
                ash::vk::PipelineStageFlags::TRANSFER,
                ash::vk::PipelineStageFlags::ALL_COMMANDS,
            )
            .with_image_barrier(
                ImageMemoryBarrierBuilder::new(
                    self.clone(),
                    ImageLayoutTransitionEvent::Transition {
                        from: ImageLayout::TRANSFER_DST_OPTIMAL,
                        to: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    },
                )
                .with_access_flags_event(AccessMaskEvent::Transition {
                    from: AccessFlags::TRANSFER_WRITE,
                    to: AccessFlags::SHADER_READ,
                }),
            )
            .build(),
        )
        .unwrap();

        cb.end_recording()
            .expect("Could not end copy command buffer");
        //We have to create a staging buffer, and a command buffer, then upload the data and then wait for it to be finished.
        let submit_fence = upload_queue
            .queue_submit(vec![SubmitInfo::new(vec![], vec![cb], vec![])])
            .expect("Failed to Submit Command buffer!");

        Ok(submit_fence)
    }
}

///Defines functions all images have in common.
pub trait AbstractImage {
    fn as_vk_image_view(&self) -> &ash::vk::ImageView;
    fn as_vk_image(&self) -> &ash::vk::Image;
    fn format(&self) -> ash::vk::Format;
    fn image_type(&self) -> &ImageType;
    fn mip_level(&self) -> u32;
    fn sharing_mode(&self) -> &SharingMode;

    fn image_info(&self) -> &ImageInfo;

    /// Returns the 2D extend of that image. Note that depth is missing if this is a 3D image.
    fn extent(&self) -> ash::vk::Extent2D;
    fn extent_3d(&self) -> ash::vk::Extent3D;
    /// Creates a new image barrier based on the supplied resources.
    /// You can either choose to transition the whole image, by supplying `None` to the `range`, or
    /// specify it your self.
    /// # Safety
    /// - Assumes that this barrier is submitted before any newly created barrier is submitted since it changes the inner state of `SharingMode` (to the new queue) and the inner state of `access_mask`. So if you create a barrier `A` and a barrier `B`, then `A` must be submitted to a command buffer before `B`. This holds true across queues etc. Use Semaphores etc. to handle that.
    /// - Assumes that the supplied new layout is supported by the images usage flags.
    /// - if no `old_layout` is supplied, the `UNDEFINED` is used.
    #[deprecated(note = "use `ImageMemoryBarrierBuilder` with a `PipelineBarrierBuilder` instead.")]
    fn new_image_barrier(
        &self,
        old_layout: Option<ash::vk::ImageLayout>,
        new_layout: Option<ash::vk::ImageLayout>,
        old_queue: Option<Arc<Queue>>,
        new_queue: Option<Arc<Queue>>,
        old_access_mask: Option<ash::vk::AccessFlags>,
        new_access_mask: Option<ash::vk::AccessFlags>,
        range: Option<ash::vk::ImageSubresourceRange>,
    ) -> ash::vk::ImageMemoryBarrier;
}

impl AbstractImage for Image {
    fn as_vk_image_view(&self) -> &ash::vk::ImageView {
        &self.image_view
    }
    fn as_vk_image(&self) -> &ash::vk::Image {
        &self.image
    }
    fn format(&self) -> ash::vk::Format {
        self.image_info.format
    }
    fn image_type(&self) -> &ImageType {
        &self.image_info.image_type
    }
    fn mip_level(&self) -> u32 {
        self.image_info
            .mip_mapping
            .to_level_count(&self.image_info.image_type)
    }
    fn sharing_mode(&self) -> &SharingMode {
        &self.sharing_mode
    }
    fn extent(&self) -> ash::vk::Extent2D {
        ash::vk::Extent2D::builder()
            .width(self.image_info.image_type.width())
            .height(self.image_info.image_type.height())
            .build()
    }

    fn extent_3d(&self) -> ash::vk::Extent3D {
        ash::vk::Extent3D::builder()
            .width(self.image_info.image_type.width())
            .height(self.image_info.image_type.height())
            .depth(self.image_info.image_type.depth())
            .build()
    }

    fn image_info(&self) -> &ImageInfo {
        &self.image_info
    }

    fn new_image_barrier(
        &self,
        old_layout: Option<ash::vk::ImageLayout>,
        new_layout: Option<ash::vk::ImageLayout>,
        old_queue: Option<Arc<Queue>>,
        new_queue: Option<Arc<Queue>>,
        old_access_mask: Option<ash::vk::AccessFlags>,
        new_access_mask: Option<ash::vk::AccessFlags>,
        range: Option<ash::vk::ImageSubresourceRange>,
    ) -> ash::vk::ImageMemoryBarrier {
        let mut builder = ash::vk::ImageMemoryBarrier::builder();

        //Transition queue if one is set and we are exclusive on one queue
        if self.sharing_mode().is_exclusive() {
            match (old_queue, new_queue) {
                //Queue transfer
                (Some(oq), Some(nq)) => {
                    builder = builder
                        .src_queue_family_index(oq.get_family().get_family_index())
                        .dst_queue_family_index(nq.get_family().get_family_index());
                }
                //Acquire
                (None, Some(nq)) => {
                    builder = builder.dst_queue_family_index(nq.get_family().get_family_index());
                }
                //Release
                (Some(oq), None) => {
                    builder = builder.src_queue_family_index(oq.get_family().get_family_index());
                }
                //No transfer
                (None, None) => {
                    //no queue transfer
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
            }
        } else {
            builder = builder
                .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
        }

        match (old_access_mask, new_access_mask) {
            (Some(oa), Some(na)) => {
                builder = builder.src_access_mask(oa).dst_access_mask(na);
            }
            (None, Some(na)) => {
                builder = builder.dst_access_mask(na);
            }
            (Some(oa), None) => {
                builder = builder.src_access_mask(oa);
            }
            (None, None) => {
                //Nothingo
            }
        }

        //transition layout
        match (old_layout, new_layout) {
            (Some(old), Some(new)) => {
                builder = builder.old_layout(old).new_layout(new);
            }
            (None, Some(new)) => {
                builder = builder
                    .old_layout(ash::vk::ImageLayout::UNDEFINED)
                    .new_layout(new);
            }
            (Some(old), None) => {
                builder = builder
                    .old_layout(old)
                    .new_layout(ash::vk::ImageLayout::UNDEFINED);
            }
            (None, None) => {
                builder = builder
                    .old_layout(ash::vk::ImageLayout::UNDEFINED)
                    .new_layout(ash::vk::ImageLayout::UNDEFINED);
            }
        }

        if let Some(r) = range {
            builder = builder.subresource_range(r);
        } else {
            builder = builder.subresource_range(self.image_info.to_subresource_range().build());
        }

        //now add the image and the whole subresource range
        builder = builder.image(self.image);

        builder.build()
    }
}

/// Represents a single swapchain image. The difference to a normal image is, that this image can become invalid when the Swapchain surface changes.
/// *NOTE*: The image layout is undefined when creating the image.
pub struct SwapchainImage {
    image: ash::vk::Image,
    image_view: ash::vk::ImageView,
    image_info: ImageInfo,
    device: Arc<Device>,
}

impl SwapchainImage {
    ///Takes a swapchain image and creates a view from it. Then stores everything to be used.
    /// TODO check if all the default values are okay
    pub fn from_swapchain_image(
        device: Arc<Device>,
        format: ash::vk::Format,
        extend: ash::vk::Extent2D,
        image: ash::vk::Image,
        usage: ImageUsage,
    ) -> ash::prelude::VkResult<Arc<Self>> {
        //TODO Check if this subresource always applies
        let subresource_range = ash::vk::ImageSubresourceRange {
            aspect_mask: ash::vk::ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        };

        let component_mapping = ash::vk::ComponentMapping {
            r: ash::vk::ComponentSwizzle::R,
            g: ash::vk::ComponentSwizzle::G,
            b: ash::vk::ComponentSwizzle::B,
            a: ash::vk::ComponentSwizzle::A,
        };

        let create_info = ash::vk::ImageViewCreateInfo::builder()
            .format(format.clone())
            .components(component_mapping) //Always map back to rgba
            .subresource_range(subresource_range)
            .image(image.clone())
            .view_type(ash::vk::ImageViewType::TYPE_2D);

        //Now create the view
        let image_view = unsafe {
            match device.vko().create_image_view(&create_info, None) {
                Ok(res) => res,
                Err(er) => return Err(er),
            }
        };

        let image_info = ImageInfo::new(
            ImageType::Image2D {
                width: extend.width,
                height: extend.height,
                samples: 1,
            },
            format,
            Some(component_mapping),
            None,
            usage,
            MemoryUsage::Unknown, //doesn't really matter since its only a view
            None,
        );

        Ok(Arc::new(SwapchainImage {
            image: image,
            image_view: image_view,
            image_info: image_info,
            device: device,
        }))
    }

    pub fn vko(&self) -> &ash::vk::Image {
        &self.image
    }
}

impl Drop for SwapchainImage {
    fn drop(&mut self) {
        unsafe {
            //Delete the image and the image view
            self.device.vko().destroy_image_view(self.image_view, None);
            //Do not destroy the image since it is owned by the swapchain... only the view
        }
    }
}

impl AbstractImage for SwapchainImage {
    fn as_vk_image_view(&self) -> &ash::vk::ImageView {
        &self.image_view
    }
    fn as_vk_image(&self) -> &ash::vk::Image {
        &self.image
    }
    fn format(&self) -> ash::vk::Format {
        self.image_info.format.clone()
    }
    fn image_type(&self) -> &ImageType {
        &self.image_info.image_type
    }
    fn mip_level(&self) -> u32 {
        self.image_info
            .mip_mapping
            .to_level_count(self.image_type())
    }
    fn sharing_mode(&self) -> &SharingMode {
        &SharingMode::Exclusive
    }

    fn image_info(&self) -> &ImageInfo {
        &self.image_info
    }

    fn extent(&self) -> ash::vk::Extent2D {
        ash::vk::Extent2D::builder()
            .width(self.image_type().width())
            .height(self.image_type().height())
            .build()
    }

    fn extent_3d(&self) -> ash::vk::Extent3D {
        ash::vk::Extent3D::builder()
            .width(self.image_info.image_type.width())
            .height(self.image_info.image_type.height())
            .depth(self.image_info.image_type.depth())
            .build()
    }
    ///Will always return `None` since a swapchain image is handled by the swapchain.
    fn new_image_barrier(
        &self,
        old_layout: Option<ash::vk::ImageLayout>,
        new_layout: Option<ash::vk::ImageLayout>,
        old_queue: Option<Arc<Queue>>,
        new_queue: Option<Arc<Queue>>,
        old_access_mask: Option<ash::vk::AccessFlags>,
        new_access_mask: Option<ash::vk::AccessFlags>,
        range: Option<ash::vk::ImageSubresourceRange>,
    ) -> ash::vk::ImageMemoryBarrier {
        let mut builder = ash::vk::ImageMemoryBarrier::builder();

        //Always transition queue ownership if needed.
        match (old_queue, new_queue) {
            //Queue transfer
            (Some(oq), Some(nq)) => {
                builder = builder
                    .src_queue_family_index(oq.get_family().get_family_index())
                    .dst_queue_family_index(nq.get_family().get_family_index());
            }
            //Acquire
            (None, Some(nq)) => {
                builder = builder.dst_queue_family_index(nq.get_family().get_family_index());
            }
            //Release
            (Some(oq), None) => {
                builder = builder.src_queue_family_index(oq.get_family().get_family_index());
            }
            //No transfer
            (None, None) => {
                //no queue transfer
                builder = builder
                    .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                    .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
            }
        }

        match (old_access_mask, new_access_mask) {
            (Some(oa), Some(na)) => {
                builder = builder.src_access_mask(oa).dst_access_mask(na);
            }
            (None, Some(na)) => {
                builder = builder.dst_access_mask(na);
            }
            (Some(oa), None) => {
                builder = builder.src_access_mask(oa);
            }
            (None, None) => {
                //Nothingo
            }
        }

        //transition layout
        if let Some(layout) = new_layout {
            let old_layout = if let Some(old_l) = old_layout {
                old_l
            } else {
                ash::vk::ImageLayout::UNDEFINED
            };
            builder = builder.old_layout(old_layout).new_layout(layout);
        }

        if let Some(r) = range {
            builder = builder.subresource_range(r);
        } else {
            builder = builder.subresource_range(self.image_info.to_subresource_range().build());
        }

        //now add the image and the whole subresource range
        builder = builder.image(self.image);

        builder.build()
    }
}
