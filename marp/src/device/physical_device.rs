/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::device::queue::QueueFamily;
use crate::instance::Instance;
use crate::miscellaneous;
use crate::swapchain::surface::Surface;

use ash;
use std::sync::Arc;

pub struct PhysicalDevice {
    instance: Arc<Instance>,
    //The raw vulkan Physical device
    raw_device: ash::vk::PhysicalDevice,
    //Features this device supports
    features: ash::vk::PhysicalDeviceFeatures,
    ///Extensions this device supports
    extensions: Vec<miscellaneous::DeviceExtension>,
    ///The queue Families on this physical device
    queue_families: Vec<QueueFamily>,
}

impl PhysicalDevice {
    pub fn find_physical_device(
        instance: Arc<Instance>,
    ) -> Result<Vec<Arc<PhysicalDevice>>, ash::vk::Result> {
        unsafe {
            let enum_devs = instance.vko().enumerate_physical_devices();
            match enum_devs {
                Ok(p_devs) => {
                    //Go through each one and make an wrapped one
                    Ok(p_devs
                        .into_iter()
                        .map(|dev| {
                            let physical_device = dev;

                            //Create the infos for this device. Should be save since we can be sure that this physical device exists, which is the only requirement.
                            let features =
                                instance.vko().get_physical_device_features(physical_device);
                            let extensions: Vec<_> = match instance
                                .vko()
                                .enumerate_device_extension_properties(physical_device)
                            {
                                Ok(exts) => exts
                                    .into_iter()
                                    .map(|ext| {
                                        let cstr =
                                            std::ffi::CStr::from_ptr(ext.extension_name.as_ptr());

                                        miscellaneous::DeviceExtension::new(
                                            cstr.to_str()
                                                .expect("Failed to convert cstr to str")
                                                .to_owned(),
                                            ext.spec_version,
                                        )
                                    })
                                    .collect(),
                                Err(er) => {
                                    #[cfg(feature = "logging")]
                                    log::error!(
                                        "Could not find physical device extensions: {:?}",
                                        er
                                    );
                                    Vec::new()
                                }
                            };

                            //Finally build all queues from the queue families
                            let queues =
                                QueueFamily::find_queue_familys(instance.clone(), &physical_device);

                            Arc::new(PhysicalDevice {
                                instance: instance.clone(),
                                raw_device: dev,
                                features,
                                extensions,
                                queue_families: queues,
                            })
                        })
                        .collect())
                }
                Err(er) => Err(er),
            }
        }
    }

    ///Returns the QueueFamilies that belong to this physical device
    pub fn get_queue_families(&self) -> &Vec<QueueFamily> {
        &self.queue_families
    }

    ///Returns a queue family with this `queue_family_index`
    pub fn get_queue_family_by_index(&self, queue_family_index: u32) -> Option<&QueueFamily> {
        for q in self.queue_families.iter() {
            if q.get_family_index() == queue_family_index {
                return Some(q);
            }
        }
        None
    }

    ///Returns the inner vulkan representation of this physical device
    pub fn vko(&self) -> &ash::vk::PhysicalDevice {
        &self.raw_device
    }

    ///Returns true if this physical device can present on `surface`, when using a queue on a queue family with `queue_family_index`.
    pub fn can_present_on_surface(
        &self,
        surface: &Arc<dyn Surface + Send + Sync>,
        queue_family_index: u32,
    ) -> bool {
        unsafe {
            surface
                .get_surface_loader()
                .get_physical_device_surface_support(
                    self.raw_device,
                    queue_family_index,
                    *surface.vko(),
                )
                .expect("Could not get physical device surface support!")
        }
    }

    ///Returns Ok with the index of a queue family we can present on or Err if this device does not support the surface OR no queue is a graphics queue.
    pub fn find_present_queue_family(
        &self,
        surface: &Arc<dyn Surface + Send + Sync>,
    ) -> Result<u32, String> {
        //Check if we have
        // - one queue family that support graphics and present
        // - that our device supports the surface

        let mut good_queues = Vec::new();
        for queue_fam in self.queue_families.iter() {
            if queue_fam
                .get_properties()
                .queue_flags
                .contains(ash::vk::QueueFlags::GRAPHICS)
                && self.can_present_on_surface(surface, queue_fam.get_family_index())
            {
                good_queues.push(queue_fam.get_family_index());
            }
        }

        //Check if we have one else error
        if let Some(idx) = good_queues.first() {
            Ok(*idx)
        } else {
            Err("Could not find appropiate queue on device".to_string())
        }
    }

    ///Returns the features this device supports
    pub fn get_features(&self) -> &ash::vk::PhysicalDeviceFeatures {
        &self.features
    }

    pub fn get_extensions(&self) -> &[miscellaneous::DeviceExtension] {
        &self.extensions
    }

    pub fn get_device_properties(&self) -> ash::vk::PhysicalDeviceProperties {
        unsafe {
            self.instance
                .vko()
                .get_physical_device_properties(*self.vko())
        }
    }

    pub fn get_instance(&self) -> Arc<Instance> {
        self.instance.clone()
    }
}
