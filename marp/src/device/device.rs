/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::device::{PhysicalDevice, Queue, QueueFamily};
use crate::instance::Instance;
use crate::memory::VMA;
use crate::miscellaneous::DeviceExtension;
use ash::{
    self,
    vk::{BaseOutStructure, ExtendsDeviceCreateInfo},
};
use std::collections::BTreeMap;
use std::iter::Iterator;
use std::{
    ffi::c_void,
    sync::{Arc, LockResult, RwLock, RwLockWriteGuard},
};

pub struct DeviceBuilder {
    instance: Arc<Instance>,
    physical_device: Arc<PhysicalDevice>,
    queue_families: Vec<(QueueFamily, f32)>,
    extensions: Vec<DeviceExtension>,
    features: Option<ash::vk::PhysicalDeviceFeatures>,
    additonal_features: Vec<Box<dyn ExtendsDeviceCreateInfo>>,
}

impl DeviceBuilder {
    pub fn new(
        instance: Arc<Instance>,
        physical_device: Arc<PhysicalDevice>,
        queue_families: Vec<(QueueFamily, f32)>,
    ) -> Self {
        DeviceBuilder {
            instance,
            physical_device,
            queue_families,
            extensions: Vec::new(),
            features: None,
            additonal_features: Vec::new(),
        }
    }

    pub fn with_extension(mut self, device_extension: DeviceExtension) -> Self {
        //TODO check they are not already present... but probably is useless
        self.extensions.push(device_extension);
        self
    }

    pub fn with_device_features(mut self, features: ash::vk::PhysicalDeviceFeatures) -> Self {
        self.features = Some(features);
        self
    }

    pub fn with_additional_feature<T: 'static>(mut self, feature: T) -> Self
    where
        T: ExtendsDeviceCreateInfo,
    {
        self.additonal_features.push(Box::new(feature));
        self
    }

    pub fn build(mut self) -> Result<(Arc<Device>, Vec<Arc<Queue>>), ash::vk::Result> {
        //Since each family should be in the iterator once, filter for each family a set of
        // priorities
        let mut family_set: BTreeMap<QueueFamily, Vec<f32>> = BTreeMap::new();
        for q in self.physical_device.get_queue_families() {
            family_set.insert(*q, vec![]);
        }

        //Now fill in our priorities
        //This will match the index in the queue_families to the actual index within the created queues.
        //Format: index_in_param, family_index, queue_index_in_family
        let mut index_match: Vec<(usize, usize, usize)> = Vec::new();

        for (qf_index, (q, p)) in family_set.iter_mut().enumerate() {
            for (p_index, (p_queue, prio)) in self.queue_families.iter().enumerate() {
                if p_queue == q {
                    let queue_index_on_family = p.len();
                    p.push(*prio);

                    index_match.push((p_index, qf_index, queue_index_on_family));
                }
            }
        }

        //Create a devicequeueinfo for each
        let queue_infos: Vec<ash::vk::DeviceQueueCreateInfoBuilder> = family_set
            .iter()
            .filter_map(|(queue, prio)| {
                if prio.len() > 0 {
                    Some(queue.to_queue_info(&prio))
                } else {
                    None
                }
            })
            .collect();

        //At least register swapchain extension
        let loaded_extensions: Vec<DeviceExtension> = if self.extensions.len() == 0 {
            vec![DeviceExtension::new("VK_KHR_swapchain".to_string(), 1)]
        } else {
            self.extensions
        };

        //Check if each extension is supported by the physical device
        let supported = self.physical_device.get_extensions();
        let device_extensions: Vec<_> = {
            for ex in loaded_extensions.iter() {
                if !ex.contained(&supported) {
                    #[cfg(feature = "logging")]
                    log::error!("Device extension was not present: {}", ex.get_name());
                    #[cfg(feature = "logging")]
                    log::info!("Supported device extensions are:");
                    for _e in supported {
                        #[cfg(feature = "logging")]
                        log::info!("\t{}", _e.get_name());
                    }
                    return Err(ash::vk::Result::ERROR_EXTENSION_NOT_PRESENT);
                }
            }
            loaded_extensions
                .iter()
                .map(|ex| ex.as_c_str().as_ptr())
                .collect()
        };

        let features = if let Some(feat) = self.features {
            //Currently not checking agains physical device features. TODO
            feat
        } else {
            self.physical_device.get_features().clone()
        };

        //Convert queue infos into a build slice
        let queue_info_slice: Vec<ash::vk::DeviceQueueCreateInfo> =
            queue_infos.into_iter().map(|qi| qi.build()).collect();
        //Take it and build a device info from it.
        let device_create_info = ash::vk::DeviceCreateInfo::builder()
            .queue_create_infos(&queue_info_slice)
            .enabled_extension_names(&device_extensions)
            .enabled_features(&features);

        //if there are additonal features, build the pointer queue and add it to the builder
        let mut create_info = device_create_info.build();
        if self.additonal_features.len() > 0 {
            //Chain the featuers together similar to the builders push
            let chain = self
                .additonal_features
                .iter_mut()
                .fold(
                    None,
                    |last: Option<&mut Box<dyn ExtendsDeviceCreateInfo>>, new| {
                        if let Some(last) = last {
                            let mut new_ptr = new.as_mut() as *mut _ as *mut BaseOutStructure;
                            unsafe {
                                (*new_ptr).p_next =
                                    last.as_mut() as *mut _ as *mut BaseOutStructure;
                            }
                            Some(new)
                        } else {
                            Some(new)
                        }
                    },
                )
                .unwrap();
            create_info.p_next = chain.as_ref() as *const _ as *const c_void;
        }

        //Create the device
        let device_unarc = unsafe {
            match self
                .instance
                .vko()
                .create_device(*self.physical_device.vko(), &create_info, None)
            {
                Ok(dev) => dev,
                Err(er) => return Err(er),
            }
        };

        let allocator = VMA::new(
            self.instance.clone(),
            device_unarc.clone(),
            self.physical_device.clone(),
            false,
        );

        //Fill the struct, then enumerate all created queues
        let device = Arc::new(Device {
            device: Arc::new(device_unarc),
            instance: self.instance.clone(),
            physical_device: self.physical_device.clone(),
            allocator: Some(allocator),
            loaded_extensions,
        });

        //Find each created queue per queue family
        let mut all_queues = BTreeMap::new();

        for (return_index, family_index, queue_index) in index_match {
            unsafe {
                //TODO test if a: family and b: index is in range otherwise panic/return err
                let this_queue = device
                    .vko()
                    .get_device_queue(family_index as u32, queue_index as u32);
                all_queues.insert(
                    return_index,
                    Queue::new(
                        this_queue,
                        device.clone(),
                        *self
                            .physical_device
                            .get_queue_family_by_index(family_index as u32)
                            .expect("Could not get family for created index"),
                    ),
                );
            }
        }
        let final_queues = all_queues.into_iter().map(|(_k, v)| v).collect();
        Ok((device, final_queues))
    }
}

///A device is created from an `Instance` and a `PhysicalDevice`.
/// It is used for most operation on vulkan related code.
pub struct Device {
    //The allocator for this device. The Option is currently needed for correct dropping behavoir.
    //Hopefully this can be changed later...
    allocator: Option<RwLock<VMA>>,

    //The raw vulkan device
    device: Arc<ash::Device>,
    instance: Arc<Instance>,
    physical_device: Arc<PhysicalDevice>,
    loaded_extensions: Vec<DeviceExtension>,
}

impl Device {
    /// Creates a new device from a physical device and the supplied queue families.
    /// A Queue family has always to specify its priority (ranging from 0..1) which is the second item in the iterator's item type.
    /// Since you can crate more then one queue from one queue family you can actually specify more than one priority value.
    /// For each value a queue is create on that family. In most cases however, you should be fine with one.
    ///
    /// The queues are returned in the order they are supplied. Keep in mind that there is a limit  of queues per family.
    ///
    /// You can also provide a list of extensions that should be supported/activated by the device.
    /// If you don't, the Swapchain extensions and all features of the device are activated.
    /// Further more you can also specify which device features you need.
    /// # Warning
    /// The supplied features are currently not checked against the physical device features.

    pub fn new(
        instance: Arc<Instance>,
        physical_device: Arc<PhysicalDevice>,
        queue_families: Vec<(QueueFamily, f32)>,
        extensions: Option<&[DeviceExtension]>,
        features: Option<ash::vk::PhysicalDeviceFeatures>,
    ) -> Result<(Arc<Self>, Vec<Arc<Queue>>), ash::vk::Result> {
        let mut builder = DeviceBuilder::new(instance, physical_device, queue_families);
        if let Some(ext) = extensions {
            for e in ext {
                builder = builder.with_extension(e.clone());
            }
        }

        if let Some(features) = features {
            builder = builder.with_device_features(features);
        }

        builder.build()
    }

    pub fn vko(&self) -> &ash::Device {
        &*self.device
    }

    pub fn get_instance(&self) -> Arc<Instance> {
        self.instance.clone()
    }

    pub fn get_physical_device(&self) -> Arc<PhysicalDevice> {
        self.physical_device.clone()
    }
    ///Returns the standard allocator. Use that to either allocate raw vulkan images/buffers yourself, or to obtain small
    /// buffer pools.
    pub fn get_allocator(&self) -> &RwLock<VMA> {
        self.allocator.as_ref().unwrap()
    }

    ///Returns the write locked allocator. Be sure to not create a lockup!
    pub fn get_allocator_locked<'a>(&'a self) -> LockResult<RwLockWriteGuard<'a, VMA>> {
        self.allocator.as_ref().unwrap().write()
    }

    ///Waits until the device idles.
    pub fn wait_idle(&self) -> Result<(), ash::vk::Result> {
        unsafe { self.device.device_wait_idle() }
    }

    ///Returns true if this format is supported for the image usage provided and the tiling mode specified
    pub fn is_format_supported(
        &self,
        format: ash::vk::Format,
        usage: &crate::image::ImageUsage,
        tiling_mode: ash::vk::ImageTiling,
    ) -> bool {
        let format_properties = unsafe {
            self.instance
                .vko()
                .get_physical_device_format_properties(*self.physical_device.vko(), format)
        };

        if tiling_mode == ash::vk::ImageTiling::OPTIMAL {
            usage.is_vaild_for_format(format_properties.optimal_tiling_features)
        } else {
            usage.is_vaild_for_format(format_properties.linear_tiling_features)
        }
    }

    ///Returns the first, usable format in this list
    pub fn get_useable_format(
        &self,
        formats: Vec<ash::vk::Format>,
        usage: &crate::image::ImageUsage,
        tiling_mode: ash::vk::ImageTiling,
    ) -> Option<ash::vk::Format> {
        formats
            .into_iter()
            .filter_map(|format| {
                if self.is_format_supported(format, usage, tiling_mode) {
                    Some(format)
                } else {
                    None
                }
            })
            .nth(0)
    }

    pub fn is_extension_loaded(&self, extension: DeviceExtension) -> bool {
        for e in self.loaded_extensions.iter() {
            if e.get_name() == extension.get_name() && e.get_version() >= extension.get_version() {
                return true;
            }
        }
        false
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        let _ = self
            .allocator
            .take()
            .expect("Failed to drop device before alloctor!");
        unsafe {
            self.device.destroy_device(None);
        }
    }
}
