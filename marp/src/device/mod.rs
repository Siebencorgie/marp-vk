/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Realation of the Device to other components
//!
//! The Device presents the logical VulkanDevice the whole implementation works with
//! when it tries to submit commands or allocate memory etc.
//! A Device can have several `Queue`s which can be thought of as different
//! threads on the GPU. The biggest difference to CPU queues is that usually a queue
//! has a certain workload it is best at. For instance only a GraphicsQueue can process
//! graphics related commands. But a ComputeQueue can run Async to this one and utilize
//! ComputeUnits that are otherwise unused.
//! A TransferQueue is best at transferring Buffers between GPU and CPU, use it!

//! # Queue Families and Queues
//! A QueueFamily represents a collection of queues which all can do the "same things".
//! For instance, there are queues that can do graphical work, and compute, and transfer work.
//! But there might be other queues that only can do a subset of those features.
//! For more information, have a look at [this excellent documentation](https://www.khronos.org/registry/vulkan/specs/1.0-wsi_extensions/html/vkspec.html#devsandqueues-queues)

///Represents a physical device of an instance.
pub mod physical_device;

///Represents the logical VulkanDevice
pub mod device;

///Represents a queue on an `Device`
pub mod queue;

pub use self::device::{Device, DeviceBuilder};
pub use self::physical_device::PhysicalDevice;
pub use self::queue::Queue;
pub use self::queue::QueueFamily;
pub use self::queue::SubmitInfo;
