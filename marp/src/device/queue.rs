/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ash;

use crate::command_buffer::*;
use crate::device::device::Device;
use crate::instance::Instance;
use crate::sync::*;
use std::sync::Arc;
///Describes which kind of work a QueueFamily can do
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct QueueType {
    pub graphics: bool,
    pub compute: bool,
    pub transfer: bool,
}

///Defines single queue family. Keep in mind that a queue family always belongs to a certain physical device.
#[derive(Clone, Copy, Debug)]
pub struct QueueFamily {
    ///Which work it cna do
    queue_type: QueueType,
    ///The properties this one is derived from
    properties: ash::vk::QueueFamilyProperties,
    ///The index of this queue family on the device
    queue_family_index: u32,
}

impl PartialEq<QueueFamily> for QueueFamily {
    fn eq(&self, other: &QueueFamily) -> bool {
        self.queue_family_index == other.get_family_index()
    }
}

impl std::cmp::Ord for QueueFamily {
    fn cmp(&self, other: &QueueFamily) -> std::cmp::Ordering {
        self.queue_family_index.cmp(&other.get_family_index())
    }
}

impl PartialOrd for QueueFamily {
    fn partial_cmp(&self, other: &QueueFamily) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for QueueFamily {}

impl QueueFamily {
    ///Finds all queue families for a physical device.
    pub fn find_queue_familys(
        instance: Arc<Instance>,
        physical_device: &ash::vk::PhysicalDevice,
    ) -> Vec<Self> {
        unsafe {
            instance
                .vko()
                .get_physical_device_queue_family_properties(*physical_device)
                .iter()
                .enumerate()
                .map(|(idx, queue_family)| {
                    let queue_type = {
                        let mut tmp = QueueType {
                            graphics: false,
                            compute: false,
                            transfer: false,
                        };

                        if queue_family
                            .queue_flags
                            .contains(ash::vk::QueueFlags::GRAPHICS)
                        {
                            tmp.graphics = true;
                        }
                        if queue_family
                            .queue_flags
                            .contains(ash::vk::QueueFlags::COMPUTE)
                        {
                            tmp.compute = true;
                        }
                        if queue_family
                            .queue_flags
                            .contains(ash::vk::QueueFlags::TRANSFER)
                        {
                            tmp.transfer = true;
                        }
                        tmp
                    };

                    QueueFamily {
                        queue_type,
                        properties: *queue_family,
                        queue_family_index: idx as u32,
                    }
                })
                .collect()
        }
    }

    pub fn get_properties(&self) -> &ash::vk::QueueFamilyProperties {
        &self.properties
    }

    pub fn get_family_index(&self) -> u32 {
        self.queue_family_index
    }

    pub fn to_queue_info<'a>(
        &self,
        priorities: &'a [f32],
    ) -> ash::vk::DeviceQueueCreateInfoBuilder<'a> {
        assert!(
            priorities.len() <= self.properties.queue_count as usize,
            "Queue count should <= queuefamily.queue_count"
        );

        ash::vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(self.queue_family_index)
            .queue_priorities(priorities)
    }
    ///Returns which type of workloads this queue family supports.
    pub fn get_queue_type(&self) -> QueueType {
        self.queue_type
    }
}

///Collection of infos used to submit work to a queue.
pub struct SubmitInfo {
    ///The semaphores that have to be signaled before this work can start.
    #[allow(dead_code)]
    wait_semaphores: Vec<Arc<Semaphore>>,
    inner_wait_semaphores: Vec<ash::vk::Semaphore>,
    ///The pipeline stages that have to be compleat before this command buffer is started. (this makes overlapping work possible between CBs, so try to make this as tight as possible).
    wait_dst_stage_mask: Vec<ash::vk::PipelineStageFlags>,
    ///The Command buffers that are executed on the queue with the specified requirements.
    command_buffers: Vec<Arc<CommandBuffer>>,
    inner_command_buffers: Vec<ash::vk::CommandBuffer>,
    ///The semaphores that are signaled when the work of all command buffers has finished.
    #[allow(dead_code)]
    signal_semaphores: Vec<Arc<Semaphore>>,
    inner_signal_semaphore: Vec<ash::vk::Semaphore>,
}

impl SubmitInfo {
    ///The semaphores that have to be signaled before this work can start.
    ///
    ///`wait_semaphores: Vec<Arc<Semaphore>>,`
    ///The pipeline stages that have to be compleat before this command buffer is started. (this makes overlapping work possible between CBs, so try to make this as tight as possible).
    ///
    ///`wait_dst_stage_mask: Vec<ash::vk::PipelineStageFlags>,`
    ///The Command buffers that are executed on the queue with the specified requirements.
    ///
    ///`command_buffers: Vec<Arc<CommandBuffer>>,`
    ///The semaphores that are signaled when the work of all command buffers has finished.
    ///
    ///`signal_semaphores: Vec<Arc<Semaphore>>,`
    pub fn new(
        wait_semaphores_and_stage: Vec<(Arc<Semaphore>, ash::vk::PipelineStageFlags)>,
        command_buffers: Vec<Arc<CommandBuffer>>,
        signal_semaphores: Vec<Arc<Semaphore>>,
    ) -> Self {
        let (wait_semaphores, inner_w_sems, wait_dst_stage_mask) =
            wait_semaphores_and_stage.iter().fold(
                (vec![], vec![], vec![]),
                |(mut c_sem, mut c_inner_sem, mut c_wait), (sem, mask)| {
                    c_sem.push(sem.clone());
                    c_inner_sem.push(*sem.vko());
                    c_wait.push(*mask);

                    (c_sem, c_inner_sem, c_wait)
                },
            );

        let cbs: Vec<ash::vk::CommandBuffer> = command_buffers.iter().map(|c| *c.vko()).collect();

        let s_sems: Vec<ash::vk::Semaphore> = signal_semaphores.iter().map(|s| *s.vko()).collect();

        SubmitInfo {
            wait_semaphores,
            inner_wait_semaphores: inner_w_sems,
            wait_dst_stage_mask,
            command_buffers,
            inner_command_buffers: cbs,
            signal_semaphores,
            inner_signal_semaphore: s_sems,
        }
    }

    pub fn useses_graphics(&self) -> bool {
        self.command_buffers.iter().fold(
            false,
            |current, c| if c.useses_graphics() { true } else { current },
        )
    }

    pub fn vko<'a>(&'a self) -> ash::vk::SubmitInfoBuilder<'a> {
        let sub_info = ash::vk::SubmitInfo::builder()
            .wait_semaphores(&self.inner_wait_semaphores)
            .wait_dst_stage_mask(self.wait_dst_stage_mask.as_slice())
            .command_buffers(&self.inner_command_buffers)
            .signal_semaphores(&self.inner_signal_semaphore);

        sub_info
    }
}

///A Queue on a vulkan `Device`
pub struct Queue {
    queue: ash::vk::Queue,
    queue_family: QueueFamily,
    device: Arc<Device>,
}

impl Queue {
    pub fn new(vk_queue: ash::vk::Queue, device: Arc<Device>, family: QueueFamily) -> Arc<Self> {
        Arc::new(Queue {
            queue: vk_queue,
            queue_family: family,
            device,
        })
    }

    pub fn get_family(&self) -> &QueueFamily {
        &self.queue_family
    }

    ///Submits a one or more commands which are executed on this queue.
    /// Returns an Fence, representing the moment the operations have finished. Keep in mind that it also holds all resources that
    /// are needed on the queue. Those have to outlive the fence.
    pub fn queue_submit(
        &self,
        submit_infos: Vec<SubmitInfo>,
    ) -> Result<Arc<Fence<Vec<SubmitInfo>>>, ash::vk::Result> {
        let submit_uses_graphics =
            submit_infos.iter().fold(
                false,
                |current, si| if si.useses_graphics() { true } else { current },
            );
        if submit_uses_graphics && !self.queue_family.get_queue_type().graphics {
            #[cfg(feature = "logging")]
            log::error!("QueueType: {:?}", self.queue_family.get_queue_type());
            panic!("Tried to submit graphics command buffer on non-graphics queue");
        }
        //We unpack each of the submit infos into the corresbonding
        //vk struct. TODO find a way that the resources outlive the
        let vk_infos: Vec<_> = submit_infos
            .iter()
            .map(|sub_info| sub_info.vko().build())
            .collect();

        //That is the fence guarding the resources and signaling the
        // point at which the work has finished.
        let fence = match Fence::new(self.device.clone(), false, Some(submit_infos)) {
            Ok(fe) => fe,
            Err(er) => return Err(er),
        };

        unsafe {
            match self
                .device
                .vko()
                .queue_submit(*self.vko(), &vk_infos, *fence.vko())
            {
                Ok(_) => Ok(fence),
                Err(er) => Err(er),
            }
        }
    }

    ///Locks until this queue idles for the first time
    pub fn wait_idle(&self) -> Result<(), ash::vk::Result> {
        unsafe {
            match self.device.vko().queue_wait_idle(*self.vko()) {
                Ok(_) => Ok(()),
                Err(er) => Err(er),
            }
        }
    }

    pub fn vko(&self) -> &ash::vk::Queue {
        &self.queue
    }

    pub fn get_device(&self) -> Arc<Device> {
        self.device.clone()
    }
}
