/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::device::*;
use ash;
use std::sync::Arc;

/// Defines which options should be used when the sampler is build.
/// Keep in mind to use valid values for the floats. If you don't want to
/// fill every field, there is also a `Default` implementation which fills everything to
/// ```ignore
/// SamplerBuilder{
///    mag_filter: ash::vk::Filter::LINEAR,
///    min_filter: ash::vk::Filter::LINEAR,
///    mipmap_mode: ash::vk::SamplerMipmapMode::LINEAR,
///    address_mode_u: ash::vk::SamplerAddressMode::REPEAT,
///    address_mode_v: ash::vk::SamplerAddressMode::REPEAT,
///    address_mode_w: ash::vk::SamplerAddressMode::REPEAT,
///    mip_lod_bias: 0.0,
///    anisotropy_enabled: false,
///    max_anisotropy: 1.0,
///    compare_enabled: false,
///    compare_op: ash::vk::ALWAYS,
///    min_lod: 0.0,
///    max_lod: 1.0,
///    border_color: ash::vk::BorderColor::IN_OPAQUE_BLACK,
///    unnormalized_coordinates: false,
/// }
/// ```
#[derive(Clone, Copy, Debug)]
pub struct SamplerBuilder {
    pub mag_filter: ash::vk::Filter,
    pub min_filter: ash::vk::Filter,
    pub mipmap_mode: ash::vk::SamplerMipmapMode,
    pub address_mode_u: ash::vk::SamplerAddressMode,
    pub address_mode_v: ash::vk::SamplerAddressMode,
    pub address_mode_w: ash::vk::SamplerAddressMode,
    pub mip_lod_bias: f32,
    /// if you want to use that, make sure the feature is enabled and supported
    pub anisotropy_enable: bool,
    /// Note this should be a number x: `0<= x <= 16`
    pub max_anisotropy: f32,
    pub compare_enable: bool,
    pub compare_op: ash::vk::CompareOp,
    pub min_lod: f32,
    pub max_lod: f32,
    pub border_color: ash::vk::BorderColor,
    pub unnormalized_coordinates: bool,
}

impl Default for SamplerBuilder {
    /// initializes as:
    /// ```ignore
    /// SamplerBuilder{
    ///    mag_filter: ash::vk::Filter::LINEAR,
    ///    min_filter: ash::vk::Filter::LINEAR,
    ///    mipmap_mode: ash::vk::SamplerMipmapMode::LINEAR,
    ///    address_mode_u: ash::vk::SamplerAddressMode::REPEAT,
    ///    address_mode_v: ash::vk::SamplerAddressMode::REPEAT,
    ///    address_mode_w: ash::vk::SamplerAddressMode::REPEAT,
    ///    mip_lod_bias: 0.0,
    ///    anisotropy_enabled: false,
    ///    max_anisotropy: 1.0,
    ///    compare_enabled: false,
    ///    compare_op: ash::vk::ALWAYS,
    ///    min_lod: 0.0,
    ///    max_lod: 1.0,
    ///    border_color: ash::vk::BorderColor::IN_OPAQUE_BLACK,
    ///    unnormalized_coordinates: false,
    /// }
    /// ```
    fn default() -> Self {
        SamplerBuilder {
            mag_filter: ash::vk::Filter::LINEAR,
            min_filter: ash::vk::Filter::LINEAR,
            mipmap_mode: ash::vk::SamplerMipmapMode::LINEAR,
            address_mode_u: ash::vk::SamplerAddressMode::REPEAT,
            address_mode_v: ash::vk::SamplerAddressMode::REPEAT,
            address_mode_w: ash::vk::SamplerAddressMode::REPEAT,
            mip_lod_bias: 0.0,
            anisotropy_enable: false,
            max_anisotropy: 1.0,
            compare_enable: false,
            compare_op: ash::vk::CompareOp::ALWAYS,
            min_lod: 0.0,
            max_lod: 1.0,
            border_color: ash::vk::BorderColor::INT_OPAQUE_BLACK,
            unnormalized_coordinates: false,
        }
    }
}

impl SamplerBuilder {
    ///Build a sampler from the currently set options.
    pub fn build(self, device: Arc<Device>) -> Result<Arc<Sampler>, ash::vk::Result> {
        let create_info = ash::vk::SamplerCreateInfo::builder()
            .mag_filter(self.mag_filter)
            .min_filter(self.min_filter)
            .mipmap_mode(self.mipmap_mode)
            .address_mode_u(self.address_mode_u)
            .address_mode_v(self.address_mode_v)
            .address_mode_w(self.address_mode_w)
            .mip_lod_bias(self.mip_lod_bias)
            .anisotropy_enable(self.anisotropy_enable)
            .max_anisotropy(self.max_anisotropy)
            .compare_enable(self.compare_enable)
            .compare_op(self.compare_op)
            .min_lod(self.min_lod)
            .max_lod(self.max_lod)
            .border_color(self.border_color)
            .unnormalized_coordinates(self.unnormalized_coordinates);

        let sampler = unsafe {
            match device.vko().create_sampler(&create_info, None) {
                Ok(sampler) => sampler,
                Err(er) => return Err(er),
            }
        };

        Ok(Arc::new(Sampler { sampler, device }))
    }
}

pub struct Sampler {
    sampler: ash::vk::Sampler,
    device: Arc<Device>,
}

impl Sampler {
    pub fn vko(&self) -> &ash::vk::Sampler {
        &self.sampler
    }
}

impl Drop for Sampler {
    fn drop(&mut self) {
        unsafe {
            self.device.vko().destroy_sampler(self.sampler, None);
        }
    }
}
