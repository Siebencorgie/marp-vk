/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![deny(warnings)]

///Vulkan API
pub extern crate ash;

///Handles buffers, their allocation and provides a buffer-pool for frequently updated buffers.
pub mod buffer;
///Records commands which at some point are executed on the gpu.
pub mod command_buffer;
///Implements a wrapped version of the Debug extension.
pub mod debug;
///Describes to the gpu in what way data is send over.
pub mod descriptor;
/// Contains a logical vulkan device which is used to interface with a physical GPU
pub mod device;
/// Together with the Renderpass it describes how which images are accessed in a renderpass.
pub mod framebuffer;
/// A single image, similar to a buffer but can be sampled via a sampler. Can be texture, a attachment image (for the frame buffer) or just (miss)used as a normal buffer, for instance for 3d data. Image data is usually faster to access for the gpu.
pub mod image;
/// Is an vulkan instance which can interface to many logical devices.
pub mod instance;
/// Handles the memory allocation for buffer pools, buffers and images as well as their mapping.
pub mod memory;
///Small interface to make loading extnsions and layers easier.
pub mod miscellaneous;
///Describes how the gpu should work with a given draw or dispatch command. For instance blending while rasterizing or how big the dispatched compute shader work group is for compute pipelines.
pub mod pipeline;
/// Describes how (image)data is sampled on the gpu
pub mod sampler;
///Contains ShaderModule and ShaderStages which link you Vulkan code to your shaders.
pub mod shader;
///Interfaces to a swapchain (how depends on the current platform). This is actually not an vulkan concept but used to often that I choose to take it in.
pub mod swapchain;
///Collects most synchronization objects.
pub mod sync;
///Provides helpful functions to make certain repetetive work easier. However, most of those functions are aimed at testing the library atm.
pub mod util;
