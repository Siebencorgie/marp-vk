/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Desciptor Set vs Desciptor
//! A Descriptor set collects several resoures. Theese
//! resources are bound to bindings 1-n.
//! When starting shader the implementation can bind
//! several descriptorsets to the shader.
//!
//! However set binding per (descriptor set) is specified while adding it to the descriptor set.
//!
//! # Usage in Marp-vk
//! When building a descriptor set, you mus ensure that the pool you use for the builder actually supports the kind
//! of descriptor you want to allocate form it (for instance uniform buffer or storage images) and you must ensure
//! that it supports the count of the object you want to allocate from it.
//! So if you want to bind 10 times some kind of storage image, make sure that you added a `DescriptorPoolSize` with count >10
//! while calling `::new()` on the `StdDescriptorPool`.
//!
//! # The easy but static way
//! If you don't have to allocate new stuff per frame you can also use a `create_static_descriptor_set()`.
//! It will allocate a new pool exactly matching the resources you supply. However, the creation might take
//! longer then with a pre allocated pool, since pool allocation needs time.
//!
//! Try to use this type as often as possible.
//!
//! # Currently unimplemented
//! Note that currently the Storage/Uniform buffer dynamic are not implemented.
//!
//! # How to choose DescriptorType and access_layout
//! ## Descriptor type:
//! The descriptor type should be chosen based on what the image/buffer should be when accessed through this descriptor.
//! Buffers are usually a uniform buffer (when using medium big, sized data) or a storage buffer if the shader has no information about the size of that buffer.
//! Similar an image should be choosen based on the access type. However, both the buffer and the image *must* support the `Usage`. So if you decide to use `ash::vk::DescriptorType::STORAGE_BUFFER`
//! then the buffer should also have the `BufferUsage::storage_buffer` flag set to true.
//!
//! ## Access Layout
//! Additionally the image have an image layout. The Layout can be set to general which is the slowest but easiest way (for the programmer) to access that image. However
//! for performance reasons a layout should be set accordingly to when the image is accessed and how.
//! since the descriptor has to know that you'll have to specified it as well.
//!
//! The rough part is to transition the image to the right ImageLayout whenever it is accessed. However this hopefully will be handled hopefully by Marp in the future.
//!
use crate::buffer::*;
use crate::device::*;
use crate::image::*;
use crate::sampler::Sampler;
use crate::util::SeaHashMap;
use ash;
use std::sync::{Arc, Mutex, MutexGuard};

///Small specialized constants that can be pushed to shader stages really quick.
pub mod push_constant;
use ash::vk::DescriptorBindingFlags;
use ash::vk::DescriptorSetLayoutBindingFlagsCreateInfo;
pub use push_constant::PushConstant;

pub struct StdDescriptorPool {
    pool: Mutex<ash::vk::DescriptorPool>,
    device: Arc<Device>,
}

impl StdDescriptorPool {
    ///Creates a new pool that can do the following:
    /// - create as many bindings for a certain pool size as specified in each `DescriptorPoolSize`
    /// - create as many descriptorsets as specified in `set_count`
    ///
    /// So if your sizes are (2x UNIFORM_BUFFER) and (2xSAMPLED_IMAGE) and you set `set_count` to 2 you could allocate:
    /// - 1 set with 1 uniform buffer and 2 sampled images as well as a set with a single uniform buffer
    /// - 1 set with 2 uniform buffers and 2 sampled images and no additionan set
    /// - 2 set with 1 uniform buffer and sampled image each
    /// - etc.
    /// The pool is always created with the `FREE_DESCRIPTOR_SET` flag, since it is needed to de-allocate single descriptorsets
    pub fn new<'a>(
        device: Arc<Device>,
        sizes: &'a [ash::vk::DescriptorPoolSize],
        set_count: u32,
    ) -> Result<Arc<Self>, ash::vk::Result> {
        let pool_info = ash::vk::DescriptorPoolCreateInfo::builder()
            .pool_sizes(sizes)
            .max_sets(set_count)
            .flags(ash::vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET);

        let pool = unsafe {
            match device.vko().create_descriptor_pool(&pool_info, None) {
                Ok(dp) => dp,
                Err(er) => return Err(er),
            }
        };

        Ok(Arc::new(StdDescriptorPool {
            pool: Mutex::new(pool),
            device,
        }))
    }

    pub fn free_descriptor_set(&self, desc_set: &[ash::vk::DescriptorSet]) {
        unsafe {
            self.device
                .vko()
                .free_descriptor_sets(
                    *self.pool.lock().expect("Faild to lock descriptor pool"),
                    desc_set,
                )
                .expect("Failed to unwrap descriptor");
        }
    }

    pub fn device(&self) -> Arc<Device> {
        self.device.clone()
    }

    pub fn vko<'a>(&'a self) -> MutexGuard<'a, ash::vk::DescriptorPool> {
        self.pool
            .lock()
            .expect("failed to lock descriptor set pool")
    }
}

pub trait DescriptorPool {
    fn next(&self) -> DescriptorSetBuilder;
}

impl DescriptorPool for Arc<StdDescriptorPool> {
    fn next(&self) -> DescriptorSetBuilder {
        DescriptorSetBuilder {
            device: self.device(),
            pool: self.clone(),
            resources: SeaHashMap::default(),
            binding_flags: Vec::new(),
        }
    }
}

impl Drop for StdDescriptorPool {
    fn drop(&mut self) {
        unsafe {
            self.device.vko().destroy_descriptor_pool(*self.vko(), None);
        }
    }
}

///Describes one binding in a Descriptor set.
///
/// If you are binding an array of images/buffer or sampler then each item in the *_info fields describes how the corresbonding image is bound from the image/buffer of sampler field.
/// For the creation try to use the `new_*` functions, since the take care of the `_info` structs.
pub enum DescResource {
    ///An image (or array of images) bound to `binding_id` with an optional sampler.
    Image {
        binding_id: u32,
        image: Vec<(Arc<Image>, Option<Arc<Sampler>>, ash::vk::ImageLayout)>,
        descriptor_type: ash::vk::DescriptorType,
        image_info: Vec<ash::vk::DescriptorImageInfo>,
    },
    ///An buffer (or array of buffers) bound to `binding_id`.
    Buffer {
        binding_id: u32,
        buffer: Vec<Arc<Buffer>>,
        descriptor_type: ash::vk::DescriptorType,
        buffer_info: Vec<ash::vk::DescriptorBufferInfo>,
    },
    ///A sampler (or array of Samplers) bound to `binding_id`.
    Sampler {
        binding_id: u32,
        sampler: Vec<Arc<Sampler>>,
        sampler_info: Vec<ash::vk::DescriptorImageInfo>,
    },
}

impl DescResource {
    ///Describes a binding for an image, or an array of images. You can supply a Sampler, which will be used with this image. However, you must supply the ImageLayout
    /// this image will be in when used with the descriptor.
    pub fn new_image(
        binding_id: u32,
        images: Vec<(Arc<Image>, Option<Arc<Sampler>>, ash::vk::ImageLayout)>,
        descriptor_type: ash::vk::DescriptorType,
    ) -> Self {
        let image_infos = images
            .iter()
            .map(|(img, sampler, layout)| {
                ash::vk::DescriptorImageInfo {
                    sampler: if let Some(s) = sampler {
                        *s.vko()
                    } else {
                        ash::vk::Sampler::null()
                    }, //the sampler if present
                    image_view: *img.as_vk_image_view(),

                    image_layout: *layout,
                }
            })
            .collect::<Vec<_>>();

        DescResource::Image {
            binding_id,
            image: images,
            descriptor_type,
            image_info: image_infos,
        }
    }

    pub fn new_buffer(
        binding_id: u32,
        buffers: Vec<Arc<Buffer>>,
        descriptor_type: ash::vk::DescriptorType,
    ) -> Self {
        let buffer_infos = buffers
            .iter()
            .map(|buffer| {
                ash::vk::DescriptorBufferInfo::builder()
                    .buffer(*buffer.vko())
                    .offset(0) //TODO supply this?
                    .range(ash::vk::WHOLE_SIZE) //TODO might change ...
                    .build()
            })
            .collect::<Vec<_>>();

        DescResource::Buffer {
            binding_id,
            buffer: buffers,
            descriptor_type,
            buffer_info: buffer_infos,
        }
    }

    pub fn new_sampler(binding_id: u32, samplers: Vec<Arc<Sampler>>) -> Self {
        let sampler_infos = samplers
            .iter()
            .map(|s| {
                ash::vk::DescriptorImageInfo {
                    sampler: *s.vko(),
                    image_view: ash::vk::ImageView::null(), //no image view for sampler
                    image_layout: ash::vk::ImageLayout::UNDEFINED, //also no layout
                }
            })
            .collect::<Vec<_>>();

        DescResource::Sampler {
            binding_id,
            sampler: samplers,
            sampler_info: sampler_infos,
        }
    }

    pub fn to_desc_set_layout_binding(&self) -> ash::vk::DescriptorSetLayoutBinding {
        match self {
            DescResource::Image {
                binding_id,
                image,
                descriptor_type,
                image_info: _,
            } => {
                ash::vk::DescriptorSetLayoutBinding::builder()
                    .binding(*binding_id)
                    .descriptor_type(*descriptor_type)
                    .descriptor_count(image.len() as u32)
                    .stage_flags(ash::vk::ShaderStageFlags::ALL) //TODO make a bit better?
                    .build()
            }
            DescResource::Buffer {
                binding_id,
                buffer,
                descriptor_type,
                buffer_info: _,
            } => {
                ash::vk::DescriptorSetLayoutBinding::builder()
                    .binding(*binding_id)
                    .descriptor_type(*descriptor_type)
                    .descriptor_count(buffer.len() as u32)
                    .stage_flags(ash::vk::ShaderStageFlags::ALL) //TODO make a bit better?
                    .build()
            }
            DescResource::Sampler {
                binding_id,
                sampler,
                sampler_info: _,
            } => {
                ash::vk::DescriptorSetLayoutBinding::builder()
                    .binding(*binding_id)
                    .descriptor_type(ash::vk::DescriptorType::SAMPLER)
                    .descriptor_count(sampler.len() as u32)
                    .stage_flags(ash::vk::ShaderStageFlags::ALL) //TODO make a bit better?
                    .build()
            }
        }
    }

    pub fn get_id(&self) -> u32 {
        match self {
            DescResource::Image {
                binding_id,
                image: _,
                descriptor_type: _,
                image_info: _,
            } => *binding_id,
            DescResource::Buffer {
                binding_id,
                buffer: _,
                descriptor_type: _,
                buffer_info: _,
            } => *binding_id,
            DescResource::Sampler {
                binding_id,
                sampler: _,
                sampler_info: _,
            } => *binding_id,
        }
    }

    ///Generates update information for the supplied descriptor set. The Resource and the binding id are derived from the resource itself.
    pub fn write_descriptor_set<'a>(
        &'a self,
        descriptor_set: &ash::vk::DescriptorSet,
    ) -> ash::vk::WriteDescriptorSetBuilder<'a> {
        match self {
            DescResource::Image {
                binding_id,
                image: _,
                descriptor_type,
                image_info,
            } => ash::vk::WriteDescriptorSet::builder()
                .dst_set(*descriptor_set)
                .dst_binding(*binding_id)
                .image_info(&image_info)
                .descriptor_type(*descriptor_type),
            DescResource::Buffer {
                binding_id,
                buffer: _,
                descriptor_type,
                buffer_info,
            } => ash::vk::WriteDescriptorSet::builder()
                .dst_set(*descriptor_set)
                .dst_binding(*binding_id)
                .buffer_info(&buffer_info)
                .descriptor_type(*descriptor_type),
            DescResource::Sampler {
                binding_id,
                sampler: _,
                sampler_info,
            } => ash::vk::WriteDescriptorSet::builder()
                .dst_set(*descriptor_set)
                .dst_binding(*binding_id)
                .image_info(&sampler_info)
                .descriptor_type(ash::vk::DescriptorType::SAMPLER),
        }
    }

    pub fn descriptor_type(&self) -> &ash::vk::DescriptorType {
        match self {
            DescResource::Image {
                binding_id: _,
                image: _,
                descriptor_type,
                image_info: _,
            } => descriptor_type,
            DescResource::Buffer {
                binding_id: _,
                buffer: _,
                descriptor_type,
                buffer_info: _,
            } => descriptor_type,
            DescResource::Sampler {
                binding_id: _,
                sampler: _,
                sampler_info: _,
            } => &ash::vk::DescriptorType::SAMPLER,
        }
    }

    ///Returns how many descriptors of this type need to be allocated for this resource.
    pub fn num_descriptors(&self) -> usize {
        match self {
            DescResource::Image {
                binding_id: _,
                image,
                descriptor_type: _,
                image_info: _,
            } => image.len(),
            DescResource::Buffer {
                binding_id: _,
                buffer,
                descriptor_type: _,
                buffer_info: _,
            } => buffer.len(),
            DescResource::Sampler {
                binding_id: _,
                sampler,
                sampler_info: _,
            } => sampler.len(),
        }
    }
}

#[derive(Debug)]
pub enum BindingError {
    ///If this binding ID is already in use
    BindingIdInUse,
    ///When the descriport type is not supported by the resource.
    DescriptorTypeNotSupported,
    ///A generic vulkan error at some other place. Usually from within one of the unsafe parts.
    VkResult(ash::vk::Result),
}

impl From<ash::vk::Result> for BindingError {
    fn from(error: ash::vk::Result) -> Self {
        BindingError::VkResult(error)
    }
}

pub struct DescriptorSetBuilder {
    device: Arc<Device>,
    //The parent pool
    pool: Arc<StdDescriptorPool>,
    //All to be bound resources sorted by their desired binding location
    resources: SeaHashMap<u32, DescResource>,
    //collects all extensions for the pNext chain,
    binding_flags: Vec<DescriptorBindingFlags>,
}

impl DescriptorSetBuilder {
    pub fn add(&mut self, res: DescResource) -> Result<(), BindingError> {
        if self.resources.contains_key(&res.get_id()) {
            return Err(BindingError::BindingIdInUse);
        }

        //TODO Check that the supplied image also supports the descriptor type?

        self.resources.insert(res.get_id(), res);
        Ok(())
    }

    pub fn add_flag(&mut self, flag: DescriptorBindingFlags) {
        self.binding_flags.push(flag);
    }

    pub fn build(self) -> Result<Arc<DescriptorSet>, ash::vk::Result> {
        //TODO accept extensions for descriptorset layout building.

        //1. Gen bindings
        let bindings: Vec<ash::vk::DescriptorSetLayoutBinding> = self
            .resources
            .iter()
            .map(|(_k, v)| v.to_desc_set_layout_binding())
            .collect();

        //2. create_descriptorset_layout(from_binidings)
        let mut descriptor_info =
            ash::vk::DescriptorSetLayoutCreateInfo::builder().bindings(bindings.as_slice());

        //Check if any flags should be set, if so, add them
        let mut flags =
            DescriptorSetLayoutBindingFlagsCreateInfo::builder().binding_flags(&self.binding_flags);
        if self.binding_flags.len() > 0 {
            descriptor_info = descriptor_info.push_next(&mut flags);
        }

        let descriptor_set_layout = unsafe {
            match self
                .device
                .vko()
                .create_descriptor_set_layout(&descriptor_info, None)
            {
                Ok(dsl) => [dsl],
                Err(er) => return Err(er),
            }
        };
        //3. alloc_descriptorset(from_pool).and(bindings)
        let desc_alloc_info = ash::vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(*self.pool.vko())
            .set_layouts(&descriptor_set_layout);
        let descriptor_set = unsafe {
            match self.device.vko().allocate_descriptor_sets(&desc_alloc_info) {
                Ok(ds) => ds,
                Err(er) => return Err(er),
            }
        };

        let write_inst = self.resources.to_write_instructions(&descriptor_set[0]);

        //Now update the descriptor set
        unsafe {
            self.device.vko().update_descriptor_sets(&write_inst, &[]); //TODO check if we can use the copys?
        }

        Ok(Arc::new(DescriptorSet {
            device: self.device,
            pool: self.pool,
            resources: self
                .resources
                .into_iter()
                .map(|(id, val)| (id, Mutex::new(val)))
                .collect(),
            descriptor_set: descriptor_set[0],
            descriptor_set_layout: descriptor_set_layout[0],
        }))
    }
}

///Collects a set of resources which are in turn described by their descriptors.
pub struct DescriptorSet {
    device: Arc<Device>,
    pool: Arc<StdDescriptorPool>,

    //All images sorted based on the binding id
    resources: SeaHashMap<u32, Mutex<DescResource>>,

    descriptor_set_layout: ash::vk::DescriptorSetLayout,
    descriptor_set: ash::vk::DescriptorSet,
}

impl DescriptorSet {
    ///Returns the layout of this descriptor set
    pub fn layout<'a>(&'a self) -> &'a ash::vk::DescriptorSetLayout {
        &self.descriptor_set_layout
    }

    ///Updates the binding at `index` or the resource with the new DescResources.
    /// #Safety
    /// 1. The supplied DescResource has to match the old one!
    /// 2. This command is not synchronized. Make sure that no command buffer accesses this descriptor-set while
    /// its updated, otherwise the command buffer becomes undefined.
    pub fn update(&self, resource: DescResource) -> Result<(), String> {
        match self.resources.get(&resource.get_id()) {
            Some(res) => {
                let mut res = res.lock().unwrap();
                if res.descriptor_type() != resource.descriptor_type() {
                    return Err("Descriptor types don't match!".to_string());
                }
                //update desc resource for this binding.
                *res = resource;

                let write_instruction = res.write_descriptor_set(&self.descriptor_set);
                //Seems to be save to update
                unsafe {
                    self.device
                        .vko()
                        .update_descriptor_sets(&[write_instruction.build()], &[]);
                }

                Ok(())
            }
            None => return Err("No resource on that Id!".to_string()),
        }
    }

    pub fn vko<'a>(&'a self) -> &'a ash::vk::DescriptorSet {
        &self.descriptor_set
    }
}

impl Drop for DescriptorSet {
    fn drop(&mut self) {
        unsafe {
            //Ignore errors since we are anyways dropping
            let _ = self
                .device
                .vko()
                .free_descriptor_sets(*self.pool.vko(), &[self.descriptor_set]);
            self.device
                .vko()
                .destroy_descriptor_set_layout(self.descriptor_set_layout, None);
        }
    }
}

///Creates a prefectly matching `StdDescriptorPool` for the supplied resources and builds the corresbonding descriptor set.
pub fn create_static_descriptor_set(
    device: Arc<Device>,
    resources: Vec<DescResource>,
) -> Result<Arc<DescriptorSet>, BindingError> {
    let sizes = resources.to_descriptor_pool_sizes();
    let pool = StdDescriptorPool::new(device.clone(), sizes.as_slice(), 1)?; //We want only one descriptor set!

    //Now setup the new descriptor set by passing in all the images buffers and sampelers
    let mut descriptor_builder = pool.next();
    for res in resources {
        descriptor_builder
            .add(res)
            .expect("Failed to add descriptor resource");
    }
    let descriptor_set = descriptor_builder.build()?;
    Ok(descriptor_set)
}

///Provides several methodes to make information extraction from DescResources faster
pub trait DescriptorResCollection {
    fn to_descriptor_pool_sizes(&self) -> Vec<ash::vk::DescriptorPoolSize>;
    /// Transforms each DescResource into a write information based on the supplied binding Id in the DescResource.
    /// # Safety
    /// Assumes that the binding Ids are unique across the Vec.
    fn to_write_instructions(
        &self,
        descriptor_set: &ash::vk::DescriptorSet,
    ) -> Vec<ash::vk::WriteDescriptorSet>;
}

impl DescriptorResCollection for Vec<DescResource> {
    fn to_descriptor_pool_sizes(&self) -> Vec<ash::vk::DescriptorPoolSize> {
        let mut map: SeaHashMap<ash::vk::DescriptorType, u32> = SeaHashMap::default();

        for res in self.iter() {
            match map.get_mut(&res.descriptor_type()) {
                Some(val) => *val += res.num_descriptors() as u32,
                None => {
                    let _ = map.insert(*res.descriptor_type(), res.num_descriptors() as u32);
                }
            }
        }

        let sizes = map
            .into_iter()
            .map(|(k, v)| ash::vk::DescriptorPoolSize {
                ty: k,
                descriptor_count: v,
            })
            .collect();

        sizes
    }

    fn to_write_instructions(
        &self,
        descriptor_set: &ash::vk::DescriptorSet,
    ) -> Vec<ash::vk::WriteDescriptorSet> {
        let collection: Vec<_> = self
            .iter()
            .map(|dr| dr.write_descriptor_set(descriptor_set))
            .collect();

        let non_builder: Vec<ash::vk::WriteDescriptorSet> =
            collection.into_iter().map(|dr| dr.build()).collect();

        non_builder
    }
}

impl DescriptorResCollection for SeaHashMap<u32, DescResource> {
    fn to_descriptor_pool_sizes(&self) -> Vec<ash::vk::DescriptorPoolSize> {
        let mut map: SeaHashMap<ash::vk::DescriptorType, u32> = SeaHashMap::default();

        for (_k, res) in self.iter() {
            match map.get_mut(&res.descriptor_type()) {
                Some(val) => *val += 1,
                None => {
                    let _ = map.insert(*res.descriptor_type(), 1);
                }
            }
        }

        let sizes = map
            .into_iter()
            .map(|(k, v)| ash::vk::DescriptorPoolSize {
                ty: k,
                descriptor_count: v,
            })
            .collect();

        sizes
    }

    fn to_write_instructions(
        &self,
        descriptor_set: &ash::vk::DescriptorSet,
    ) -> Vec<ash::vk::WriteDescriptorSet> {
        let collection: Vec<_> = self
            .iter()
            .map(|(_k, dr)| dr.write_descriptor_set(descriptor_set))
            .collect();

        let non_builder: Vec<ash::vk::WriteDescriptorSet> =
            collection.into_iter().map(|dr| dr.build()).collect();

        non_builder
    }
}
