/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::device::Device;
use ash;
use std::sync::Arc;

///Describes how an attachment to a Renderpass is treated.
pub struct AttachmentDescription {
    format: ash::vk::Format,
    sampling: ash::vk::SampleCountFlags,
    store_op: ash::vk::AttachmentStoreOp,
    load_op: ash::vk::AttachmentLoadOp,
    stencil_store_op: ash::vk::AttachmentStoreOp,
    stencil_load_op: ash::vk::AttachmentLoadOp,
    initial_layout: ash::vk::ImageLayout,
    final_layout: ash::vk::ImageLayout,
}

impl AttachmentDescription {
    ///Describes how the image is used as an attachment. Some information is derived from the image,
    /// Other information has to be specified.
    /// Before using make sure:
    /// - ** That the supplied `final_layout` is not `UNDEFINED` or `PREINITIALIZED`
    /// - **TODO check** If you know the current layout out supply it to `initial_layout` else `UNDEFINED` is used
    /// - If you store anything use the `OP_STORE` else use `DONT_CARE`, if you store something, make sure it has the `ACCESS_*_WRITE` set.
    pub fn new(
        sampling: ash::vk::SampleCountFlags,
        format: ash::vk::Format,
        store_op: ash::vk::AttachmentStoreOp,
        load_op: ash::vk::AttachmentLoadOp,
        stencil_store_op: Option<ash::vk::AttachmentStoreOp>,
        stencil_load_op: Option<ash::vk::AttachmentLoadOp>,
        initial_layout: Option<ash::vk::ImageLayout>,
        final_layout: ash::vk::ImageLayout,
    ) -> Self {
        //First check some stuff
        debug_assert!(
            final_layout != ash::vk::ImageLayout::UNDEFINED && final_layout != ash::vk::ImageLayout::PREINITIALIZED,
            "The final_layout on an AttachmentDescription should not be UNDEFINED or PREINITIALIZED"
        );

        //Fill in possibly needed data.
        let ini_layout = if let Some(lay) = initial_layout {
            lay
        } else {
            ash::vk::ImageLayout::UNDEFINED
        };

        //TODO Check if that's actually okay o.o
        let sten_store = if let Some(op) = stencil_store_op {
            op
        } else {
            ash::vk::AttachmentStoreOp::DONT_CARE
        };

        let sten_load = if let Some(op) = stencil_load_op {
            op
        } else {
            ash::vk::AttachmentLoadOp::DONT_CARE
        };

        AttachmentDescription {
            format,
            sampling,
            store_op,
            load_op,
            stencil_store_op: sten_store,
            stencil_load_op: sten_load,
            initial_layout: ini_layout,
            final_layout,
        }
    }

    fn vko<'a>(&'a self) -> ash::vk::AttachmentDescriptionBuilder<'a> {
        let desc = ash::vk::AttachmentDescription::builder()
            .format(self.format)
            .samples(self.sampling)
            .load_op(self.load_op)
            .store_op(self.store_op)
            .stencil_store_op(self.stencil_store_op)
            .stencil_load_op(self.stencil_load_op)
            .initial_layout(self.initial_layout)
            .final_layout(self.final_layout);

        desc
    }
}

///Describes how an attachment is used in a subpass
#[derive(PartialEq)]
pub enum AttachmentRole {
    ///An Input attachment with a color format. For instance the GBuffer images after they where filled with data in the next pass (SSAO, LightAcc. etc.)
    InputColor,
    /// An InputAttachment with a depth and/or stencil format
    InputDepthStencil,
    ///Color attachment which gets written to. Usually your frambuffer images when used first in a renderpass.
    OutputColor,
    ///Marks this as an resolve attachment
    Resolve,
    /// When this is the depth and/or stencil attachment to this renderpass
    DepthStencil,
    Preserve,
}

pub struct Subpass {
    //The attachments that are referenced
    inp_attach: Vec<ash::vk::AttachmentReference>,
    col_attach: Vec<ash::vk::AttachmentReference>,
    res_attach: Vec<ash::vk::AttachmentReference>,
    ds_attach: Vec<ash::vk::AttachmentReference>,
    pre_attach: Vec<u32>,
}

impl Subpass {
    ///Builds a subpass based on the
    pub fn new(roles: Vec<AttachmentRole>) -> Self {
        //Sort out which attachment index should do what and build the attachment references accordingly
        let mut inp_attach: Vec<ash::vk::AttachmentReference> = Vec::new();
        let mut col_attach: Vec<ash::vk::AttachmentReference> = Vec::new();
        let mut res_attach: Vec<ash::vk::AttachmentReference> = Vec::new();
        let mut ds_attach: Vec<ash::vk::AttachmentReference> = Vec::new();

        let mut pre_attach: Vec<u32> = Vec::new(); //Only describes the index used

        for (index, role) in roles.into_iter().enumerate() {
            match role {
                AttachmentRole::InputColor => inp_attach.push(
                    ash::vk::AttachmentReference::builder()
                        .attachment(index as u32)
                        .layout(ash::vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                        .build(),
                ),
                AttachmentRole::InputDepthStencil => inp_attach.push(
                    ash::vk::AttachmentReference::builder()
                        .attachment(index as u32)
                        .layout(ash::vk::ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL)
                        .build(),
                ),
                AttachmentRole::OutputColor => col_attach.push(
                    ash::vk::AttachmentReference::builder()
                        .attachment(index as u32)
                        .layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .build(),
                ),
                AttachmentRole::Resolve => res_attach.push(
                    ash::vk::AttachmentReference::builder()
                        .attachment(index as u32)
                        .layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
                        .build(),
                ),
                AttachmentRole::DepthStencil => ds_attach.push(
                    ash::vk::AttachmentReference::builder()
                        .attachment(index as u32)
                        .layout(ash::vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
                        .build(),
                ),
                AttachmentRole::Preserve => pre_attach.push(index as u32),
            }
        }

        debug_assert!(
            ds_attach.len() <= 1,
            "There should only be one depth stencil attachment be specified"
        );

        Subpass {
            inp_attach,
            col_attach,
            res_attach,
            ds_attach,
            pre_attach,
        }
    }

    pub fn vko<'a>(&'a self) -> ash::vk::SubpassDescriptionBuilder<'a> {
        let mut info = ash::vk::SubpassDescription::builder();
        if self.inp_attach.len() > 0 {
            info = info.input_attachments(&self.inp_attach);
        }
        if self.col_attach.len() > 0 {
            info = info.color_attachments(&self.col_attach);
        }
        if self.res_attach.len() > 0 {
            info = info.resolve_attachments(&self.res_attach);
        }
        if self.ds_attach.len() > 0 {
            info = info.depth_stencil_attachment(&self.ds_attach[0]);
        }
        if self.pre_attach.len() > 0 {
            info = info.resolve_attachments(&self.res_attach);
        }
        info
    }
}
///Defines how one subpass depends on another.
/// # Why and how do I synchronize subpasses.
/// # Why?
/// The dependency tell the gpu at which point it can transition the image layout of an image from the layout needed in subpass A to the layout needed in subpass B.
/// Which layouts is set within the SubpassDescription on a per attachment bases. In marp-vk this is abstract by setting roles per attachment for each
/// subpass. E.g., if the role in subpass A was `OutputColor` and is `InputColor` in B then the layout will be changed from `INPUT_ATTACHMENT_OPTIMAL` to `SHADER_READ_ONLY_OPTIMAL`.
/// ## src_subpass and dst_subpass
/// Describes between which passes this dependency must be fulfilled.
/// If you want a former subpass, outside of this renderpass finished, use `ash::vk::SUBPASS_EXTERNAL`.
///
/// ## stage masks
/// The `src_stage_mask` (src_stage and dst_stage in the `new()` function) describes which stage on the src subpass has to finished before we can transition.
/// For instance if you write to a attachment and want to read from it in the next subpass you'll need to use the
/// `COLOR_ATTACHMENT_OUTPUT`, since the output should be finished.
///
/// The `dst_stage_mask` describes until which point a pipeline can be execute before it has to wait on the `src_subpass` reaching the `src_mask`.
/// For instance when using a full-screen quad to do post processing you can already process its vertex_shader and only need to wait at the fragment shader
/// till the src_pass (in this example geometry drawing) has finished. Therefore you would use `FRAGMENT_SHADER` here.
///
/// ## AccessMask
/// Specifies how a command/shader can access the image. Try to hold them as small as possible.

/// ## Performance
/// In general you should try to avoid bigger changes between stages, the worst case would be from BottomOfPipe to TopOfPipe since you would have to wait till the first subpass has completed it's work completely until you can start the next pass.
/// # General Usage advice.
/// The dependencies between the passes are depenent on what you do and what attachments you use. However, you'll
/// usually need a dependency from `SUBPASS_EXTERNAL` to subpass 0 and on from the last subpass to `SUBPASS_EXTERNAL`.
pub struct SubpassDependency {
    dependency: ash::vk::SubpassDependency,
}

impl SubpassDependency {
    pub fn new(
        src_pass_index: u32,
        dst_pass_index: u32,
        src_stage: ash::vk::PipelineStageFlags,
        dst_stage: ash::vk::PipelineStageFlags,
        src_access_mask: ash::vk::AccessFlags,
        dst_access_mask: ash::vk::AccessFlags,
    ) -> Self {
        SubpassDependency {
            dependency: ash::vk::SubpassDependency::builder()
                .src_subpass(src_pass_index)
                .dst_subpass(dst_pass_index)
                .src_stage_mask(src_stage)
                .dst_stage_mask(dst_stage)
                .src_access_mask(src_access_mask)
                .dst_access_mask(dst_access_mask)
                .build(),
        }
    }

    pub fn vko(&self) -> &ash::vk::SubpassDependency {
        &self.dependency
    }
}

pub struct RenderPassBuilder {
    attachments: Vec<AttachmentDescription>,
    dependencies: Vec<SubpassDependency>,
    subpasses: Vec<Subpass>,
}

impl RenderPassBuilder {
    pub fn new(attachments: Vec<AttachmentDescription>) -> Self {
        RenderPassBuilder {
            attachments,
            dependencies: Vec::new(),
            subpasses: Vec::new(),
        }
    }

    ///Adds a subpass which assigns roles to each attachment in the order they where supplied to the `new()` function.
    pub fn add_subpass(mut self, roles: Vec<AttachmentRole>) -> Self {
        debug_assert!(
            roles.len() == self.attachments.len(),
            "You use exactly one role per attachment! Roles: {}, attachmentents: {}",
            roles.len(),
            self.attachments.len()
        );
        self.subpasses.push(Subpass::new(roles));
        self
    }

    pub fn add_dependency(mut self, dep: SubpassDependency) -> Self {
        self.dependencies.push(dep);
        self
    }

    ///Builds the final renderpass description.
    pub fn build(self, device: Arc<Device>) -> Result<Arc<RenderPass>, ash::vk::Result> {
        let attachments_raw: Vec<ash::vk::AttachmentDescription> =
            self.attachments.into_iter().map(|at| *at.vko()).collect();

        let subpass_raw: Vec<ash::vk::SubpassDescription> =
            self.subpasses.iter().map(|sp| *sp.vko()).collect();
        let deps_raw: Vec<ash::vk::SubpassDependency> =
            self.dependencies.iter().map(|dep| *dep.vko()).collect();

        let info = ash::vk::RenderPassCreateInfo::builder()
            .attachments(attachments_raw.as_slice())
            .subpasses(subpass_raw.as_slice())
            .dependencies(deps_raw.as_slice())
            .build();

        let renderpass: ash::vk::RenderPass = unsafe {
            match device.vko().create_render_pass(&info, None) {
                Ok(renderpass) => renderpass,
                Err(er) => return Err(er),
            }
        };

        Ok(Arc::new(RenderPass {
            device: device,
            renderpass: renderpass,
        }))
    }
}

/// Describes in which order the gpu can read and write data to images of the Framebuffer.
/// # Attachment image types
/// ## Input attachments:
/// The input attachment is what you usually access in the shader via a line like
/// ```ignore
/// layout(location = 0) in vec3 InputAttachmentTex;
/// ```
/// You'll have to present the Renderpass exactly as many input attachments as are referenced in the shader.
/// When calling `add_input_attachment(attachment)` you can reference the iamge in the shader at `location = x+1` where x is the number of added input attachments before that call.
/// Be sure to also provide enough attachments in the `Framebuffer` which is used with this renderpass and pipeline.
/// ## Color Attachments
/// Describes the outputted images used with this renderpass.
/// In glsl you'll usually have one ore more lines like
/// ```ignore
/// layout(location = 0) out vec4 Output;
/// ```
/// As usual, be sure to submit an actual image at the location x when using `layout(location = x)` in the shader.
/// ## Resolve attachment
/// When using n multi sampled color attachments, you can also provide n resolve attachments with a different sampling count.
/// After each subpass a resolve operation will resolve the color attachment at index `x` to the resolve attachment at index x
/// ## Depth stencil attachment
/// Defines an attachment in an depth+stencil or only depth or stencil attachment that is used in this subpass to write out one of the operations.
/// ## Preserve attachment
/// Defines images that preserve their content until after this pass, but are not used to read or write from in this pass.
/// # Usage
/// The usual way of creating a whole RenderPass is:
/// - define a AttachmentDescription for each attachment
/// - create a RenderPassBuilder from them
/// - create supasses by defining the role of each attachment. The order of the rolles is directly mapped to the
/// attachment. So `Role[n]` will be mapped to `attachment[n]`
/// - define dependencies between the subpasses. Similar to the roles the subpass indexes coresond directly to the submitting order via `add_subpass()`
/// - build final renderpass.
pub struct RenderPass {
    renderpass: ash::vk::RenderPass,
    device: Arc<Device>,
}

impl RenderPass {
    pub fn new(attachments: Vec<AttachmentDescription>) -> RenderPassBuilder {
        RenderPassBuilder::new(attachments)
    }

    pub fn vko(&self) -> &ash::vk::RenderPass {
        &self.renderpass
    }
}

impl Drop for RenderPass {
    fn drop(&mut self) {
        unsafe {
            self.device.vko().destroy_render_pass(*self.vko(), None);
        }
    }
}
