/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Pipelines
//! In essence there are two types of pipelines:
//! - Compute: works on data by normal reads/writes
//! - Graphics: processes input data by pushing it through the graphics pipeline (like input assembly, vertex sahder, fragment shader etc.)
//!
//! # PipelineLayout
//! The pipeline layout describes which data the pipeline has to expect when starting. This contains:
//! - DescriptorSetsLayouts
//! - push constants
//! You'll usually need to specify this on a per Pipeline basis, since the inputs ususually change.
//!
//! # A Note on the optional state patameters
//! Those can be used to quickly change the behavoir of a pipeline right before starting to draw with the pipeline.
//! However, it is usually better to predefine them if possible. Keep in mind that if you use a `None` value, you are expected to the value
//! at runtime via the specified `cmd_set_*` functions.
//!
//!
//!
//!

use crate::device::*;
use crate::shader::*;

use crate::framebuffer;
use std::sync::Arc;

use ash::vk;

#[derive(Debug)]
pub enum PipelineError {
    LayoutCreateError(vk::Result),
    ComputePipelineCreateError(vk::Result),
    GraphicsPipelineCreateError(vk::Result),
}

///Describes the specialization constants for a pipeline. Needs to be passed when the pipeline
/// is created and stays constant.
///# Note
/// This is currently unimplemented!
pub struct SpecializationConstant {}

///Describes in which order descriptor_sets and push_constants will be supplied to the pipeline using that layout.
pub struct PipelineLayout {
    device: Arc<Device>,
    layout: vk::PipelineLayout,
    descriptor_layouts: Vec<vk::DescriptorSetLayout>,
    push_constants: Vec<vk::PushConstantRange>,
}

impl PipelineLayout {
    ///Creates a new Pipeline layout at which you can bind descriptor sets and push constants with the specified layout
    /// or range.
    pub fn new(
        device: Arc<Device>,
        descriptor_sets: Vec<vk::DescriptorSetLayout>,
        push_constants: Vec<vk::PushConstantRange>,
    ) -> Result<Arc<Self>, PipelineError> {
        let info = vk::PipelineLayoutCreateInfo::builder()
            .set_layouts(&descriptor_sets)
            .push_constant_ranges(&push_constants);

        let layout = unsafe {
            match device.vko().create_pipeline_layout(&info, None) {
                Ok(lay) => lay,
                Err(er) => return Err(PipelineError::LayoutCreateError(er)),
            }
        };

        Ok(Arc::new(PipelineLayout {
            device,
            layout,
            descriptor_layouts: descriptor_sets,
            push_constants,
        }))
    }

    pub fn vko<'a>(&'a self) -> &'a vk::PipelineLayout {
        &self.layout
    }

    pub fn device(&self) -> Arc<Device> {
        self.device.clone()
    }

    pub fn get_descriptor_set_layouts(&self) -> &Vec<vk::DescriptorSetLayout> {
        &self.descriptor_layouts
    }

    pub fn get_push_constant_ranges(&self) -> &Vec<vk::PushConstantRange> {
        &self.push_constants
    }
}

impl Drop for PipelineLayout {
    fn drop(&mut self) {
        unsafe { self.device.vko().destroy_pipeline_layout(self.layout, None) }
    }
}

pub trait Pipeline {
    fn vko(&self) -> &vk::Pipeline;
    fn layout(&self) -> Arc<PipelineLayout>;
}

pub struct ComputePipeline {
    device: Arc<Device>,
    layout: Arc<PipelineLayout>,
    pipeline: vk::Pipeline,
    #[allow(dead_code)]
    shader_stage: Arc<ShaderStage>,
}

impl ComputePipeline {
    pub fn new(
        device: Arc<Device>,
        shader: Arc<ShaderStage>,
        layout: Arc<PipelineLayout>,
    ) -> Result<Arc<Self>, PipelineError> {
        debug_assert!(
            shader.stage() == Stage::Compute,
            "The shader stage for a compute pipeline must be Compute"
        );

        let info = ash::vk::ComputePipelineCreateInfo::builder()
            .stage(*shader.vko(None)) //TODO implement specialization constants
            .layout(*layout.vko());

        let pipeline = unsafe {
            match device.vko().create_compute_pipelines(
                vk::PipelineCache::null(), //Currently we don't use pipeline caches... might change
                &[info.build()],
                None,
            ) {
                Ok(mut pipes) => pipes.pop().expect("Failed to get created pipeline!"),
                Err((cor_pipes, er)) => {
                    //Delete the created pipelines if there are any, then return the error
                    for p in cor_pipes.into_iter() {
                        device.vko().destroy_pipeline(p, None);
                    }
                    return Err(PipelineError::ComputePipelineCreateError(er));
                }
            }
        };

        Ok(Arc::new(ComputePipeline {
            device,
            layout,
            pipeline,
            shader_stage: shader,
        }))
    }

    pub fn vko<'a>(&'a self) -> &'a vk::Pipeline {
        &self.pipeline
    }

    pub fn layout(&self) -> Arc<PipelineLayout> {
        self.layout.clone()
    }
}

impl Drop for ComputePipeline {
    fn drop(&mut self) {
        unsafe { self.device.vko().destroy_pipeline(self.pipeline, None) }
    }
}

impl Pipeline for ComputePipeline {
    fn vko(&self) -> &vk::Pipeline {
        &self.pipeline
    }
    fn layout(&self) -> Arc<PipelineLayout> {
        self.layout.clone()
    }
}

///Describes how a mask is used. Either set to a static value, or update dynamicly.
#[derive(Copy, Clone, Debug)]
pub enum MaskValue<T> {
    Static { val: T },
    Dynamic,
}

///Describes how Vertexs are bound to the pipeline and what to expect from one vertex.
#[derive(Clone, Debug)]
pub struct VertexInputState {
    ///Descibes to which binding in the shader the vertex attributes are bound
    pub vertex_binding_descriptions: Vec<vk::VertexInputBindingDescription>,
    ///Describes which attrbutes are sent how on a per vertex basis.
    pub vertex_attribute_descriptions: Vec<vk::VertexInputAttributeDescription>,
}

impl VertexInputState {
    pub fn vko<'a>(&'a self) -> vk::PipelineVertexInputStateCreateInfoBuilder<'a> {
        vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(&self.vertex_binding_descriptions)
            .vertex_attribute_descriptions(&self.vertex_attribute_descriptions)
    }
}

///Describes the input assemply stage.
/// # Safety
/// If you use a `*_LIST` topology type then the following must be true:
/// - restart must be `false`
/// - the geometry-shader feature must be enabled
#[derive(Copy, Clone, Debug)]
pub struct InputAssemblyState {
    pub topology: vk::PrimitiveTopology,
    ///Can be set to true which enables you to pass the 0xFFFFFFFF/0xFFFF as an index to restart primitives assembly. Is not allowed
    /// for LIST topologies.
    /// You usually have to set this to false.
    pub restart_enabled: bool,
}

impl InputAssemblyState {
    pub fn vko<'a>(&'a self) -> vk::PipelineInputAssemblyStateCreateInfoBuilder<'a> {
        let info = vk::PipelineInputAssemblyStateCreateInfo::builder()
            .topology(self.topology)
            .primitive_restart_enable(self.restart_enabled);

        info
    }
}

#[derive(Copy, Clone, Debug)]
pub struct TessellationState {
    ///Number of control points per patch
    pub patch_control_points: u32,
}

impl TessellationState {
    pub fn vko<'a>(&self) -> vk::PipelineTessellationStateCreateInfoBuilder<'a> {
        vk::PipelineTessellationStateCreateInfo::builder()
            .patch_control_points(self.patch_control_points)
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Viewport {
    pub corner_xy: [f32; 2],
    pub width_height: [f32; 2],
    pub min_max_depth: [f32; 2],
}

impl Viewport {
    pub fn new(
        corner_xy: [f32; 2],
        width: f32,
        height: f32,
        min_depth: f32,
        max_depth: f32,
    ) -> Self {
        let info = Viewport {
            corner_xy,
            width_height: [width, height],
            min_max_depth: [min_depth, max_depth],
        };

        info
    }

    pub fn width(&self) -> f32 {
        self.width_height[0]
    }

    pub fn height(&self) -> f32 {
        self.width_height[1]
    }

    pub fn min_depth(&self) -> f32 {
        self.min_max_depth[0]
    }

    pub fn max_depth(&self) -> f32 {
        self.min_max_depth[1]
    }

    pub fn vko<'a>(&'a self) -> vk::Viewport {
        vk::Viewport::builder()
            .x(self.corner_xy[0])
            .y(self.corner_xy[1])
            .width(self.width())
            .height(self.height())
            .min_depth(self.min_depth())
            .max_depth(self.max_depth())
            .build()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Scissor {
    ///Offset in `[x,y]` direction
    pub offset: [i32; 2],
    /// extend in `[x,y]` direction
    pub extent: vk::Extent2D,
}

impl Scissor {
    pub fn vko(&self) -> vk::Rect2D {
        vk::Rect2D::builder()
            .extent(self.extent)
            .offset(
                vk::Offset2D::builder()
                    .x(self.offset[0])
                    .y(self.offset[1])
                    .build(),
            )
            .build()
    }
}

/// Decribes the state the viewport is in. If a value is `None` is is assumed that the value is set while drawing with the
/// correct set-command on the command-buffer.
///
/// For each vieport used by this pipeline there should be scissor.
#[derive(Clone)]
pub struct ViewportState {
    mode: ViewportMode,
    ///If None, has to be set via `cmd_set_viewport()`
    #[allow(dead_code)]
    viewports: Option<Vec<Viewport>>,
    vk_viewports: Vec<vk::Viewport>,
    ///If None, has to be set via `cmd_set_scissor()`
    #[allow(dead_code)]
    scissors: Option<Vec<Scissor>>,
    vk_scissors: Vec<vk::Rect2D>,
    //The inner state
    //state: vk::PipelineViewportStateCreateInfoBuilder<'a>,
}

#[derive(Clone)]
pub enum ViewportMode {
    ///Viewport and scissors have to be set when recording the command buffer, the usize is the nuber of viewports you want to have.
    /// On normal screens this is usuall 1, but must never be 0.
    Dynamic(usize),
    Static(Vec<(Viewport, Scissor)>),
}

impl ViewportState {
    ///Creates a new vieport state based on the supplied viewport and scissors info.
    /// If you want to create multible viewports, the `multible_viewports` feature must be enabled, otherwise the vector has to have length 1.
    /// You can also do not specify any viewport and/or scissor, in which case you would hvae to set them on the command buffer via the right `cmd_*_()` function.
    pub fn new(viewports: ViewportMode) -> Self {
        let mut views = Vec::new();
        let mut scis = Vec::new();
        if let ViewportMode::Static(view_scis) = viewports.clone() {
            for (v, s) in view_scis {
                views.push(v);
                scis.push(s);
            }
        }

        let vk_views: Vec<vk::Viewport> = views.iter().map(|v| v.vko()).collect();
        let vk_scis: Vec<vk::Rect2D> = scis.iter().map(|s| s.vko()).collect();

        let inner_views = if views.len() > 0 { Some(views) } else { None };

        let inner_scis = if scis.len() > 0 { Some(scis) } else { None };

        ViewportState {
            mode: viewports,
            viewports: inner_views,
            vk_viewports: vk_views,
            scissors: inner_scis,
            vk_scissors: vk_scis,
        }
    }

    pub fn vko<'a>(&'a self) -> vk::PipelineViewportStateCreateInfoBuilder<'a> {
        //Check if we are the dynamic or the static version.
        match self.mode {
            ViewportMode::Dynamic(count) => vk::PipelineViewportStateCreateInfo::builder()
                .viewport_count(count as u32)
                .scissor_count(count as u32),
            ViewportMode::Static(_) => {
                //We can add the viewports since we got some
                vk::PipelineViewportStateCreateInfo::builder()
                    .viewports(&self.vk_viewports)
                    .scissors(&self.vk_scissors)
            }
        }
    }
    ///Returns true if this viewport is staticly set and has not to be set while recording a command buffer.
    pub fn is_static(&self) -> bool {
        match self.mode {
            ViewportMode::Static(_) => true,
            _ => false,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum DepthBias {
    ///Used if the depth bias is set on pipeline creation
    Static {
        /// scalar factor that is added to each depth value per fragment.
        depth_bias_constant_factor: f32,
        /// Is the maximum (or minimum) dpeth bias of a fragment
        depth_bias_clamp: f32,
        /// is a scalar applied to a fragments slope in depth bias calculation
        depth_bias_slope_factor: f32,
    },
    ///Used if the values are set right before using this pipeline via `cmd_set_depth_bias`
    Dynamic,
}

impl DepthBias {
    /// Returns with no bias added to depth calculation
    pub fn none() -> Self {
        //TODO check if those are right
        DepthBias::Static {
            depth_bias_constant_factor: 0.0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct RasterizationState {
    ///If the depth values should be clamped instead of clipping primitives to the z plane of the frustum.
    ///If this is true, the `depth_clamp` feature has to be enabled.
    pub depth_clamp_enabled: bool,
    //if primitves should be discarded imidiatly  before rasterization stage
    pub rasterizer_discard_enabled: bool,
    ///The triangle rendering mode, if this is not `POLYGONE_MODE_FILL` then the `non_filling_mode` feature has to be enabled.
    pub polygone_mode: vk::PolygonMode,
    /// Controlls whether polygones should be culled based on their facing direction
    pub cull_mode: vk::CullModeFlags,
    ///Defines which means "front facing" when deciding to cull a triangle
    pub front_face: vk::FrontFace,
    ///If None, has to be set via `cmd_set_line_width()`
    pub line_width: Option<f32>,
    ///If None, then `use_depth_bias` is set to `false`, otherwise is set according to the value
    pub depth_bias: Option<DepthBias>,
}

impl RasterizationState {
    pub fn vko<'a>(&'a self) -> vk::PipelineRasterizationStateCreateInfoBuilder<'a> {
        let mut info = vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(self.depth_clamp_enabled)
            .rasterizer_discard_enable(self.rasterizer_discard_enabled)
            .polygon_mode(self.polygone_mode)
            .cull_mode(self.cull_mode)
            .front_face(self.front_face);

        //Check if we have line width and depth_bias info set
        if let Some(bias_info) = self.depth_bias {
            info = info.depth_bias_enable(true);
            //Check if we got values, otherwise those are set on runtime
            match bias_info {
                DepthBias::Static {
                    depth_bias_constant_factor,
                    depth_bias_clamp,
                    depth_bias_slope_factor,
                } => {
                    info = info
                        .depth_bias_constant_factor(depth_bias_constant_factor)
                        .depth_bias_clamp(depth_bias_clamp)
                        .depth_bias_slope_factor(depth_bias_slope_factor);
                }
                DepthBias::Dynamic => {}
            }
        } else {
            info = info.depth_bias_enable(false);
        }

        //Now check if we can set a line width
        if let Some(wd) = self.line_width {
            info = info.line_width(wd);
        }

        info
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MultisampleState {
    ///Specifies how many samples are taken per pixel
    pub sample_count: vk::SampleCountFlags,
    ///If true, each sample will be shaded on its own. `sample_shading_enable` feature must be enabled for this to work.
    pub sample_shading: bool,
    ///if `sample_shading` is true, specdifies a minimum fraction of sample shading
    pub min_sample_shading: f32,
    //TODO add the sample mask stuff, but I'm not sure when to use it and how to make it more easy to use.
    /// Controls whether a temporary coverage value is generated based on the alpha component of the fragment’s
    /// first color output as specified in the `Multisample Coverage` section.
    pub alpha_to_coverage: bool,
    /// controls whether the alpha component of the fragment’s first color output is replaced with one as described in `Multisample Coverage`.
    pub alpha_to_one: bool,
}

impl MultisampleState {
    pub fn vko<'a>(&'a self) -> vk::PipelineMultisampleStateCreateInfoBuilder<'a> {
        let info = vk::PipelineMultisampleStateCreateInfo::builder()
            .rasterization_samples(self.sample_count)
            .sample_shading_enable(self.sample_shading)
            .min_sample_shading(self.min_sample_shading)
            .alpha_to_coverage_enable(self.alpha_to_coverage)
            .alpha_to_one_enable(self.alpha_to_one);

        info
    }
}

#[derive(Copy, Clone, Debug)]
pub enum ColorBlendAttachmentState {
    ///No Blending, each fragment will oveerride the one that was there before.
    NoBlending,
    ///Turns on Blending, lets you choose how a fragment is blended to the information that where at this location before hand.
    Blending {
        ///selects which blend factor is used to determine the source factors
        src_color_blend_factor: vk::BlendFactor,
        ///selects which blend factor is used to determine the destination factors
        dst_color_blend_factor: vk::BlendFactor,
        /// set the blend operation used to write to this attachment
        color_blend_op: vk::BlendOp,
        /// selects which blend factor is used to determine the source factor
        src_alpha_blend_factor: vk::BlendFactor,
        /// selects which blend factor is used to determine the destination factor
        dst_alpha_blend_factor: vk::BlendFactor,
        ///the blending operation for the alpha channel.
        alpha_blend_op: vk::BlendOp,
        ///Mask which describes which channels are enabled for writing.
        color_write_mask: vk::ColorComponentFlags,
    },
}

impl ColorBlendAttachmentState {
    pub fn vko(&self) -> vk::PipelineColorBlendAttachmentState {
        match self {
            ColorBlendAttachmentState::NoBlending => vk::PipelineColorBlendAttachmentState {
                blend_enable: 0,
                src_color_blend_factor: vk::BlendFactor::ONE,
                dst_color_blend_factor: vk::BlendFactor::ZERO,
                color_blend_op: vk::BlendOp::ADD,
                src_alpha_blend_factor: vk::BlendFactor::ONE,
                dst_alpha_blend_factor: vk::BlendFactor::ZERO,
                alpha_blend_op: vk::BlendOp::ADD,
                color_write_mask: vk::ColorComponentFlags::all(),
            },
            ColorBlendAttachmentState::Blending {
                src_color_blend_factor,
                dst_color_blend_factor,
                color_blend_op,
                src_alpha_blend_factor,
                dst_alpha_blend_factor,
                alpha_blend_op,
                color_write_mask,
            } => vk::PipelineColorBlendAttachmentState {
                blend_enable: 1,
                src_color_blend_factor: *src_color_blend_factor,
                dst_color_blend_factor: *dst_color_blend_factor,
                color_blend_op: *color_blend_op,
                src_alpha_blend_factor: *src_alpha_blend_factor,
                dst_alpha_blend_factor: *dst_alpha_blend_factor,
                alpha_blend_op: *alpha_blend_op,
                color_write_mask: *color_write_mask,
            },
        }
    }
}

///Descibes blending behavoir within this pipeline between the processed fragments.
#[derive(Clone, Debug)]
pub struct ColorBlendState {
    /// If None, has to be set via `cmd_set_blend_constant()`
    pub blend_constant: Option<[f32; 4]>,
    /// Can set a logical operation which is used while blending
    pub logic_op: Option<vk::LogicOp>,
    ///The attachments used for this blend state
    pub attachments: Vec<ColorBlendAttachmentState>,
    pub vk_attachments: Vec<vk::PipelineColorBlendAttachmentState>,
    //The vulkan representation of the info recived when creating.
    //pub color_blend_state: vk::PipelineColorBlendStateCreateInfo,
}

impl ColorBlendState {
    ///Builds the approbriate vulkan create info for this data.
    /// #Safety
    /// You need one `ColorBlendAttachmentState` per color attachment in the renderpass. The index of the blend_state corresponds to the
    /// attachment index in the renderpass.
    pub fn new(
        blend_constant: Option<[f32; 4]>,
        logic_op: Option<vk::LogicOp>,
        attachments: Vec<ColorBlendAttachmentState>,
    ) -> Self {
        let ats: Vec<vk::PipelineColorBlendAttachmentState> =
            attachments.iter().map(|at| at.vko()).collect();
        ColorBlendState {
            blend_constant,
            logic_op,
            attachments,
            vk_attachments: ats,
            //color_blend_state: info.build(),
        }
    }
    ///Builds the ColorBlendStateInfo. Keep in mind that the `attachments` must contains exactly
    /// as many entrys as there are attachments attached to the subpass this pipeline is used in.
    pub fn vko<'a>(&'a self) -> vk::PipelineColorBlendStateCreateInfoBuilder<'a> {
        let mut info = vk::PipelineColorBlendStateCreateInfo::builder();
        if let Some(op) = self.logic_op {
            info = info.logic_op_enable(true).logic_op(op);
        } else {
            info = info.logic_op_enable(false).logic_op(vk::LogicOp::NO_OP);
        }

        if let Some(blend_const) = self.blend_constant {
            info = info.blend_constants(blend_const);
        }

        //let vk_attachments: Vec<vk::PipelineColorBlendAttachmentStateBuilder> = attachments.iter().map(|at| at.vko()).collect();
        //Now set the attachments we use and return
        info = info.attachments(&self.vk_attachments);
        info
    }
}

///Describes how a mask is set. Either on runtime, with a static value, or not at all.
#[derive(Copy, Clone, Debug)]
pub enum MaskState<T> {
    StaticValue(T),
    ///If it is set via a command buffer set command
    DynmamicValue,
    /// If there is no mask
    None,
}

impl<T> MaskState<T> {
    pub fn is_none(&self) -> bool {
        match self {
            MaskState::None => true,
            _ => false,
        }
    }

    pub fn is_dynamic(&self) -> bool {
        match self {
            MaskState::DynmamicValue => true,
            _ => false,
        }
    }
}

///Specifies how the stencil has to behave when being used.
#[derive(Copy, Clone, Debug)]
pub struct StencilOpState {
    pub fail_op: vk::StencilOp,
    pub pass_op: vk::StencilOp,
    pub depth_fail_op: vk::StencilOp,
    pub compare_op: vk::CompareOp,
    ///If Dynamic, has to be set via `cmd_set_stencil_compare_mask()`. Selects the bits that are used when doing the stencil compare operation.
    pub compare_mask: MaskState<u32>,
    ///If Dynamic, has to be set via `cmd_set_stencil_write_mask()`. Selects the bits that get update when
    pub write_mask: MaskState<u32>,
    ///If Dynamic, has to be set via `cmd_set_stencil_reference()`
    pub reference: MaskState<u32>,
}

impl StencilOpState {
    pub fn vko<'a>(&'a self) -> vk::StencilOpStateBuilder<'a> {
        let mut info = vk::StencilOpState::builder()
            .fail_op(self.fail_op)
            .pass_op(self.pass_op)
            .depth_fail_op(self.depth_fail_op)
            .compare_op(self.compare_op);

        match self.compare_mask {
            MaskState::StaticValue(val) => info = info.compare_mask(val),
            _ => {}
        }

        match self.write_mask {
            MaskState::StaticValue(val) => info = info.write_mask(val),
            _ => {}
        }

        match self.reference {
            MaskState::StaticValue(val) => info = info.reference(val),
            _ => {}
        }

        info
    }
}

#[derive(Copy, Clone, Debug)]
pub enum DepthBounds {
    ///Static bounds set at creation time. Must be between 0.0-1.0
    Static { min: f32, max: f32 },
    /// Will be set via `cmd_set_depth_bounds()` on runtime.
    Dynamic,
}

#[derive(Clone, Copy, Debug)]
pub enum StencilTestState {
    ///If no stencil tests are performed on the fragments.
    NoTest,
    ///If there are stencil tests performed
    Test {
        front: StencilOpState,
        back: StencilOpState,
    },
}

impl StencilTestState {
    pub fn stencil_test_enabled(&self) -> bool {
        match self {
            StencilTestState::NoTest => true,
            _ => false,
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DepthStencilState {
    ///True if depth test is enabled
    pub depth_test_enabled: bool,
    ///True if the fragments depth value should be written to the depth attachment
    pub depth_write_enabled: bool,
    ///Controlls the compare operation done, decide culling based on depth values.
    pub depth_compare_op: vk::CompareOp,
    ///True if stencil test should be done as well.
    pub stencil_test: StencilTestState,
    ///If None, `depth_bound_enable` is set to `false`, otherwise it is `true` and either set
    /// On creation time, or via a command.
    pub depth_bounds: Option<DepthBounds>,
}

impl DepthStencilState {
    pub fn vko<'a>(&'a self) -> vk::PipelineDepthStencilStateCreateInfoBuilder<'a> {
        let mut info = vk::PipelineDepthStencilStateCreateInfo::builder()
            .depth_test_enable(self.depth_test_enabled)
            .depth_write_enable(self.depth_write_enabled)
            .depth_compare_op(self.depth_compare_op);

        if let Some(db) = self.depth_bounds {
            info = info.depth_bounds_test_enable(true);
            match db {
                DepthBounds::Static { min, max } => {
                    info = info.min_depth_bounds(min).max_depth_bounds(max);
                }
                _ => {} //will be set via the command
            }
        } else {
            //No bound testing at all
            info = info.depth_bounds_test_enable(false);
        }

        //Check if we do stencil testing
        if let StencilTestState::Test { front, back } = self.stencil_test {
            info = info
                .stencil_test_enable(true)
                .front(*front.vko())
                .back(*back.vko());
        } else {
            info = info.stencil_test_enable(false);
        }

        info
    }
}

///An abstraction over the variouse states a `GraphicsPipeline` has to specify.

pub struct PipelineState {
    vertex_input_state: VertexInputState,
    input_assembly_state: InputAssemblyState,
    tessellation_state: Option<TessellationState>,
    viewport_state: ViewportState,
    rasterization_state: RasterizationState,
    multisample_state: MultisampleState,
    depth_stencil_state: DepthStencilState,
    /// When using a fragment stage you have to supply information about how color is blended as
    /// well as about the attachments in the order they are binded to the subpass in which this pipeline is used.
    color_blend_state: Option<ColorBlendState>,
    ///The calculated dynamic states for this pipeline
    dynamic_state: vk::PipelineDynamicStateCreateInfo,
    #[allow(dead_code)]
    dyn_info: Vec<vk::DynamicState>,
}

///Defines the whole pipeline state. If you include a fragment stage, be sure to define `color_blend_state` with `Some()` information.
/// Same is valid if you use a tesselation shader for the `tesselation_state`.
impl PipelineState {
    pub fn new(
        vertex_input_state: VertexInputState,
        input_assembly_state: InputAssemblyState,
        tessellation_state: Option<TessellationState>,
        viewport_state: ViewportState,
        rasterization_state: RasterizationState,
        multisample_state: MultisampleState,
        depth_stencil_state: DepthStencilState,
        color_blend_state: Option<ColorBlendState>,
    ) -> Self {
        //Calculate the dynamic state based on the inputs.
        let mut infos = Vec::new();

        if !viewport_state.is_static() {
            infos.push(vk::DynamicState::VIEWPORT);
            infos.push(vk::DynamicState::SCISSOR);
        }
        if rasterization_state.line_width.is_none() {
            infos.push(vk::DynamicState::LINE_WIDTH);
        }
        if let Some(ref db_info) = rasterization_state.depth_bias {
            //Check if it is dynamic
            match db_info {
                DepthBias::Dynamic => {
                    infos.push(vk::DynamicState::DEPTH_BIAS);
                }
                _ => {} //Seems to be static, so it is set at pipeline creation
            }
        }
        if let Some(ref blend_state) = color_blend_state {
            if blend_state.blend_constant.is_none() {
                infos.push(vk::DynamicState::BLEND_CONSTANTS);
            }
        }

        if let Some(ref debo) = depth_stencil_state.depth_bounds {
            match debo {
                DepthBounds::Dynamic => infos.push(vk::DynamicState::DEPTH_BOUNDS),
                _ => {} //Set static at creation time.
            }
        }

        //Check for the stencil dynamic parameters
        if let StencilTestState::Test { front, back } = depth_stencil_state.stencil_test {
            if front.compare_mask.is_dynamic() || back.compare_mask.is_dynamic() {
                infos.push(vk::DynamicState::STENCIL_COMPARE_MASK);
            }
            if front.write_mask.is_dynamic() || back.write_mask.is_dynamic() {
                infos.push(vk::DynamicState::STENCIL_WRITE_MASK);
            }
            if front.reference.is_dynamic() || back.reference.is_dynamic() {
                infos.push(vk::DynamicState::STENCIL_REFERENCE);
            }
        }

        let dynamic_state = vk::PipelineDynamicStateCreateInfo::builder()
            .dynamic_states(&infos)
            .build();

        PipelineState {
            vertex_input_state,
            input_assembly_state,
            tessellation_state,
            viewport_state,
            rasterization_state,
            multisample_state,
            depth_stencil_state,
            color_blend_state,
            dynamic_state,
            dyn_info: infos,
        }
    }
}

pub struct GraphicsPipeline {
    device: Arc<Device>,
    pipeline: vk::Pipeline,
    //The things that have to outlive this object
    layout: Arc<PipelineLayout>,
    #[allow(dead_code)]
    render_pass: Arc<framebuffer::RenderPass>,
    #[allow(dead_code)]
    stages: Vec<Arc<ShaderStage>>,
}

impl GraphicsPipeline {
    pub fn new(
        device: Arc<Device>,
        stages: Vec<Arc<ShaderStage>>,
        pipeline_state: PipelineState,
        layout: Arc<PipelineLayout>,
        render_pass: Arc<framebuffer::RenderPass>,
        subpass_id: u32,
    ) -> Result<Arc<Self>, PipelineError> {
        let inner_stages: Vec<_> = stages.iter().map(|s| *s.vko(None)).collect();

        let vert_info = pipeline_state.vertex_input_state.vko();
        let inp_info = pipeline_state.input_assembly_state.vko();
        let view_info = pipeline_state.viewport_state.vko();
        let raster_info = pipeline_state.rasterization_state.vko();
        let multi_info = pipeline_state.multisample_state.vko();
        let depth_stencil_info = pipeline_state.depth_stencil_state.vko();

        //First set the info that's always iven, then check if we got blend and tesselation info
        let mut info = vk::GraphicsPipelineCreateInfo::builder()
            .stages(&inner_stages)
            .vertex_input_state(&vert_info)
            .input_assembly_state(&inp_info)
            .viewport_state(&view_info)
            .rasterization_state(&raster_info)
            .multisample_state(&multi_info)
            .depth_stencil_state(&depth_stencil_info)
            .dynamic_state(&pipeline_state.dynamic_state)
            .layout(*layout.vko())
            .render_pass(*render_pass.vko())
            .subpass(subpass_id);

        //Find a potential tesselation info
        let mut tessellation_info = vk::PipelineTessellationStateCreateInfo::builder();
        if let Some(tes_inf) = pipeline_state.tessellation_state {
            tessellation_info = tes_inf.vko();
        };
        info = info.tessellation_state(&tessellation_info);

        //Find potential blend info
        let mut blend_state = vk::PipelineColorBlendStateCreateInfo::builder();
        if let Some(ref state) = pipeline_state.color_blend_state {
            //info = info.color_blend_state(&blend_state.vko());
            blend_state = state.vko();
        };
        info = info.color_blend_state(&blend_state);

        //Now create the pipeline from this info and keep it.
        let pipeline = unsafe {
            //currently not using a pipeline chache, might change in the future.
            match device.vko().create_graphics_pipelines(
                vk::PipelineCache::null(),
                &[info.build()],
                None,
            ) {
                Ok(pip) => pip,
                Err((pip, er)) => {
                    for p in pip {
                        #[cfg(feature = "logging")]
                        log::info!("Destroying failed pipeline creation object");
                        device.vko().destroy_pipeline(p, None);
                    }

                    return Err(PipelineError::GraphicsPipelineCreateError(er));
                }
            }
        };

        if pipeline.len() < 1 {
            #[cfg(feature = "logging")]
            log::warn!("no pipeline created, but there was no error!");
            panic!("No pipeline created but also no error thrown.");
        }

        Ok(Arc::new(GraphicsPipeline {
            device,
            pipeline: pipeline[0],
            layout,
            render_pass,
            stages,
        }))
    }
}

impl Drop for GraphicsPipeline {
    fn drop(&mut self) {
        unsafe {
            self.device.vko().destroy_pipeline(self.pipeline, None);
        }
    }
}

impl Pipeline for GraphicsPipeline {
    fn vko(&self) -> &vk::Pipeline {
        &self.pipeline
    }
    fn layout(&self) -> Arc<PipelineLayout> {
        self.layout.clone()
    }
}
