/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use ash::extensions::ext::DebugUtils;
use ash::vk::DebugUtilsMessengerEXT;

use std::default::Default;
use std::ops::Drop;
use std::sync::Arc;

use crate::miscellaneous::{AppInfo, Debugging, InstanceExtensions, InstanceLayer};

///Hosts the main entry point to the API as well as the Instance (Entry point to Vulkan).
pub struct Instance {
    entry: ash::Entry,
    instance: ash::Instance,
    pub(crate) debug_report_loader: Option<DebugUtils>,
    debug_messenger: Option<DebugUtilsMessengerEXT>,
    extensions: InstanceExtensions,
}

impl Instance {
    pub fn new(
        app_info: Option<AppInfo>,
        mut extensions: Option<InstanceExtensions>,
        layers: Option<Vec<InstanceLayer>>,
        debug_report: Option<Debugging>,
    ) -> Result<Arc<Self>, ash::InstanceError> {
        let app_info = if let Some(appinf) = app_info {
            appinf
        } else {
            AppInfo::default("Vulkan APP".to_string())
        };

        //if a debugger is supplied, make sure the extension is loaded
        if debug_report.is_some() {
            extensions = if let Some(mut e) = extensions {
                e.debug_utils = true;
                Some(e)
            } else {
                let mut e = InstanceExtensions::default();
                e.debug_utils = true;
                Some(e)
            };
        }

        let ext = if let Some(ext) = extensions {
            ext
        } else {
            InstanceExtensions::default()
        };

        let lay = if let Some(l) = layers { l } else { vec![] };

        let entry = unsafe { ash::Entry::new().expect("failed to generate entry") };
        let app_info_ci = app_info.to_vk_app_info();

        let layer_pointers: Vec<*const i8> = lay.iter().map(|l| l.as_ptr()).collect();
        let extensions_names = ext.to_names();

        //Since we have an entry, extension and layer, lets create the instance
        let inst_create_info = ash::vk::InstanceCreateInfo::builder()
            .flags(Default::default()) //TODO should they be set to something else?
            .application_info(&app_info_ci)
            .enabled_layer_names(&layer_pointers)
            .enabled_extension_names(&extensions_names);

        //Since we can be sure at this point that the values within inst_create_info are all no null pointers change to the unsafe part of starting the instance and adding (if needed)
        //debug layers
        unsafe {
            let instance: ash::Instance = match entry.create_instance(&inst_create_info, None) {
                Ok(inst) => inst,
                Err(er) => {
                    match er {
                        ash::InstanceError::LoadError(ref vec) => {
                            #[cfg(feature = "logging")]
                            log::error!("Instance Load Error: {:?}", vec);
                        }
                        ash::InstanceError::VkError(er) => {
                            #[cfg(feature = "logging")]
                            log::error!("Instance VkError: {}", er);
                        }
                    }
                    #[cfg(feature = "logging")]
                    log::error!("Error while creating Instance: {}!", er);
                    return Err(er);
                }
            };

            //Since we got a instance, test if we need a report, if so register
            let (debug_util_loader, messenger) = if let Some(rep) = debug_report {
                let debug_report_loader = DebugUtils::new(&entry, &instance);

                //Now based on the Debug features create a debug callback
                let create_info = rep.to_info();
                let messenger = debug_report_loader
                    .create_debug_utils_messenger(&create_info, None)
                    .unwrap();
                (Some(debug_report_loader), Some(messenger))
            } else {
                (None, None)
            };

            Ok(Arc::new(Instance {
                entry,
                instance,
                debug_report_loader: debug_util_loader,
                debug_messenger: messenger,
                extensions: ext,
            }))
        }
    }

    ///Returns the Entry point used for this Instance
    pub fn get_entry(&self) -> &ash::Entry {
        &self.entry
    }

    ///Returns the internal vulkan instance. Use that if you want to find for instance the physical devices this instance can talk to.
    /// # Warning
    /// Do not deference this and change it otherwise the application could have undefined behavior.
    pub fn vko(&self) -> &ash::Instance {
        &self.instance
    }

    pub fn get_loaded_extensions(&self) -> &InstanceExtensions {
        &self.extensions
    }

    pub fn get_debugger(&self) -> Option<&DebugUtils> {
        (&self.debug_report_loader).as_ref()
    }
}

impl Drop for Instance {
    fn drop(&mut self) {
        unsafe {
            //Destroy each thing
            if let Some(ref mut drl) = self.debug_report_loader {
                if let Some(dc) = self.debug_messenger {
                    drl.destroy_debug_utils_messenger(dc, None);
                }
            }
            self.instance.destroy_instance(None);
        }
    }
}
