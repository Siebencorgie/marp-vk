# Marp
This is the main wrapper library. It can be used standalone. Since it exposes the raw `ash` types for all the objects, it can also be use with other projects like the rust wrapper around AMD's memory allocator.
