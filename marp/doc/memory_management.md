# Memory management in Marp-vk
## Intro
According to several resources on the net it is the most efficient to allocate "big" buffers and suballocate resources from that buffer.
Since one of the reasons to use Vulkan is anyways to have a better performance this should be possible in Marp-vk as well.

## Build in memory allocator
I'll try to create a memory allocator that sits within the `Device`. It will still be possible to allocate memory your self
via ash/vulkan. However, the implementations for all images and buffer will use that allocator.
The idea is to, together with the resource send requirements to the allocator.
Based on the requirements the allocator searches for "free" memory in one of the allocations and sub allocates.
If that's not possible it will create a new buffer. At first this will happen in 256 Mb blocks (or 64 Mb for the small cpu and gpu visible heaps on amd). Later I might add some kind
of heuristics to allocate based on the resource and how those resources where used before. Or I might add some kind of hinting.
It should also be possible to allocate a fixed block of memory for the user if he is sure that he knows what he is doing. That is as specially useful for RenderTargets and bigger buffers.

## VRam recording
Most allocations which are held on the gpu for a longer time (eg. textures, GBuffer and statically sized buffers) should be created in Vram.
To bring something from the host (CPU) to the device we need to copy it over (from RAM to VRAM). However this is done by a command buffer.
While using the allocator you have two possibilities.
1. copy immediately 
2. add to a copy queue which is dispatched when calling `flush_copy()` the next time on the allocator.

When using method 1. you'll only have to wait for the returned semaphore to be "done". However, it is advised to use the copy queue for more allocations since this will be faster.

## User responsibility
The user of Mapr-vk can still use sub ranges of the create buffers and images. For instance for a texture lookup table or collecting all voxels of a scene in a big buffer. So nothing unusual. 
