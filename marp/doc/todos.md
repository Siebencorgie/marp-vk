# A collection of TODOs I have to do and would otherwise forget
## Descriptors and descriptor sets
- [ ] Transition the image that is used to the correct layout when used. There should be some kind of detection what is needed.
- [ ] Check if it should be allowed to specifiy the offset and range used when binding a buffer to a descriptor set
## Command Buffer
- [ ] add all the command buffer functions from the list.
- [ ] transition the dependent resouces to a HashSet to not store redundant pointers.
## Resource transition.
- [ ] implement that resource can transition. Buffer/Image can change access bits and queue ownership and image can transition their layout. Everything has to be synchronised with a command buffer. 
- [ ] make single execute transitions. For instance to transition a texture image once to the correct layout and access mask. Will wrap a mini command buffer which only execute the transition and then waits for finishing up.
## Sync module
- [ ] in the `wait_for_fences()` standalone function, check if the parent Arc of the fences can't be dropped since the inner fence is copied.
## Pipelines
- [ ] Check if we have to specify redundant information. If so, put the redundant info into the `.vko()` functions and add it a pipeline creation time.
## Memory
- [ ] Rewrite the way memory is managed. It is currently a bit clunky and memory uploads are non trivial. I'd like this to be fast and easy to use.
