# Marp-Vk
An simple wrapper around [ash](https://github.com/MaikKlein/ash) which tries to make working with Vulkan a bit easier.

# Disclaimer:
Since this is a one person project at the moment it won't advance as fast as you might like. If you want to use vulkan via rust I advice you to either use `ash` or `vulkano`
at the moment.

## Goals
Since I used vulkano before for a long time I want to make working with lowlevel vulkan a bit easier, but still remain lowlevel.
That means that this crate will still allow you to make mistakes, but it will take some work of you, you would have done anyways.
My idea is to wrap around the creation process (which usually involves many *CreationInfo* structs) of vulkan object. It should also handle deletion of said objects.
You will be able to choose how to create (automatic upload or handle upload your self) the objects and will have other convenient functions at your hand.
If you need the low_level version of the type you'll always be able to just call `.vko()`.

## Non Goals
I don't want to repeat the work of [vulkano](https://github.com/vulkano-rs/vulkano/tree/master/vulkano/src). I also don't want to have "only" bindings to vulkan. Instead I try to make the middle ground between ash and vulkano. An easy to use API which you can use, but still mess with the low level details.

## Who is the target group?
Mostly me :D . I "design" the API in a way that helps me advance my engines a bit faster.
*However*, if you like the way this API works, give it a try. If you like it, don't hesitate to commit and/or open issues.

## A Note on targeted performance
Since the whole goal of this library is to have a nice user experience but still be able to use all the features of vulkan I divide most of the processes in two
categories.
Creation: A process done only once per resource. For instance image creation or device creation. I try to catch some of the most common issues which might make it a bit
slower then raw vulkan but has a huge benefit.

ResourceUsage: the process of binding memory or similar actions a resource has to do several times. This is mostly unchecked (except for some small checks like correct queue family checks). This should make most of the run time cost very similar to raw vulkan.

## Usage and examples
You can find several examples on how to use the crate in the `examples` directory.

The triangle example is written with all the boilerplate code you'd expect in a vulkan triangle example. The other examples use the `marp-utils` subcrate to handle device and queue creation as well as starting and ending the command buffer.

Additionally, the `cube_textures.rs` example shows how to use the `debug_marker` extension for tagging regions and events in a command buffer while debugging via renderdoc.

## Stability of the crate
The crate in its current state is usable and kind of stable. I use it in my new voxel [engine](https://gitlab.com/tendsinmende/tikan). Most of the api features are tested there. However, be aware that the crate allows you to use the vulkan API in a wrong way. So be sure to understand vulkan before using it. This crate is a convenient wrapper around Ash, not a full fledged save API like vulkano. 


## Unimplemented Ideas 
- [ ] Use `spriv_reflect` crate to analyze shaders in debug mode and warn about incorrect usage.
- [ ] Use `spriv_reflect` to create input Structs for shaders automaticly (through macros?).
- [ ] macro for implementing a vertex state on a custom vertex struct

## Why do you do this in general?
Well, I need a substition for vulkano which I use for my hobby [engine](https://gitlab.com/tendsinmende/jakar-engine) since I need finer control over vulkan.

## Why "Marp"
Since my engine and its tools are called "jakar-…" which is a village in Buthan I wanted to call the vulkan wrapped they use something Buthanese as well. Since I could not find the word for fire or vulcan, i used the word for red: Marp.
